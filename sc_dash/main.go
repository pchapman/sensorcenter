package main

import (
	"fmt"
	"gitlab.com/pchapman/sensorcenter/sc_dash/httpsvc"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"os"
	"os/signal"
	"syscall"
)

const (
	envSSApiUrl     = "SS_API_URL"
	envSSDashKeyId  = "SS_DASH_KEY_ID"
	envSSDashSecret = "SS_DASH_KEY_SECRET"
)

func main() {
	// Obtain and verify configuration
	apiUrl := os.Getenv(envSSApiUrl)
	if apiUrl == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSApiUrl))
	}
	dashKeyId := os.Getenv(envSSDashKeyId)
	if dashKeyId == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSDashKeyId))
	}
	dashKeyPass := os.Getenv(envSSDashSecret)
	if dashKeyPass == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSDashSecret))
	}

	// Set up client services
	authProvider := ssub.NewUserDashAuthProvider(dashKeyId, dashKeyPass)
	ssClient := ssub.NewSsub(apiUrl, authProvider)
	// Verify credentials
	me, err := ssClient.UserDashboards.RetrieveThisDash()
	if err != nil {
		panic(fmt.Sprintf("error retrieving dashboard: %s", err))
	}
	if me == nil {
		panic("no reporter found")
	}

	// Create and start the app
	myApp, err := httpsvc.NewApp(ssClient, me)
	if err != nil {
		panic(fmt.Sprintf("error creating app: %s", err))
	}
	myApp.Start()

	// Create a channel to receive OS signals
	signalChan := make(chan os.Signal, 1)

	// Signal to wait for `Ctrl+C`
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	// Wait for the signal
	fmt.Printf("Press Ctrl+C to stop\n")
	sig := <-signalChan
	fmt.Println("Received signal:", sig)
	_ = myApp.Stop()

	// Exit the program
	os.Exit(0)
}
