package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/sc_dash/view"
	"net/http"
)

type ErrorHandler struct {
	templates *view.Templates
}

func newErrorHandler(templates *view.Templates) *ErrorHandler {
	return &ErrorHandler{
		templates: templates,
	}
}

func (eh *ErrorHandler) ErrorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	c.Logger().Error(err)

	data := make(map[string]interface{})
	data["code"] = code
	data["message"] = err.Error()

	err = eh.templates.RenderError(c, data)

	if err != nil {
		c.Logger().Error(err)
	}
	c.NoContent(code)
}
