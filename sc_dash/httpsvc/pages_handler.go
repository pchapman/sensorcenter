package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/sc_dash/view"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"net/http"
	"time"
)

const (
	homeURI         = "/"
	paramCollection = "collection"
	statusURI       = "/status"
)

type pagesHandler struct {
	scClient  *ssub.Ssub
	templates *view.Templates
	userDash  *scom.UserDash
}

func newPagesHandler(scClient *ssub.Ssub, templates *view.Templates, userDash *scom.UserDash) *pagesHandler {
	return &pagesHandler{
		scClient:  scClient,
		templates: templates,
		userDash:  userDash,
	}
}

func (ph *pagesHandler) registerRoutes(e *echo.Echo) {
	e.GET(homeURI, ph.homeHandler)
	e.GET(statusURI, ph.statusFragmentHandler)
}

func (ph *pagesHandler) homeHandler(c echo.Context) error {
	sensorReadings, err := ph.scClient.Locations.RetrieveLocationReadings(ph.userDash.LocationId)
	if err != nil {
		return err
	}
	sensorReadings.Reported = time.Now()
	return ph.templates.Render(c, "home.html", sensorReadings)
}

func (ph *pagesHandler) statusFragmentHandler(c echo.Context) error {
	outputJson := false
	if c.Request().Header.Get("HX-Request") == "" {
		if c.Request().Header.Get("Accept") == "application/json" {
			outputJson = true
		} else {
			// This is not an HTMX AJAX request, redirect to the home page
			return c.Redirect(http.StatusTemporaryRedirect, homeURI)
		}
	}
	sensorReadings, err := ph.scClient.Locations.RetrieveLocationReadings(ph.userDash.LocationId)
	if err != nil {
		return err
	}
	sensorReadings.Reported = time.Now()
	if outputJson {
		return c.JSON(http.StatusOK, sensorReadings)
	}
	return ph.templates.Render(c, "status_frag.html", sensorReadings)
}
