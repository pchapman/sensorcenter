package httpsvc

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/pchapman/go-utils/misc"
	"gitlab.com/pchapman/sensorcenter/sc_dash/view"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"log"
	"net/http"
	"os"
)

type App struct {
	listenAddr string
	port       int32
	echo       *echo.Echo
}

func NewApp(scClient *ssub.Ssub, reporter *scom.UserDash) (*App, error) {
	listenAddr := os.Getenv(config.EnvListenAddress)
	if listenAddr == "" {
		listenAddr = "0.0.0.0"
	}
	port, ok := misc.Int32Env(config.EnvListenPort)
	if !ok {
		port = 8080
	}

	e := echo.New()

	assetHandler := http.FileServerFS(view.Static())
	e.GET("/static/*", echo.WrapHandler(assetHandler))

	tmpls, err := view.NewTemplates()
	if err != nil {
		e.Logger.Fatalf("failed to create templates: %s", err)
	}
	eh := newErrorHandler(tmpls)
	e.HTTPErrorHandler = eh.ErrorHandler

	// Set up Middlewares
	e.Use(middleware.Logger())

	// Routes
	ph := newPagesHandler(scClient, tmpls, reporter)
	ph.registerRoutes(e)

	return &App{
		listenAddr: listenAddr,
		port:       port,
		echo:       e,
	}, nil
}

func (app *App) Start() {
	go func() {
		err := app.echo.Start(fmt.Sprintf("%s:%d", app.listenAddr, app.port))
		if err != nil {
			log.Printf("Unable to start HTTP server: %s", err)
		}
	}()
}

func (app *App) Stop() error {
	return app.echo.Shutdown(context.Background())
}
