package view

import (
	"embed"
	"fmt"
	"github.com/labstack/echo/v4"
	"html/template"
	"log/slog"
	"time"
)

//go:embed static/*
var static embed.FS

//go:embed templates/*
var templates embed.FS

func Static() embed.FS {
	return static
}

type Templates struct {
	templates *template.Template
}

func NewTemplates() (*Templates, error) {
	//	t, err := template.New("").ParseFS(templates, "templates/*.html")
	t, err := template.New("").Funcs(funcMap).ParseFS(templates, "templates/*html", "templates/fragments/*")
	if err != nil {
		return nil, err
	}

	for _, tpl := range t.Templates() {
		slog.Info(fmt.Sprintf("Loaded template %s", tpl.Name()))
	}

	return &Templates{templates: t}, nil
}

func (t *Templates) Render(ctx echo.Context, name string, data interface{}) error {
	return t.templates.ExecuteTemplate(ctx.Response().Writer, name, data)
}

func (t *Templates) RenderError(c echo.Context, data interface{}) error {
	return t.Render(c, "error.html", data)
}

var funcMap = template.FuncMap{
	"unixTime": func(v int64) time.Time {
		return time.Unix(v, 0)
	},
}
