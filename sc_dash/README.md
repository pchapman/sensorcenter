# SensorCenter Dashboard

SensorCenter Dashboard is a webapp server that shows sensor readings for a given location.

## Notes

SensorCenter Dashboard uses the [minicss](https://minicss.us/) css library rather than the larger library used by SDom.

## Installing SensorCenter Reporter with SystemD

1. Create system user and group `scdash`:  `sudo useradd scdash -s /sbin/nologin -M`
2. Copy sc_reporter executable into `/usr/local/bin`
3. Copy scdash.service into `/etc/systemd/system`
4. Ensure permissions for systemd file: `sudo chmod 750 /etc/systemd/system/scdash.service`
5. Refresh systemd: `systemctl daemon-reload`
6. Edit the configuration to modify environment variables: `sudo systemctl edit scdash.service`
7. Enable the service: `sudo systemctl enable scdash.service`
8. Start the service: `sudo systemctl start scdash.service`
9. Verify the service is running by checking logs: `sudo journalctl -f -u scdash.service`
