package scom

import "time"

// Status represents the status of the application
type Status struct {
	Application *string    `json:"application" yaml:"application"`
	Version     *string    `json:"version" yaml:"version"`
	Environment *string    `json:"environment" yaml:"environment"`
	UpSince     *time.Time `json:"upSince" yaml:"upSince"`
}

const PathGetStatus = "/api/"
