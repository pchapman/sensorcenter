package scom

// Reporter represents a source of sensor readings and data for verifying a reporter's credentials
type Reporter struct {
	Id         int64  `form:"id" json:"id" yaml:"id"`
	KeyId      string `form:"keyId" json:"keyId" yaml:"keyId"`
	LocationId int64  `form:"locationId" json:"locationId" yaml:"locationId"`
	Name       string `form:"name" json:"name" yaml:"name"`
	AuthHash   string `form:"-" json:"-" yaml:"-"`
}

const (
	PathGetReporter = "/api/reporter/:" + ParamReporterId
)
