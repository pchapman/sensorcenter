package scom

import (
	"context"
	"fmt"
	"github.com/adhocore/gronx"
	"github.com/adhocore/gronx/pkg/tasker"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"
)

type SensorCheck func() error

type Scheduler interface {
	ScheduleSensorChecks(sensor *Sensor, checkFunc SensorCheck) error
	Start()
	Stop()
}

type scheduler struct {
	running bool
	tickers []time.Ticker
	tr      *tasker.Tasker
	sync.RWMutex
}

func NewScheduler() Scheduler {
	return &scheduler{
		running: false,
		tickers: make([]time.Ticker, 0),
		tr:      nil,
	}
}

func (s *scheduler) ScheduleSensorChecks(sensor *Sensor, checkFunc SensorCheck) error {
	if strings.HasPrefix(sensor.Schedule, "cron: ") {
		s.Lock()
		defer s.Unlock()
		sched := sensor.Schedule[9:]
		if s.tr == nil {
			s.tr = tasker.New(tasker.Option{})
		}
		s.tr.Task(sched, func(ctx context.Context) (int, error) {
			return 0, checkFunc()
		}, false)
	} else {
		d := ScheduleDuration(sensor.Schedule)
		if d > 0 {
			s.Lock()
			defer s.Unlock()
			t := time.NewTicker(d)
			go func() {
				for range t.C {
					s.RLock()
					if s.running {
						s.RUnlock()
						err := checkFunc()
						if err != nil {
							log.Printf("Error checking sensor %d: %s", sensor.Id, err)
						}
					} else {
						s.RUnlock()
					}
				}
			}()
		} else {
			return fmt.Errorf("invalid schedule: %s", sensor.Schedule)
		}
	}
	return nil
}

func (s *scheduler) Start() {
	s.Lock()
	defer s.Unlock()
	s.running = true
	if s.tr != nil {
		go s.tr.Run()
	}
}

func (s *scheduler) Stop() {
	s.Lock()
	defer s.Unlock()
	s.running = false
	for _, t := range s.tickers {
		t.Stop()
	}
	if s.tr != nil {
		s.tr.Stop()
	}
}

func ValidateSchedule(schedule string) bool {
	if strings.HasPrefix(schedule, "cron: ") {
		return gronx.New().IsValid(schedule[6:])
	}
	return ScheduleDuration(schedule) > 0
}

func ScheduleDuration(schedule string) time.Duration {
	if strings.HasPrefix(schedule, "@every ") {
		schedule = schedule[7:]
		d, err := duration(schedule)
		if err != nil {
			return 0
		}
		return d
	}

	return 0
}

func duration(schedule string) (time.Duration, error) {
	var s string
	var found bool
	if found, s = removeSuffix(schedule, " days"); found {
		return calculateDuration(s, time.Hour*24)
	}
	if found, s = removeSuffix(schedule, " hours"); found {
		return calculateDuration(s, time.Hour)
	}
	if found, s = removeSuffix(schedule, " minutes"); found {
		return calculateDuration(s, time.Minute)
	}
	if found, s = removeSuffix(schedule, " milliseconds"); found {
		return calculateDuration(s, time.Millisecond)
	}
	if found, s = removeSuffix(schedule, " nanoseconds"); found {
		return calculateDuration(s, time.Nanosecond)
	}
	if found, s = removeSuffix(schedule, " seconds"); found {
		return calculateDuration(s, time.Second)
	}
	// else, we assume it's a number of milliseconds
	return calculateDuration(schedule, time.Millisecond)
}

func calculateDuration(s string, multiplier time.Duration) (time.Duration, error) {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, err
	}
	return time.Duration(i) * multiplier, nil
}

func removeSuffix(s, suffix string) (bool, string) {
	if strings.HasSuffix(s, suffix) {
		return true, s[:len(s)-len(suffix)]
	}
	return false, s
}
