package scom

type Rule struct {
	Id                int64  `form:"id" json:"id" yaml:"id"`
	UserId            string `form:"userId" json:"userId" yaml:"userId"`
	SensorId          int64  `form:"sensorId" json:"sensorId" yaml:"sensorId"`
	NotificationTopic string `form:"notificationTopic" json:"notificationTopic" yaml:"notificationTopic"`
	Script            string `form:"script" json:"script" yaml:"script"`
	State             string `form:"-" json:"-" yaml:"-"`
}
