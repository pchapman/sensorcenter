package scom

// CtoF converts a temperature value from Celsius to Fahrenheit
func CtoF(c float32) float32 {
	return c*1.8 + 32
}

// FtoC converts a temperature value from Fahrenheit to Celsius
func FtoC(f float32) float32 {
	return (f - 32) / 1.8
}
