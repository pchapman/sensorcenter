package scom

import "testing"

func TestValidateSchedule(t *testing.T) {
	// Happy path
	if !ValidateSchedule("cron: 24 23 22 21 1 *") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 days") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 hours") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 minutes") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 milliseconds") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 nanoseconds") {
		t.Error("failed")
	}
	if !ValidateSchedule("@every 1 seconds") {
		t.Error("failed")
	}

	// Unhappy path
	if ValidateSchedule("interrupt") {
		t.Error("failed")
	}
	if ValidateSchedule("cron: 200 23 22 21 20 *") {
		t.Error("failed")
	}
	if ValidateSchedule("cron: 20 23 22 21 20 2 *") {
		t.Error("failed")
	}
	if ValidateSchedule("cron:20 23 22 21 20 *") {
		t.Error("failed")
	}
	if ValidateSchedule("@every1 minutes") {
		t.Error("failed")
	}
	if ValidateSchedule("@every 1milliseconds") {
		t.Error("failed")
	}
	if ValidateSchedule("every 1 nanoseconds") {
		t.Error("failed")
	}
}
