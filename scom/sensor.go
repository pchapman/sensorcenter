package scom

import (
	"encoding/json"
	"fmt"
	"github.com/casbin/govaluate"
	"gitlab.com/pchapman/go-utils/misc"
	"strconv"
	"strings"
	"time"
)

type SensorType string

const (
	ST_Angle       = SensorType("angle")
	ST_Current     = SensorType("current")
	ST_Humidity    = SensorType("humidity")
	ST_Position    = SensorType("position")
	ST_Pressure    = SensorType("pressure")
	ST_Resistance  = SensorType("resistance")
	ST_RPM         = SensorType("rpm")
	ST_Speed       = SensorType("speed")
	ST_Switch      = SensorType("switch")
	ST_Temperature = SensorType("temperature")
	ST_Voltage     = SensorType("voltage")
)

func SensorTypes() []SensorType {
	return []SensorType{ST_Angle, ST_Current, ST_Humidity, ST_Position, ST_Pressure, ST_Resistance, ST_RPM, ST_Speed, ST_Switch, ST_Temperature, ST_Voltage}
}

func ValidateSensorType(s string) bool {
	t := SensorType(s)
	switch t {
	case ST_Angle, ST_Current, ST_Humidity, ST_Position, ST_Pressure, ST_Resistance, ST_RPM, ST_Speed, ST_Switch, ST_Temperature, ST_Voltage:
		return true
	}
	return false
}

func (s SensorType) ProperCase() string {
	str := string(s)
	return strings.ToUpper(str[0:1]) + str[1:]
}

func (s SensorType) String() string {
	return string(s)
}

// Sensor represents a sensor from which data is obtained.  Type indicates the type of sensor being monitored.
// StoreUnit is the unit in which sensor readings are stored.  DisplayUnit is the unit in which sensor readings are
// to be displayed.  DisplayConversion is a govaluate expression that can be used to convert sensor readings from the
// stored value to the display value.  If DisplayConversion is empty string, the stored value is used as the display
// value.  See https://github.com/Knetic/govaluate for more information on govaluate.
type Sensor struct {
	Id                int64      `form:"id" json:"id" yaml:"id"`
	LocationId        int64      `form:"locationId" json:"locationId" yaml:"locationId"`
	ReporterId        int64      `form:"reporterId" json:"reporterId" yaml:"reporterId"`
	Name              string     `form:"name" json:"name" yaml:"name"`
	Type              SensorType `form:"type" json:"type" yaml:"type"`
	Schedule          string     `form:"schedule" json:"schedule" yaml:"schedule"`
	ConfigData        string     `form:"configData" json:"configData" yaml:"configData"`
	StoreUnit         string     `form:"storeUnit" json:"storeUnit" yaml:"storeUnit"`
	DisplayUnit       string     `form:"displayUnit" json:"displayUnit" yaml:"displayUnit"`
	DisplayConversion string     `form:"displayConversion" json:"displayConversion" yaml:"displayConversion"`
	DisplaySigDigits  *int32     `form:"displaySigDigits" json:"displaySigDigits" yaml:"displaySigDigits"`
}

func ValidateDisplayConversion(s string) (valid bool, reason string) {
	if s == "" {
		return true, ""
	}
	// Ensure the variable named "value" is used
	if !strings.Contains(s, "value") {
		return false, "Display conversion expression does not reference the variable name \"value\""
	}
	_, err := govaluate.NewEvaluableExpression(s)
	if err != nil {
		return false, err.Error()
	}

	return true, ""
}

func (s *Sensor) GetConfigData() (data map[string]string, err error) {
	err = json.Unmarshal([]byte(s.ConfigData), &data)
	return
}

func (s *Sensor) SetConfigData(data map[string]string) (err error) {
	ba, err := json.Marshal(data)
	if err != nil {
		return
	}
	s.ConfigData = string(ba)
	return
}

func (s *Sensor) Copy() *Sensor {
	ss := &Sensor{
		Id:                s.Id,
		LocationId:        s.LocationId,
		ReporterId:        s.ReporterId,
		Name:              s.Name,
		Type:              s.Type,
		Schedule:          s.Schedule,
		ConfigData:        s.ConfigData,
		StoreUnit:         s.StoreUnit,
		DisplayUnit:       s.DisplayUnit,
		DisplayConversion: s.DisplayConversion,
	}
	if s.DisplaySigDigits != nil {
		ss.DisplaySigDigits = misc.PInt32(*s.DisplaySigDigits)
	}
	return ss
}

// SensorReading represents a sensor reading.
type SensorReading struct {
	Id           int64   `json:"id" yaml:"id"`
	LocationId   int64   `json:"locationId" yaml:"locationId"`
	SensorId     int64   `json:"sensorId" yaml:"sensorId"`
	Timestamp    int64   `json:"timestamp" yaml:"timestamp"`
	Value        float64 `json:"value" yaml:"value"`
	DisplayValue float64 `json:"displayValue" yaml:"displayValue"`
}

func NewSensorReading(sensor *Sensor, timestamp int64, value float64) (*SensorReading, error) {
	sr := &SensorReading{
		LocationId:   sensor.LocationId,
		SensorId:     sensor.Id,
		Value:        value,
		DisplayValue: value,
	}
	if timestamp == 0 {
		sr.Timestamp = time.Now().UTC().Unix()
	} else {
		sr.Timestamp = timestamp
	}
	if sensor.DisplayConversion == "" {
		sr.DisplayValue = sr.Value
	} else {
		expr, err := govaluate.NewEvaluableExpression(sensor.DisplayConversion)
		if err != nil {
			return nil, err
		}
		params := map[string]interface{}{
			"value": sr.Value,
		}
		v, err := expr.Evaluate(params)
		if err != nil {
			return nil, err
		}
		var ok bool
		sr.DisplayValue, ok = v.(float64)
		if !ok {
			return nil, fmt.Errorf("failed to convert %v to float64", v)
		}
	}
	return sr, nil
}

type SensorReadingElem struct {
	Sensor  *Sensor        `json:"sensor" yaml:"sensor"`
	Reading *SensorReading `json:"reading" yaml:"reading"`
}

func (elem *SensorReadingElem) DisplayString() string {
	if elem.Sensor.Type == ST_Switch {
		if elem.Reading.DisplayValue == 0.0 {
			return fmt.Sprintf("Is Not %s", elem.Sensor.DisplayUnit)
		}
		return fmt.Sprintf("Is %s", elem.Sensor.DisplayUnit)
	}
	if elem.Sensor.DisplaySigDigits == nil {
		return fmt.Sprintf("%f %s", elem.Reading.DisplayValue, elem.Sensor.DisplayUnit)
	}
	return fmt.Sprintf("%s %s",
		strconv.FormatFloat(elem.Reading.DisplayValue, 'f', int(*elem.Sensor.DisplaySigDigits), 64),
		elem.Sensor.DisplayUnit)
}

// const PathGetSensor = "/sensor/:" + ParamSensorId
// const PathPostNewSensor = "/sensor"
// const PathPutSensorUpdate = "/sensor/:" + ParamSensorId
// const PathPostSensorSearch = "/sensors"
const PathGetAllSensors = "/api/sensors"

// const PathGetLastReading = "/sensor/:" + ParamSensorId + "/reading"
const PathPostNewReading = "/api/sensor/:" + ParamSensorId + "/reading"

//const PathPostReadingSearch = "/readings"
