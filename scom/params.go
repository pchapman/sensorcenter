package scom

const (
	ParamLocationId = "locationId"
	ParamName       = "name"
	ParamReporterId = "reporterId"
	ParamSensorId   = "sensorId"
	ParamValue      = "value"
	ParamTimestamp  = "timestamp"
	ParamUserDashId = "dashId"
)
