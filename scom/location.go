package scom

import "time"

// Location is a location where sensors are located.
type Location struct {
	Id                int64   `form:"id" json:"id" yaml:"id"`
	Name              string  `form:"name" json:"name" yaml:"name"`
	Description       *string `form:"description" json:"description" yaml:"description"`
	AddressLine1      *string `form:"addressLine1" json:"addressLine1" yaml:"addressLine1"`
	AddressLine2      *string `form:"addressLine2" json:"addressLine2" yaml:"addressLine2"`
	AddressCity       *string `form:"addressCity" json:"addressCity" yaml:"addressCity"`
	AddressRegion     *string `form:"addressRegion" json:"addressRegion" yaml:"addressRegion"`
	AddressPostalCode *string `form:"addressPostalCode" json:"addressPostalCode" yaml:"addressPostalCode"`
	AddressCountry    *string `form:"addressCountry" json:"addressCountry" yaml:"addressCountry"`

	Longitude *float32 `form:"longitude" json:"longitude" yaml:"longitude"`
	Latitude  *float32 `form:"latitude" json:"latitude" yaml:"latitude"`
}

type LocationSensorReadings struct {
	LocationId   int64               `json:"locationId" yaml:"locationId"`
	LocationName string              `json:"locationName" yaml:"locationName"`
	Reported     time.Time           `json:"reported" yaml:"reported"`
	Readings     []SensorReadingElem `json:"readings" yaml:"readings"`
}

// UserLocation is a relationship between a user and a location. Only users associated with a location can read that
// location's data.
type UserLocation struct {
	Id         int64 `json:"id" yaml:"id"`
	LocationId int64 `json:"locationId" yaml:"locationId"`
	UserId     int64 `json:"userId" yaml:"userId"`
}

const PathGetLocation = "/api/location/:" + ParamLocationId

// const PathPostNewLocation = "/location"
// const PathPutLocation = "/location/:" + ParamLocationId
const PathGetAllLocations = "/api/locations"

// const PathPostLocationSearch = "/locations"
const PathGetLocationReadings = "/api/locations/:" + ParamLocationId + "/readings"
