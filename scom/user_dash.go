package scom

type UserDash struct {
	Id         int64  `form:"id" json:"id" yaml:"id"`
	UserId     string `form:"userId" json:"userId" yaml:"userId"`
	LocationId int64  `form:"locationId" json:"locationId" yaml:"locationId"`
	KeyId      string `form:"keyId" json:"keyId" yaml:"keyId"`
	AuthHash   string `form:"-" json:"-" yaml:"-"`
}

const (
	PathGetUserDash = "/api/userdash/:" + ParamUserDashId
)
