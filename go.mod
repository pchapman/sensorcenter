module gitlab.com/pchapman/sensorcenter

//replace gitlab.com/pchapman/go-utils v1.0.0 => ../go-utils

require (
	github.com/adhocore/gronx v1.8.1
	github.com/aws/aws-sdk-go v1.54.5
	github.com/casbin/govaluate v1.1.1
	github.com/google/uuid v1.6.0
	github.com/gorilla/sessions v1.3.0
	github.com/hectormalot/omgo v0.1.3
	github.com/labstack/echo-contrib v0.17.1
	github.com/labstack/echo/v4 v4.12.0
	github.com/lib/pq v1.10.9
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/robertkrimen/otto v0.4.0
	github.com/spf13/cobra v1.8.1
	github.com/stianeikeland/go-rpio/v4 v4.6.0
	gitlab.com/pchapman/go-utils v1.1.0
	gitlab.com/pchapman/weatherflow_go v0.0.0-20240605220957-78a897f59fef
	golang.org/x/crypto v0.24.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/gorilla/context v1.1.2 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/labstack/gommon v0.4.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	github.com/warthog618/go-gpiocdev v0.9.0 // indirect
	golang.org/x/net v0.26.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)

go 1.21

toolchain go1.22.4
