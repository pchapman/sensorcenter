# SDom

SDom is the central server for SensorCenter.  It has two functions.  The primary function is a central place for
reporters to report sensor readings and for dashboards to obtain the latest readings.  The second function is as a
webapp for users to be able to check on and maintain their SensorCenter configuration and data.

## Notes

SDom uses the [bulma](https://bulma.io/) CSS library.
Icons come from [flaticon.com](https://www.flaticon.com).  Icons with solid weight and of type uicon were searched and
used: `https://www.flaticon.com/search?weight=solid&type=uicon&word=some%20search%20text`

## Installing SDOM with SystemD

1. Create system user and group `sdom`:  `sudo useradd sdom -s /sbin/nologin -M`
2. Copy sdom executable into `/usr/local/bin`
3. Copy sdom.service into `/etc/systemd/system`
4. Ensure permissions for systemd file: `sudo chmod 750 /etc/systemd/system/sdom.service`
5. Refresh systemd: `systemctl daemon-reload`
6. Edit the configuration to modify environment variables: `sudo systemctl edit sdom.service`
7. Enable the service: `sudo systemctl enable sdom.service`
8. Start the service: `sudo systemctl start sdom.service`
9. Verify the service is running by checking logs: `sudo journalctl -f -u sdom.service`
