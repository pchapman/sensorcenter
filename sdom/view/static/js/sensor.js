// If this is a new sensor, set display and store units to the default values when a new sensor type value is selected.
function sensorTypeChanged() {
    var sel = document.getElementById("sensor-type");
    var val = sel.options[sel.selectedIndex];
    if (val === undefined) {
        return;
    }
    var dfltDisp = val.getAttribute("default-displayed-unit");
    var dfltStore = val.getAttribute("default-stored-unit");
    var helpTxt = val.getAttribute("help-text");
    if (document.getElementById("sensor-id").value === "0") {
        document.getElementById("displayed-unit").value = dfltDisp;
        document.getElementById("stored-unit").value = dfltStore;
    }
    if (helpTxt === "") {
        document.getElementById("config-data-help").innerHTML =
            "There is currently, no documentation on how to configure this type of sensor.";
    } else {
        document.getElementById("config-data-help").innerHTML =
            "For documentation on how to configure this type of sensor, see documentation <a href='" +
            helpTxt + "' target='_blank'>here</a>.";
    }
}

// createReporter will make an ajax call to PUT to /location/{locationId}/reporter.  It will then show a modal with the
// key ID and new secret
function createReporter(locationId) {
    let name = document.getElementById("reporter-name").value
    if (name === "") {
        alert("Reporter name cannot be empty");
        return;
    }
    let body = new FormData();
    body.set("name", name)
    fetch("/location/" + locationId + "/reporter?name=" + name, {
        method: "PUT",
        headers: {
            "Accept": "application/json",
        },
        body: body
    }).then((response) => {
        if (response.status === 200) {
            response.json().then((data) => {
                /* Add and select new reporter option
                   Expected response from services:

                   {
                        "id":     r.Id,
                        "key":    r.KeyId,
                        "secret": secret,
                    }


                    Expected new option in page:

                    <option value="{{.Id}}" {{if eq .Id .Sensor.ReporterId}} selected="selected" {{end}}
                    >{{.Name}}</option>
                */
                let sel = document.getElementById("reporter-id")
                let opt = document.createElement("option");
                opt.value = data.id;
                opt.selected = true;
                opt.innerText = name;
                sel.appendChild(opt);
                sel.value = data.id;
                document.getElementById("reporter-name").value = "";
                openSecretModal("Reporter", data.key, data.secret);
            })
        }
    }).catch((error) => {
        console.log(error)
    });
}

window.onload = sensorTypeChanged;
