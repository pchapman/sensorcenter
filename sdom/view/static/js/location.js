/* *** Secret Modal *** */

function closeSecretModal() {
    document.getElementById("secret-modal").classList.remove("is-active");
}

function openSecretModal(owner, key, secret) {
    document.getElementById("copy-msg").innerHTML = "";
    document.getElementById("secret-owner").innerText = owner;
    document.getElementById("key-value").innerText = key;
    document.getElementById("secret-value").innerText = secret;
    document.getElementById("secret-modal").classList.add("is-active");
}

/* *** User Dashboards *** */

// createUserDash will make an ajax call to PUT to /location/{locationId}/dash.  It will then show a modal with the key
// ID and new secret
function createDashSecret(locationId) {
    fetch("/location/" + locationId + "/dash", {
        method: "PUT",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then((response) => {
        if (response.status === 200) {
            response.json().then((data) => {
                openSecretModal("User Dashboard", data.key, data.secret);
                // Update list of dashboards
                htmx.ajax("GET", "/location/" + locationId + "/dash", {
                    target: "#user-dashboards-table",
                    swap: "outerHTML"
                });
            })
        }
    }).catch((error) => {
        console.log(error)
    });
}


// resetDashSecret will make an ajax call to /location/{locationId}/dash/{dashId}/reset.  It will then show a modal
// with the key ID and new secret
function resetDashSecret(locationId, dashId) {
    fetch("/location/" + locationId + "/dash/" + dashId + "/reset", {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then((response) => {
        if (response.status === 200) {
            response.json().then((data) => {
                openSecretModal("User Dashboard", data.key, data.secret);
            })
        }
    }).catch((error) => {
        console.log(error)
    });
}

/* *** Reporters *** */

// createReporter will make an ajax call to PUT to /location/{locationId}/reporter.  It will then show a modal with the
// key ID and new secret
function createReporter(locationId) {
    let name = document.getElementById("reporter-name").value
    if (name === "") {
        alert("Reporter name cannot be empty");
        return;
    }
    let body = new FormData();
    body.set("name", name)
    fetch("/location/" + locationId + "/reporter?name=" + name, {
        method: "PUT",
        headers: {
            "Accept": "application/json",
        },
        body: body
    }).then((response) => {
        if (response.status === 200) {
            response.json().then((data) => {
                openSecretModal("Reporter", data.key, data.secret);
                // Update list of dashboards
                htmx.ajax("GET", "/location/" + locationId + "/reporter", {
                    target: "#reporters-table",
                    swap: "outerHTML"
                });
            })
        }
    }).catch((error) => {
        console.log(error)
    });
}

// resetReporterSecret will make an ajax call to /location/{locationId}/reporter/{dashId}/reset.  It will then show a
// modal with the key ID and new secret
function resetReporterSecret(locationId, reporterId) {
    fetch("/location/" + locationId + "/reporter/" + reporterId + "/reset", {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    }).then((response) => {
        if (response.status === 200) {
            response.json().then((data) => {
                openSecretModal("Reporter", data.key, data.secret);
            })
        }
    }).catch((error) => {
        console.log(error)
    });
}
