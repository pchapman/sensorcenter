// copyToKeyboard will copy the value inside the element with id copyFrom to the clipboard.  It will then display a
// message which includes the value name inside the element with id msgTo in order to let the user know it was copied.
// If msgTo is !alert, then an alert will be shown instead.
function copyToKeyboard(copyFrom, msgTo, valueName) {
    let elem = document.getElementById(copyFrom);
    let tagName = elem.tagName.toLowerCase();
    if (tagName === 'input' || tagName === 'textarea') {
        elem.select();
        elem.setSelectionRange(0, 99999); // For mobile devices
        navigator.clipboard.writeText(elem.value);
    } else {
        navigator.clipboard.writeText(elem.innerText);
    }

    let msg = "The " + valueName + " has been copied to clipboard";
    if (msgTo === "!alert") {
        alert(msg);
    } else {
        elem = document.getElementById(msgTo);
        tagName = elem.tagName.toLowerCase();
        if (tagName === 'input' || tagName === 'textarea') {
            elem.value = msg;
        } else {
            elem.innerText = msg;
        }
    }
}
