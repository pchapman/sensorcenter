package view

import (
	"embed"
	"fmt"
	"github.com/labstack/echo/v4"
	"html/template"
	"log/slog"
	"time"
)

//go:embed static/*
var static embed.FS

//go:embed templates/*
var templates embed.FS

func Static() embed.FS {
	return static
}

type Templates struct {
	templates *template.Template
}

func NewTemplates() (*Templates, error) {
	//	t, err := template.New("").ParseFS(templates, "templates/*.html")
	t, err := template.New("").Funcs(funcMap).ParseFS(templates, "templates/*html", "templates/fragments/*html")
	if err != nil {
		return nil, err
	}

	for _, tpl := range t.Templates() {
		slog.Info(fmt.Sprintf("Loaded template %s", tpl.Name()))
	}

	return &Templates{templates: t}, nil
}

func (t *Templates) Render(ctx echo.Context, name string, data interface{}) error {
	return t.templates.ExecuteTemplate(ctx.Response().Writer, name, data)
}

var funcMap = template.FuncMap{
	"nillableInt32": func(v *int32) string {
		if v == nil {
			return ""
		}
		return fmt.Sprintf("%d", *v)
	},
	"nillableInt64": func(v *int64) string {
		if v == nil {
			return ""
		}
		return fmt.Sprintf("%d", *v)
	},
	"nilToEmptyStr": func(v *string) string {
		if v == nil {
			return ""
		}
		return *v
	},
	"sprintf": fmt.Sprintf,
	"unixTime": func(v int64) time.Time {
		return time.Unix(v, 0)
	},
}
