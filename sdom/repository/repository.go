package repository

import "gitlab.com/pchapman/sensorcenter/scom"

type Repository interface {
	Close() (err error)
	DeleteLocationById(id int64) (err error)
	DeleteReporterById(id int64) error
	DeleteReportersForLocation(locationId int64) (err error)
	DeleteRulesForLocation(locationId int64) error
	DeleteRulesForSensor(sensorId int64) error
	DeleteSensorById(sensorId int64) error
	DeleteSensorReadingsForLocation(locationId int64) (err error)
	DeleteSensorReadingsForSensor(sensorId int64) error
	DeleteSensorsForLocation(locationId int64) (err error)
	DeleteUserDashboardById(id int64) error
	DeleteUserDashboardsForLocation(locationId int64) (err error)
	DeleteUserLocationsForLocation(locationId int64) (err error)
	InitializeDb() (err error)
	InsertLocation(location *scom.Location) (int64, error)
	InsertReporter(reporter *scom.Reporter) (int64, error)
	InsertSensor(sensor *scom.Sensor) (int64, error)
	InsertSensorReading(reading *scom.SensorReading) (int64, error)
	InsertUserDash(dash *scom.UserDash) (int64, error)
	InsertUserLocation(userId string, locationId int64) error
	LocationNameExists(name string) (exists bool, err error)
	ReporterKeyIdExists(externalId string) (bool, error)
	ReporterNameExists(name string) (bool, error)
	RetrieveAllLocations() ([]scom.Location, error)
	RetrieveAllSensors() ([]scom.Sensor, error)
	RetrieveLatestSensorReading(sensorId int64) (*scom.SensorReading, error)
	RetrieveLocationById(locationId int64) (*scom.Location, error)
	RetrieveLocationIDsForUser(id string) ([]int64, error)
	RetrieveLocationsForUser(id string) ([]scom.Location, error)
	RetrieveLocationByCoordinates(lat *float32, lon *float32) (*scom.Location, error)
	RetrieveLocationByName(name string) (*scom.Location, error)
	RetrieveReporterByKeyId(externalId string) (*scom.Reporter, error)
	RetrieveReporterById(reporterId int64) (*scom.Reporter, error)
	RetrieveReportersForLocation(locationId int64) ([]scom.Reporter, error)
	RetrieveRulesForSensor(sensorId int64) ([]scom.Rule, error)
	RetrieveSensorById(sensorId int64) (*scom.Sensor, error)
	RetrieveSensorByName(name string) (*scom.Sensor, error)
	RetrieveSensorIDsForReporter(reporterId int64) ([]int64, error)
	RetrieveSensorIDsForUser(id string) ([]int64, error)
	RetrieveSensorIDsForUserDash(dashId int64) ([]int64, error)
	RetrieveSensorsForLocation(locationId int64) ([]scom.Sensor, error)
	RetrieveSensorsForReporter(id int64) ([]scom.Sensor, error)
	RetrieveSensorsForUser(id string) ([]scom.Sensor, error)
	RetrieveUserDashById(id int64) (*scom.UserDash, error)
	RetrieveUserDashByKeyId(id string) (*scom.UserDash, error)
	RetrieveUserDashboardsByUser(userId string) ([]scom.UserDash, error)
	RetrieveUserDashboardsByUserAndLocation(userId string, locationId int64) ([]scom.UserDash, error)
	SensorNameExists(name string) (bool, error)
	UpdateLocation(location *scom.Location) (err error)
	UpdateReporterAuthHash(id int64, hash string) (err error)
	UpdateRule(rule *scom.Rule) (err error)
	UpdateSensor(sensor *scom.Sensor) (err error)
	UpdateUserDashAuthHash(id int64, hash string) error
}
