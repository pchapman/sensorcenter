package repository

import (
	"database/sql"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"os"
	"time"
)

type repository struct {
	db *sql.DB
}

func (r *repository) Close() error {
	return r.db.Close()
}

func (r *repository) InitializeDb() error {
	return executeUpdates(r.db)
}

func (r *repository) DeleteLocationById(id int64) error {
	var err error
	_, err = r.db.Exec(sqlDeleteLocation, id)
	return err
}

func (r *repository) DeleteReporterById(id int64) error {
	_, err := r.db.Exec(sqlDeleteReporterById, id)
	return err
}

func (r *repository) DeleteReportersForLocation(locationId int64) error {
	_, err := r.db.Exec(sqlDeleteReportersByLocation, locationId)
	return err
}

func (r *repository) DeleteRulesForLocation(locationId int64) error {
	_, err := r.db.Exec(sqlDeleteRulesByLocation, locationId)
	return err
}

func (r *repository) DeleteRulesForSensor(sensorId int64) error {
	_, err := r.db.Exec(sqlDeleteRulesBySensor, sensorId)
	return err
}

func (r *repository) DeleteSensorReadingsForLocation(locationId int64) error {
	_, err := r.db.Exec(sqlDeleteSensorReadingsByLocation, locationId)
	return err
}

func (r *repository) DeleteSensorReadingsForSensor(sensorId int64) error {
	_, err := r.db.Exec(sqlDeleteSensorReadingsBySensor, sensorId)
	return err
}

func (r *repository) DeleteSensorById(sensorId int64) error {
	_, err := r.db.Exec(sqlDeleteSensorById, sensorId)
	return err
}

func (r *repository) DeleteSensorsForLocation(locationId int64) error {
	_, err := r.db.Exec(sqlDeleteSensorsByLocation, locationId)
	return err
}

func (r *repository) DeleteUserDashboardById(id int64) error {
	_, err := r.db.Exec(sqlDeleteUserDashboardById, id)
	return err
}

func (r *repository) DeleteUserDashboardsForLocation(locationId int64) error {
	_, err := r.db.Exec(sqlDeleteUserDashboardsByLocation, locationId)
	return err
}

func (r *repository) DeleteUserLocationsForLocation(locationId int64) (err error) {
	_, err = r.db.Exec(sqlDeleteUserLocationsByLocation, locationId)
	return
}

func (r *repository) InsertLocation(loc *scom.Location) (id int64, err error) {
	rst := r.db.QueryRow(
		sqlInsLocation, loc.Name, loc.Description, loc.AddressLine1, loc.AddressLine2, loc.AddressCity,
		loc.AddressRegion, loc.AddressPostalCode, loc.AddressCountry, loc.Latitude, loc.Longitude)
	if rst.Err() != nil {
		return 0, rst.Err()
	}
	err = rst.Scan(&id)
	return
}

func (r *repository) InsertReporter(rptr *scom.Reporter) (id int64, err error) {
	rst := r.db.QueryRow(sqlInsReporter, rptr.KeyId, rptr.LocationId, rptr.Name, rptr.AuthHash)
	if rst.Err() != nil {
		return 0, rst.Err()
	}
	err = rst.Scan(&id)
	return
}

func (r *repository) InsertSensor(sensor *scom.Sensor) (id int64, err error) {
	rst := r.db.QueryRow(sqlInsSensor, sensor.LocationId, sensor.ReporterId, sensor.Name, sensor.Type,
		sensor.Schedule, sensor.ConfigData, sensor.StoreUnit, sensor.DisplayUnit, sensor.DisplayConversion,
		sensor.DisplaySigDigits)
	if rst.Err() != nil {
		return 0, rst.Err()
	}
	err = rst.Scan(&id)
	return
}

func (r *repository) InsertSensorReading(reading *scom.SensorReading) (id int64, err error) {
	ts := time.Unix(reading.Timestamp, 0)
	rst := r.db.QueryRow(sqlInsSensorReading, reading.LocationId, reading.SensorId, ts, ts, reading.Value,
		reading.DisplayValue)
	if rst.Err() != nil {
		return 0, rst.Err()
	}
	err = rst.Scan(&id)
	return
}

func (r *repository) InsertUserDash(dash *scom.UserDash) (id int64, err error) {
	row := r.db.QueryRow(sqlInsUserDash, dash.UserId, dash.LocationId, dash.KeyId, dash.AuthHash)
	if err != nil {
		return 0, err
	}
	err = row.Scan(&id)
	return
}

func (r *repository) InsertUserLocation(userId string, locationId int64) (err error) {
	_, err = r.db.Exec(sqlInsUserLocation, userId, locationId)
	return
}

func (r *repository) LocationNameExists(name string) (exists bool, err error) {
	rst := r.db.QueryRow(sqlSelLocationNameExists, name)
	if rst.Err() != nil {
		return false, rst.Err()
	}
	err = rst.Scan(&exists)
	return
}

func (r *repository) ReporterKeyIdExists(externId string) (exists bool, err error) {
	rst := r.db.QueryRow(sqlSelReporterKeyIdExists, externId)
	if rst.Err() != nil {
		return false, rst.Err()
	}
	err = rst.Scan(&exists)
	return
}

func (r *repository) ReporterNameExists(name string) (exists bool, err error) {
	rst := r.db.QueryRow(sqlSelReporterNameExists, name)
	if rst.Err() != nil {
		return false, rst.Err()
	}
	err = rst.Scan(&exists)
	return
}

func (r *repository) RetrieveAllLocations() ([]scom.Location, error) {
	rows, err := r.db.Query(sqlSelAllLocations)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	locations := make([]scom.Location, 0)
	for rows.Next() {
		loc, err := scanLocationRow(rows)
		if err != nil {
			return nil, err
		}
		locations = append(locations, *loc)
	}
	return locations, nil
}

func (r *repository) RetrieveAllSensors() ([]scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelAllSensors)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	sensors := make([]scom.Sensor, 0)
	for rows.Next() {
		sensor, err := scanSensorRow(rows)
		if err != nil {
			return nil, err
		}
		sensors = append(sensors, *sensor)
	}
	return sensors, nil
}

func (r *repository) RetrieveLatestSensorReading(sensorId int64) (*scom.SensorReading, error) {
	rows, err := r.db.Query(sqlSelLatestSensorReading, sensorId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanSensorReadingRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveLocationByCoordinates(lat *float32, lon *float32) (*scom.Location, error) {
	if lat == nil || lon == nil {
		return nil, nil
	}
	rows, err := r.db.Query(sqlSelLocationByCoordinates, lat, lon)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		loc, err := scanLocationRow(rows)
		if err != nil {
			return nil, err
		}
		return loc, nil
	}
	return nil, nil
}

func (r *repository) RetrieveLocationById(locationId int64) (*scom.Location, error) {
	rows, err := r.db.Query(sqlSelLocationById, locationId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		loc, err := scanLocationRow(rows)
		if err != nil {
			return nil, err
		}
		return loc, nil
	}
	return nil, nil
}

func (r *repository) RetrieveLocationByName(name string) (*scom.Location, error) {
	rows, err := r.db.Query(sqlSelLocationByName, name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		loc, err := scanLocationRow(rows)
		if err != nil {
			return nil, err
		}
		return loc, nil
	}
	return nil, nil
}

func (r *repository) RetrieveLocationIDsForUser(id string) ([]int64, error) {
	// Slightly inefficient, but works for now
	locations, err := r.RetrieveLocationsForUser(id)
	if err != nil {
		return nil, err
	}
	ids := make([]int64, 0)
	for _, loc := range locations {
		ids = append(ids, loc.Id)
	}
	return ids, nil
}

func (r *repository) RetrieveLocationsForUser(userId string) ([]scom.Location, error) {
	rows, err := r.db.Query(sqlSelLocationsForUser, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	locations := make([]scom.Location, 0)
	for rows.Next() {
		loc, err := scanLocationRow(rows)
		if err != nil {
			return nil, err
		}
		locations = append(locations, *loc)
	}
	return locations, nil
}

func (r *repository) RetrieveReporterByKeyId(externalId string) (rep *scom.Reporter, err error) {
	rows, err := r.db.Query(sqlSelReporterByKeyId, externalId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanReporterRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveReporterById(reporterId int64) (*scom.Reporter, error) {
	rows, err := r.db.Query(sqlSelReporterById, reporterId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanReporterRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveReportersForLocation(locationId int64) ([]scom.Reporter, error) {
	rows, err := r.db.Query(sqlSelReportersByLocationId, locationId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	reporters := make([]scom.Reporter, 0)
	for rows.Next() {
		rep, err := scanReporterRow(rows)
		if err != nil {
			return nil, err
		}
		reporters = append(reporters, *rep)
	}
	return reporters, nil
}

func (r *repository) RetrieveRulesForSensor(sensorId int64) ([]scom.Rule, error) {
	rows, err := r.db.Query(sqlSelectRulesForSensor, sensorId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var rule *scom.Rule
	rules := make([]scom.Rule, 0)
	for rows.Next() {
		rule = new(scom.Rule)
		err = rows.Scan(&rule.Id, &rule.UserId, &rule.SensorId, &rule.NotificationTopic, &rule.Script, &rule.State)
		if err != nil {
			return nil, err
		}
		rules = append(rules, *rule)
	}
	return rules, nil
}

func (r *repository) RetrieveSensorById(sensorId int64) (*scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelSensorById, sensorId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanSensorRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveSensorByName(name string) (*scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelSensorByName, name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanSensorRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveSensorIDsForReporter(reporterId int64) ([]int64, error) {
	rows, err := r.db.Query(sqlSelSensorIDsForReporter, reporterId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var sensorId int64
	sensorIds := make([]int64, 0)
	for rows.Next() {
		if err := rows.Scan(&sensorId); err != nil {
			return nil, err
		}
		sensorIds = append(sensorIds, sensorId)
	}
	return sensorIds, nil
}

func (r *repository) RetrieveSensorIDsForUser(userId string) ([]int64, error) {
	rows, err := r.db.Query(sqlSelSensorIDsForUser, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var sensorId int64
	sensorIds := make([]int64, 0)
	for rows.Next() {
		if err := rows.Scan(&sensorId); err != nil {
			return nil, err
		}
		sensorIds = append(sensorIds, sensorId)
	}
	return sensorIds, nil
}

func (r *repository) RetrieveSensorIDsForUserDash(dashId int64) ([]int64, error) {
	rows, err := r.db.Query(sqlSelSensorIDsForUserDash, dashId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var sensorId int64
	sensorIds := make([]int64, 0)
	for rows.Next() {
		if err := rows.Scan(&sensorId); err != nil {
			return nil, err
		}
		sensorIds = append(sensorIds, sensorId)
	}
	return sensorIds, nil
}

func (r *repository) RetrieveSensorsForLocation(locationId int64) ([]scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelSensorsForLocation, locationId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	sensors := make([]scom.Sensor, 0)
	for rows.Next() {
		sensor, err := scanSensorRow(rows)
		if err != nil {
			return nil, err
		}
		sensors = append(sensors, *sensor)
	}
	return sensors, nil
}

func (r *repository) RetrieveSensorsForReporter(reporterId int64) ([]scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelSensorsForReporter, reporterId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	sensors := make([]scom.Sensor, 0)
	for rows.Next() {
		sensor, err := scanSensorRow(rows)
		if err != nil {
			return nil, err
		}
		sensors = append(sensors, *sensor)
	}
	return sensors, nil
}

func (r *repository) RetrieveSensorsForUser(userId string) ([]scom.Sensor, error) {
	rows, err := r.db.Query(sqlSelSensorsForUser, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	sensors := make([]scom.Sensor, 0)
	for rows.Next() {
		sensor, err := scanSensorRow(rows)
		if err != nil {
			return nil, err
		}
		sensors = append(sensors, *sensor)
	}
	return sensors, nil
}

func (r *repository) RetrieveUserDashboardsByUser(userId string) ([]scom.UserDash, error) {
	rows, err := r.db.Query(sqlSelUserDashByUserId, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	dashes := make([]scom.UserDash, 0)
	for rows.Next() {
		dash, err := scanUserDashRow(rows)
		if err != nil {
			return nil, err
		}
		dashes = append(dashes, *dash)
	}
	return dashes, nil
}

func (r *repository) RetrieveUserDashboardsByUserAndLocation(userId string, locationId int64) ([]scom.UserDash, error) {
	rows, err := r.db.Query(sqlSelUserDashByUserAndLocation, userId, locationId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	dashes := make([]scom.UserDash, 0)
	for rows.Next() {
		dash, err := scanUserDashRow(rows)
		if err != nil {
			return nil, err
		}
		dashes = append(dashes, *dash)
	}
	return dashes, nil
}

func (r *repository) RetrieveUserDashById(id int64) (*scom.UserDash, error) {
	rows, err := r.db.Query(sqlSelUserDashById, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanUserDashRow(rows)
	}
	return nil, nil
}

func (r *repository) RetrieveUserDashByKeyId(id string) (*scom.UserDash, error) {
	rows, err := r.db.Query(sqlSelUserDashByKeyId, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return scanUserDashRow(rows)
	}
	return nil, nil
}

func (r *repository) SensorNameExists(name string) (exists bool, err error) {
	rst := r.db.QueryRow(sqlSelSensorNameExists, name)
	if rst.Err() != nil {
		return false, rst.Err()
	}
	err = rst.Scan(&exists)
	return
}

func (r *repository) UpdateLocation(location *scom.Location) (err error) {
	_, err = r.db.Exec(
		sqlUpdLocation, location.Id, location.Name, location.Description, location.AddressLine1,
		location.AddressLine2, location.AddressCity, location.AddressRegion, location.AddressPostalCode,
		location.AddressCountry, location.Latitude, location.Longitude)
	return
}

func (r *repository) UpdateReporterAuthHash(id int64, hash string) (err error) {
	_, err = r.db.Exec(sqlUpdReporterAuthHash, id, hash)
	return
}

func (r *repository) UpdateRule(rule *scom.Rule) (err error) {
	_, err = r.db.Exec(sqlUpdRule, rule.Id, rule.NotificationTopic, rule.Script, rule.State)
	return err
}

func (r *repository) UpdateSensor(sensor *scom.Sensor) (err error) {
	sd := sql.NullInt32{Valid: sensor.DisplaySigDigits != nil}
	if sd.Valid {
		sd.Int32 = *sensor.DisplaySigDigits
	}
	_, err = r.db.Exec(sqlUpdSensor, sensor.Id, sensor.LocationId, sensor.ReporterId, sensor.Name, sensor.Type,
		sensor.Schedule, sensor.ConfigData, sensor.StoreUnit, sensor.DisplayUnit, sensor.DisplayConversion, sd)
	return err
}

func (r *repository) UpdateUserDashAuthHash(id int64, hash string) (err error) {
	_, err = r.db.Exec(sqlUpdUserDashAuthHash, id, hash)
	return
}

func NewRepository() (Repository, error) {
	dburl := os.Getenv(config.EnvDbUrl)
	if dburl == "" {
		return nil, fmt.Errorf("database connection URL expected in environment variable %s", config.EnvDbUrl)
	}
	var err error
	repo := repository{}
	repo.db, err = sql.Open("postgres", dburl)
	if err != nil {
		return nil, err
	}
	return &repo, nil
}

func scanLocationRow(rows *sql.Rows) (*scom.Location, error) {
	loc := &scom.Location{}
	if err := rows.Scan(&loc.Id, &loc.Name, &loc.Description, &loc.AddressLine1, &loc.AddressLine2,
		&loc.AddressCity, &loc.AddressRegion, &loc.AddressPostalCode, &loc.AddressCountry, &loc.Latitude,
		&loc.Longitude); err != nil {
		return nil, err
	}
	return loc, nil
}

func scanReporterRow(rows *sql.Rows) (*scom.Reporter, error) {
	rep := &scom.Reporter{}
	if err := rows.Scan(&rep.Id, &rep.KeyId, &rep.LocationId, &rep.Name, &rep.AuthHash); err != nil {
		return nil, err
	}
	return rep, nil
}

func scanSensorRow(rows *sql.Rows) (*scom.Sensor, error) {
	digits := sql.NullInt32{}
	sensor := &scom.Sensor{}
	if err := rows.Scan(&sensor.Id, &sensor.LocationId, &sensor.ReporterId, &sensor.Name,
		&sensor.Type, &sensor.Schedule, &sensor.ConfigData, &sensor.StoreUnit,
		&sensor.DisplayUnit, &sensor.DisplayConversion, &digits,
	); err != nil {
		return nil, err
	}
	if digits.Valid {
		sensor.DisplaySigDigits = &digits.Int32
	}
	return sensor, nil
}

func scanSensorReadingRow(rows *sql.Rows) (*scom.SensorReading, error) {
	var ts time.Time
	reading := &scom.SensorReading{}
	if err := rows.Scan(&reading.Id, &reading.LocationId, &reading.SensorId, &ts,
		&reading.Value, &reading.DisplayValue,
	); err != nil {
		return nil, err
	}
	reading.Timestamp = ts.Unix()
	return reading, nil
}

func scanUserDashRow(rows *sql.Rows) (*scom.UserDash, error) {
	ud := &scom.UserDash{}
	if err := rows.Scan(&ud.Id, &ud.UserId, &ud.LocationId, &ud.KeyId, &ud.AuthHash); err != nil {
		return nil, err
	}
	return ud, nil
}
