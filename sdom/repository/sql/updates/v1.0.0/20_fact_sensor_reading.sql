CREATE TABLE fact_sensor_reading (
    id BIGSERIAL PRIMARY KEY,
    location_id BIGINT NOT NULL,
    sensor_id BIGINT NOT NULL,
    date_id BIGINT NOT NULL,
    time_id BIGINT NOT NULL,
    value NUMERIC NOT NULL,
    display_value NUMERIC NOT NULL,

    CONSTRAINT idx_sensor_reading_01 UNIQUE (sensor_id, date_id, time_id)
);
