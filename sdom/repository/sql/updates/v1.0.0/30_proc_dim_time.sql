CREATE OR REPLACE FUNCTION proc_dim_time(time) RETURNS BIGINT
AS $_$
DECLARE
    p_time ALIAS FOR $1;
    v_id BIGINT;
BEGIN
    IF p_time IS NULL THEN
        RETURN NULL;
    ELSE
        SELECT id FROM dim_time WHERE pg_time = p_time INTO v_id;

        IF v_id IS NULL THEN
            SELECT NEXTVAL('dim_time_id_seq') INTO v_id;
            INSERT INTO dim_time (id, hour, minute, second, pg_time)
            VALUES (v_id, DATE_PART('HOUR', p_time), DATE_PART('MINUTE', p_time), DATE_PART('SECOND', p_time),  p_time);
        END IF;

        RETURN v_id;
    END IF;
END;
$_$
    LANGUAGE plpgsql;
