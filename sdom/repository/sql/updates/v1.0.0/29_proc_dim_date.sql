CREATE OR REPLACE FUNCTION proc_dim_date(date) RETURNS BIGINT
AS $_$
DECLARE
    p_date ALIAS FOR $1;
    v_id BIGINT;
BEGIN
    IF p_date IS NULL THEN
        RETURN NULL;
    ELSE
        SELECT id FROM dim_date WHERE pg_date = p_date INTO v_id;

        IF v_id IS NULL THEN
            SELECT NEXTVAL('dim_date_id_seq') INTO v_id;
            INSERT INTO dim_date (id, year, month, day, day_of_week, day_of_year, pg_date)
            VALUES (v_id, DATE_PART('YEAR', p_date), DATE_PART('MONTH', p_date), DATE_PART('DAY', p_date), DATE_PART('DOW', p_date), DATE_PART('DOY', p_date), p_date);
        END IF;

        RETURN v_id;
    END IF;
END;
$_$
    LANGUAGE plpgsql;
