CREATE TABLE dim_sensor (
    id BIGSERIAL PRIMARY KEY,

    location_id BIGINT NOT NULL,
    reporter_id BIGINT NOT NULL,

    name VARCHAR(64) NOT NULL,
    type VARCHAR(64) NOT NULL,
    schedule VARCHAR(64) NOT NULL,
    config_data JSONB NOT NULL DEFAULT '{}',
    store_unit VARCHAR NOT NULL,
    display_unit VARCHAR NOT NULL,
    display_conversion TEXT NOT NULL DEFAULT '',
    display_sig_digits int NULL,

    CONSTRAINT idx_sensor_01 UNIQUE (location_id, name, type),

    CONSTRAINT fk_sensor_01 FOREIGN KEY (location_id)
        REFERENCES dim_location (id),
    CONSTRAINT fk_sensor_02 FOREIGN KEY (reporter_id)
        REFERENCES dim_reporter (id)
);
