CREATE TABLE dim_time (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('dim_time_id_seq'),

    hour INTEGER NOT NULL,
    minute INTEGER NOT NULL,
    second INTEGER NOT NULL,
    pg_time TIME WITHOUT TIME ZONE NOT NULL,

    CONSTRAINT idx_time_01 UNIQUE (pg_time)
);
