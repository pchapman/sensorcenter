CREATE TABLE dim_location (
    id BIGSERIAL PRIMARY KEY,

    name VARCHAR(64) not null,
    description TEXT,

    address_line1 VARCHAR(128),
    address_line2 VARCHAR(128),
    address_city VARCHAR(128),
    address_region VARCHAR(128),
    address_postal_code VARCHAR(128),
    address_country VARCHAR(128),

    longitude NUMERIC(6,3),
    latitude  NUMERIC(6,3),

    CONSTRAINT idx_location_01 UNIQUE (name),
    CONSTRAINT idx_location_02 UNIQUE (address_country, address_postal_code, address_region, address_city, address_line1, address_line2),
    CONSTRAINT idx_location_03 UNIQUE (longitude, latitude)
);
