CREATE TABLE sensor_rule (
    id BIGSERIAL PRIMARY KEY,
    user_id VARCHAR(128) NOT NULL,
    sensor_id BIGINT NOT NULL,
    notification_topic VARCHAR(128) NOT NULL,
    script TEXT NOT NULL,
    state JSONB NOT NULL DEFAULT '{}',

    CONSTRAINT idx_sensor_rule_01 UNIQUE (user_id, sensor_id),
    CONSTRAINT fk_sensor_rule_01 FOREIGN KEY (sensor_id)
        REFERENCES dim_sensor(id)
);
