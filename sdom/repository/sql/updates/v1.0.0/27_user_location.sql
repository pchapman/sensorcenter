CREATE TABLE user_location (
    id BIGSERIAL PRIMARY KEY,

    user_id VARCHAR(128) NOT NULL,
    location_id BIGINT NOT NULL,

    CONSTRAINT idx_user_location_01 UNIQUE (user_id, location_id),
    CONSTRAINT fk_user_location_01 FOREIGN KEY (location_id)
       REFERENCES dim_location (id)
);
