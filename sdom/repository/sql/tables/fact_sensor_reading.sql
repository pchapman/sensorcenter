-- Sensor Reading --
CREATE TABLE fact_sensor_reading (
    id BIGSERIAL PRIMARY KEY,
    location_id BIGINT NOT NULL,
    sensor_id BIGINT NOT NULL,
    date_id BIGINT NOT NULL,
    time_id BIGINT NOT NULL,
    value NUMERIC(5, 5) NOT NULL,
    display_value NUMERIC(5, 5) NOT NULL,

    CONSTRAINT idx_sensor_reading_01 UNIQUE (sensor_id, date_id, time_id)
);

CREATE INDEX idx_sensor_reading_02 ON fact_sensor_reading (location_id, sensor_id);
CREATE INDEX idx_sensor_reading_03 ON fact_sensor_reading (date_id, sensor_id);
CREATE INDEX idx_sensor_reading_04 ON fact_sensor_reading (time_id, sensor_id);
