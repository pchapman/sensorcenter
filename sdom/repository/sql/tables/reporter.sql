-- Reporter Dimension --
CREATE TABLE reporter
(
    id   BIGSERIAL PRIMARY KEY,

    key_id VARCHAR(64) NOT NULL,
    location_id BIGINT NOT NULL,
    name VARCHAR(64) NOT NULL,
    auth_hash VARCHAR NOT NULL,

    CONSTRAINT idx_reporter_01 UNIQUE (key_id),
    CONSTRAINT idx_reporter_02 UNIQUE (name),
    CONSTRAINT fk_reporter_01 FOREIGN KEY (location_id)
        REFERENCES dim_location (id)
);
