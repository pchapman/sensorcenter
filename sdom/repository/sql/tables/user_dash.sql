-- Reporter Dimension --
CREATE TABLE user_dash
(
    id   BIGSERIAL PRIMARY KEY,

    user_id VARCHAR(64) NOT NULL,
    location_id BIGINT NOT NULL,
    key_id VARCHAR(64) NOT NULL,
    auth_hash VARCHAR NOT NULL,

    CONSTRAINT idx_user_dash_01 UNIQUE (key_id),
    CONSTRAINT fk_user_dash_01 FOREIGN KEY (location_id)
        REFERENCES dim_location (id)
);

CREATE INDEX idx_user_dash_02 ON user_dash (user_id);
CREATE INDEX idx_user_dash_03 ON user_dash (location_id);
