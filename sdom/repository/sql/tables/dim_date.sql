-- Date Dimension --
CREATE SEQUENCE IF NOT EXISTS dim_date_id_seq AS BIGINT;
CREATE TABLE IF NOT EXISTS dim_date (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('dim_date_id_seq'),

    year INTEGER NOT NULL,
    month INTEGER NOT NULL,
    day INTEGER NOT NULL,
    day_of_week INTEGER NOT NULL,
    day_of_year INTEGER NOT NULL,
    pg_date DATE NOT NULL,

    CONSTRAINT idx_date_01 UNIQUE (year, month, day),
    CONSTRAINT idx_date_02 UNIQUE (year, day_of_year),
    CONSTRAINT idx_date_03 UNIQUE (pg_date)
);

CREATE INDEX idx_date_04 ON dim_date (month);
CREATE INDEX idx_date_05 ON dim_date (day);
CREATE INDEX idx_date_06 ON dim_date (day_of_week);
CREATE INDEX idx_date_07 ON dim_date (day_of_year);
