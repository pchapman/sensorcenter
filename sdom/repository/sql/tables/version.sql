-- Version --
CREATE TABLE version (
    id SERIAL PRIMARY KEY,
    major INTEGER NOT NULL,
    minor INTEGER NOT NULL,
    revision INTEGER NOT NULL,

    CONSTRAINT idx_version_01 UNIQUE (major, minor, revision)
);
