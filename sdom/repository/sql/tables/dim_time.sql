-- Time Dimension --
CREATE SEQUENCE IF NOT EXISTS dim_time_id_seq AS BIGINT;
CREATE TABLE dim_time (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT NEXTVAL('dim_time_id_seq'),

    hour INTEGER NOT NULL,
    minute INTEGER NOT NULL,
    second INTEGER NOT NULL,
    pg_time TIME WITHOUT TIME ZONE NOT NULL,

    CONSTRAINT idx_time_01 UNIQUE (pg_time)
);

CREATE INDEX idx_time_02 ON dim_time (hour, minute, second);
CREATE INDEX idx_time_03 ON dim_time (minute);
CREATE INDEX idx_time_04 ON dim_time (second);
