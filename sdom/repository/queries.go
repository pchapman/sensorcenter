package repository

const sqlDeleteLocation = `
	DELETE FROM dim_location WHERE id = $1;
`
const sqlDeleteReporterById = `
	DELETE FROM reporter WHERE id = $1;
`
const sqlDeleteReportersByLocation = `
	DELETE FROM reporter WHERE location_id = $1;
`
const sqlDeleteRulesByLocation = `
	DELETE FROM sensor_rule WHERE sensor_id IN (SELECT id FROM dim_sensor WHERE location_id = $1);
`
const sqlDeleteRulesBySensor = `
	DELETE FROM sensor_rule WHERE sensor_id = $1;
`
const sqlDeleteSensorById = `
	DELETE FROM dim_sensor WHERE id = $1;
`
const sqlDeleteSensorsByLocation = `
	DELETE FROM dim_sensor WHERE location_id = $1;
`
const sqlDeleteSensorReadingsByLocation = `
	DELETE FROM fact_sensor_reading WHERE sensor_id in (SELECT id FROM dim_sensor WHERE location_id = $1);
`
const sqlDeleteSensorReadingsBySensor = `
	DELETE FROM fact_sensor_reading WHERE sensor_id = $1;
`
const sqlDeleteUserDashboardById = `
	DELETE FROM user_dash WHERE id = $1;
`
const sqlDeleteUserDashboardsByLocation = `
	DELETE FROM user_dash WHERE location_id = $1;
`
const sqlDeleteUserLocationsByLocation = `
	DELETE FROM user_location WHERE location_id = $1;
`
const sqlInsLocation = `
	INSERT INTO dim_location (
		name, description,
		address_line1, address_line2, address_city, address_region, address_postal_code, address_country,
		latitude, longitude
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	RETURNING id;
`
const sqlInsReporter = `
	INSERT INTO reporter (key_id, location_id, name, auth_hash) VALUES ($1, $2, $3, $4) RETURNING id;
`
const sqlInsSensor = `
	INSERT INTO dim_sensor (
		location_id, reporter_id, name, type, schedule, config_data, store_unit,
	    display_unit, display_conversion, display_sig_digits
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	RETURNING id;
`
const sqlInsSensorReading = `
	INSERT INTO fact_sensor_reading (location_id, sensor_id, date_id, time_id, value, display_value)
	VALUES ($1, $2, proc_dim_date($3::date), proc_dim_time($4::time), $5, $6) RETURNING id;
`
const sqlInsUserDash = `
	INSERT INTO user_dash (user_id, location_id, key_id, auth_hash) VALUES ($1, $2, $3, $4) RETURNING id;
`
const sqlInsUserLocation = `INSERT INTO user_location (user_id, location_id) VALUES ($1, $2);`
const sqlInsVersion = `INSERT INTO version (major, minor, revision) VALUES ($1, $2, $3);`
const sqlSelAllLocations = sqlSelLocationHeader + `
	ORDER BY l.name;
`
const sqlSelAllSensors = sqlSelSensorHeader + `
	ORDER BY s.name;
`
const sqlSelLocationNameExists = `SELECT EXISTS(SELECT 1 FROM dim_location WHERE name = $1);`
const sqlSelReporterByKeyId = sqlSelReporterHeader + `
	WHERE rep.key_id = $1
`
const sqlSelReporterById = sqlSelReporterHeader + `
	WHERE rep.id = $1
`
const sqlSelReporterHeader = `
	SELECT rep.id, rep.key_id, rep.location_id, rep.name, rep.auth_hash
	FROM reporter rep
`
const sqlSelReporterNameExists = `SELECT EXISTS(SELECT 1 FROM reporter WHERE name = $1);`
const sqlSelReporterKeyIdExists = `SELECT EXISTS(SELECT 1 FROM reporter WHERE key_id = $1);`
const sqlSelReportersByLocationId = sqlSelReporterHeader + `
	WHERE rep.location_id = $1
	ORDER BY rep.name
`
const sqlSelLatestSensorReading = sqlSelSensorReadingHeader + `
	WHERE
		r.sensor_id = $1
	ORDER BY d.pg_date DESC, t.pg_time DESC
	LIMIT 1
`
const sqlSelLocationByCoordinates = sqlSelLocationHeader +
	` WHERE l.latitude = $1::NUMERIC(6,3) AND l.longitude = $2::NUMERIC(6,3);`
const sqlSelLocationById = sqlSelLocationHeader +
	` WHERE l.id = $1;`
const sqlSelLocationByName = sqlSelLocationHeader +
	` WHERE l.name = $1;`
const sqlSelLocationsForUser = sqlSelLocationHeader +
	` JOIN user_location ul ON ul.location_id = l.id WHERE ul.user_id = $1 ORDER BY l.name;`
const sqlSelLocationHeader = `
	SELECT
		l.id, l.name, l.description,
		l.address_line1, l.address_line2, l.address_city, l.address_region, l.address_postal_code, l.address_country,
		l.latitude, l.longitude
	FROM dim_location l
`
const sqlSelectRulesForSensor = `
	SELECT id, user_id, sensor_id, notification_topic, script, state
	FROM sensor_rule
	WHERE sensor_id = $1
`
const sqlSelSensorById = sqlSelSensorHeader + `
	WHERE s.id = $1;
`
const sqlSelSensorByName = sqlSelSensorHeader + `
	WHERE s.name = $1;
`
const sqlSelSensorHeader = `
	SELECT
		s.id, s.location_id, s.reporter_id, s.name, s.type, s.schedule, s.config_data,
		s.store_unit, s.display_unit, s.display_conversion, s.display_sig_digits
	FROM dim_sensor s
`
const sqlSelSensorIDsForReporter = `
	SELECT s.id
	FROM dim_sensor s
	JOIN reporter rep ON rep.id = s.reporter_id
	WHERE rep.id = $1
`
const sqlSelSensorIDsForUser = `
	SELECT s.id
	FROM dim_sensor s
	JOIN user_location ul ON ul.location_id = s.location_id
	WHERE ul.user_id = $1
`
const sqlSelSensorIDsForUserDash = `
	SELECT s.id
	FROM
		user_dash ud
		JOIN dim_sensor s ON s.location_id = ud.location_id
	WHERE ud.id = $1
`
const sqlSelSensorsForLocation = sqlSelSensorHeader + `
	WHERE s.location_id = $1
	ORDER BY s.name
`
const sqlSelSensorsForReporter = sqlSelSensorHeader + `
	WHERE s.reporter_id = $1
	ORDER BY s.name
`
const sqlSelSensorsForUser = sqlSelSensorHeader + `
	JOIN user_location ul ON ul.location_id = s.location_id
	WHERE ul.user_id = $1
	ORDER BY s.name
`
const sqlSelSensorReadingHeader = `
	SELECT r.id, r.location_id, r.sensor_id, d.pg_date + t.pg_time AS ts, r.value, r.display_value
	FROM
		fact_sensor_reading r
		JOIN dim_date d ON d.id = r.date_id
		JOIN dim_time t ON t.id = r.time_id
`
const sqlSelSensorNameExists = `SELECT EXISTS(SELECT 1 FROM dim_sensor WHERE name = $1);`
const sqlSelUserDashById = sqlSelUserDashHeader + `
	WHERE ud.id = $1
`
const sqlSelUserDashByKeyId = sqlSelUserDashHeader + `
	WHERE ud.key_id = $1
`
const sqlSelUserDashByUserId = sqlSelUserDashHeader + `
	WHERE ud.user_id = $1
	ORDER BY ud.key_id
`
const sqlSelUserDashByUserAndLocation = sqlSelUserDashHeader + `
	WHERE ud.user_id = $1 AND ud.location_id = $2
	ORDER BY ud.key_id
`
const sqlSelUserDashHeader = `
	SELECT ud.id, ud.user_id, ud.location_id, ud.key_id, ud.auth_hash
	FROM user_dash ud
`
const sqlUpdLocation = `
	UPDATE dim_location SET
		name = $2, description = $3,
		address_line1 = $4, address_line2 = $5, address_city = $6, address_region = $7, address_postal_code = $8,
		address_country = $9, latitude = $10, longitude = $11
	WHERE id = $1
`
const sqlUpdReporterAuthHash = `UPDATE reporter SET auth_hash = $2 WHERE id = $1;`
const sqlUpdRule = `UPDATE sensor_rule SET notification_topic = $2, script = $3, state = $4 WHERE id = $1;`
const sqlUpdSensor = `
	UPDATE dim_sensor
	SET
		location_id = $2, reporter_id = $3, name = $4, type = $5, schedule = $6, config_data = $7, store_unit = $8,
		display_unit = $9, display_conversion = $10, display_sig_digits = $11 WHERE id = $1
	;
`
const sqlUpdUserDashAuthHash = `UPDATE user_dash SET auth_hash = $2 WHERE id = $1;`
const sqlSelVersion = `
	SELECT major, minor, revision FROM version ORDER BY major DESC, minor DESC, revision DESC LIMIT 1;
`
