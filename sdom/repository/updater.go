package repository

import (
	"database/sql"
	"embed"
	"fmt"
	"gopkg.in/yaml.v2"
	"log"
	"path/filepath"
)

//go:embed sql/updates/*/*.sql
var sqlStatements embed.FS

//go:embed sql/updates/updates.yaml sql/updates/*/*.yaml
var updateScripts embed.FS

type Updates struct {
	Updates []string `json:"updates" yaml:"updates"`
}

type Version struct {
	Major int `json:"major" yaml:"major"`
	Minor int `json:"minor" yaml:"minor"`
	Rev   int `json:"rev" yaml:"rev"`
}

func NewVersion(major, minor, rev int) *Version {
	return &Version{
		Major: major,
		Minor: minor,
		Rev:   rev,
	}
}

func (v *Version) After(v2 *Version) bool {
	if v.Major > v2.Major {
		return true
	} else if v.Major == v2.Major {
		if v.Minor > v2.Minor {
			return true
		} else if v.Minor == v2.Minor {
			if v.Rev > v2.Rev {
				return true
			}
		}
	}
	return false
}

func (v *Version) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Rev)
}

type Update struct {
	Scripts     []string `json:"scripts" yaml:"scripts"`
	UpdatesPath string   `json:"-" yaml:"-"`
	Version     Version  `json:"version" yaml:"version"`
}

func LoadUpdates() (updatesSlice []Update, err error) {
	data, err := updateScripts.ReadFile("sql/updates/updates.yaml")
	if err != nil {
		return
	}
	updates := Updates{}
	err = yaml.Unmarshal(data, &updates)
	if err != nil {
		return
	}
	var update *Update
	for _, fileName := range updates.Updates {
		data, err = updateScripts.ReadFile("sql/updates/" + fileName)
		if err != nil {
			return
		}
		update = new(Update)
		update.UpdatesPath = filepath.Dir("sql/updates/" + fileName)
		err = yaml.Unmarshal(data, update)
		if err != nil {
			return
		}
		updatesSlice = append(updatesSlice, *update)
	}
	return
}

func (u *Update) ReadScript(fileName string) (sql string, err error) {
	data, err := sqlStatements.ReadFile(u.UpdatesPath + "/" + fileName)
	if err != nil {
		return
	}
	return string(data), nil
}

func executeUpdates(db *sql.DB) (err error) {
	//TODO: Add support for tracking which update scripts have been applied, allowing for restarts of failed update.

	// Setup one, load all possible updates
	updates, err := LoadUpdates()
	if err != nil {
		return
	}

	// Get the current version.  If an error is thrown, assume version is 0.0.0.
	current := NewVersion(0, 0, 0)
	rst := db.QueryRow(sqlSelVersion)
	err = rst.Scan(&current.Major, &current.Minor, &current.Rev)
	if err != nil {
		log.Printf("Uninitialized DB, all updates will be applied")
	} else {
		log.Printf("Current version: %s", current.String())
	}

	// Execute each update after the current version
	var sqlScript string
	for _, update := range updates {
		if update.Version.After(current) {
			log.Printf("Applying update %s", update.Version.String())
			for _, scriptName := range update.Scripts {
				sqlScript, err = update.ReadScript(scriptName)
				if err != nil {
					log.Printf("Unable to read update %s script %s: %s", update.Version.String(), scriptName, err)
					return
				}
				_, err = db.Exec(sqlScript)
				if err != nil {
					log.Printf("Unable to execute update %s script %s: %s", update.Version.String(), scriptName, err)
					return
				}
			}
			_, err = db.Exec(sqlInsVersion, update.Version.Major, update.Version.Minor, update.Version.Rev)
			if err != nil {
				log.Printf("Unable to record update %s: %s", update.Version.String(), err)
				return
			}
			log.Printf("Update %s applied", update.Version.String())
		}
	}
	return
}
