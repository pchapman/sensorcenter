package services

import (
	"bytes"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"math/rand"
	"testing"
)

type mockRepository struct {
	reportersByExternId map[string]scom.Reporter
	reportersById       map[int64]scom.Reporter
	t                   *testing.T
	repository.Repository
}

func (r mockRepository) InsertReporter(reporter *scom.Reporter) (id int64, err error) {
	if reporter.LocationId != 2345 {
		return 0, fmt.Errorf("expected location id 2345, got %d", reporter.LocationId)
	}
	if reporter.Name != "test reporter" {
		return 0, fmt.Errorf("expected name test reporter, got %s", reporter.Name)
	}
	mine := scom.Reporter{
		Id:         rand.Int63(),
		KeyId:      reporter.KeyId,
		LocationId: reporter.LocationId,
		Name:       reporter.Name,
		AuthHash:   reporter.AuthHash,
	}
	r.reportersByExternId[mine.KeyId] = mine
	r.reportersById[mine.Id] = mine
	return mine.Id, nil
}

func (r mockRepository) ReporterKeyIdExists(externalId string) (bool, error) {
	_, ok := r.reportersByExternId[externalId]
	return ok, nil
}

func (r mockRepository) RetrieveLocationById(locationId int64) (*scom.Location, error) {
	return &scom.Location{
		Id:   locationId,
		Name: "test location",
	}, nil
}

func (r mockRepository) RetrieveReporterByKeyId(externalId string) (*scom.Reporter, error) {
	mine, ok := r.reportersByExternId[externalId]
	if !ok {
		return nil, fmt.Errorf("not found")
	}
	return copyReporter(&mine), nil
}

func (r mockRepository) RetrieveReporterById(reporterId int64) (*scom.Reporter, error) {
	mine, ok := r.reportersById[reporterId]
	if !ok {
		return nil, fmt.Errorf("not found")
	}
	return copyReporter(&mine), nil
}

func (r mockRepository) UpdateReporterAuthHash(id int64, hash string) (err error) {
	mine, ok := r.reportersById[id]
	if !ok {
		return fmt.Errorf("not found")
	}
	r.t.Logf("Updating auth hash for reporter %d from %s to %s", id, mine.AuthHash, hash)
	mine.AuthHash = hash
	r.reportersByExternId[mine.KeyId] = mine
	r.reportersById[id] = mine
	return nil
}

func newMockRepository(t *testing.T) mockRepository {
	return mockRepository{
		reportersByExternId: map[string]scom.Reporter{},
		reportersById:       map[int64]scom.Reporter{},
		t:                   t,
	}
}

func copyReporter(r *scom.Reporter) *scom.Reporter {
	return &scom.Reporter{
		Id:         r.Id,
		KeyId:      r.KeyId,
		LocationId: r.LocationId,
		Name:       r.Name,
		AuthHash:   r.AuthHash,
	}
}

func TestCreateReporter(t *testing.T) {
	var err error
	var rptrA, rptrB *scom.Reporter
	var passA, passB string

	repo := newMockRepository(t)
	authSvc := authService{
		repo:    repo,
		time:    2,
		memory:  65536,
		threads: 4,
		keyLen:  128,
		saltLen: 128,
	}

	// Create a reporter
	rptrA, passA, err = authSvc.CreateReporter(2345, "test reporter")
	if err != nil {
		t.Fatalf("Expected no error, but got: %s", err)
	}
	if rptrA == nil {
		t.Fatalf("Expected reporter, but got nil")
	}
	if rptrA.Id < 1 {
		t.Fatalf("Expected id > 0, got %d", rptrA.Id)
	}
	if rptrA.KeyId == "" {
		t.Fatalf("Expected external id, but got empty string")
	}
	if len(passA) != 32 {
		t.Fatalf("Expected password of 32 chars, but got one of %d", len(passA))
	}

	// Test password comparison
	rptrB, err = authSvc.AuthenticateReporter(rptrA.KeyId, passA)
	if err != nil {
		t.Fatalf("Expected no error, but got: %s", err)
	}
	if rptrB == nil {
		t.Fatalf("Expected reporter, but got nil")
	}
	if rptrB.Id != rptrA.Id {
		t.Fatalf("Expected id %d, got %d", rptrA.Id, rptrB.Id)
	}

	// Test password mismatch
	_, err = authSvc.AuthenticateReporter(rptrA.KeyId, "wrong")
	if err == nil {
		t.Fatalf("Expected error, but got none")
	}

	// Test rotating password
	passB, err = authSvc.RotateReporterPassword(rptrA.Id)
	if err != nil {
		t.Fatalf("Expected no error, but got: %s", err)
	}
	if passB == "" {
		t.Fatalf("Expected password, but got empty")
	}
	if passB == passA {
		t.Fatalf("Expected password change, but got no change")
	}

	// Test old password mismatch
	_, err = authSvc.AuthenticateReporter(rptrA.KeyId, passA)
	if err == nil {
		t.Fatalf("Expected error, but got none")
	}

	// Test password comparison
	rptrB, err = authSvc.AuthenticateReporter(rptrA.KeyId, passB)
	if err != nil {
		t.Fatalf("Expected no error, but got: %s", err)
	}
	if rptrB == nil {
		t.Fatalf("Expected reporter, but got nil")
	}
	if rptrB.Id != rptrA.Id {
		t.Fatalf("Expected id %d, got %d", rptrA.Id, rptrB.Id)
	}

	// Test password mismatch
	_, err = authSvc.AuthenticateReporter(rptrA.KeyId, "wrong")
	if err == nil {
		t.Fatalf("Expected error, but got none")
	}
}

func TestGenerateRandomString(t *testing.T) {
	// Ensure the testing function with valid inputs
	s := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	err := testGeneratedString(s, uint8(len(s)))
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	s = "!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	err = testGeneratedString(s, uint8(len(s)))
	if err == nil {
		t.Errorf("Expected error, but got none")
	}
	for _, l := range []uint8{10, 20, 30, 32} {
		s = generateRandomString(l)
		err = testGeneratedString(s, l)
		if err != nil {
			t.Errorf("Expected no error, but got: %s", err)
		}
	}
}

func testGeneratedString(s string, l uint8) error {
	if len(s) != int(l) {
		return fmt.Errorf("expected length %d, got %d", l, len(s))
	}
	for i, c := range s {
		err := fmt.Errorf("expected alphanumeric at position %d, got %c", i, c)
		if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') {
			continue
		} else {
			return err
		}
	}
	return nil
}

func TestHashEncoding(t *testing.T) {
	salt1 := []byte("0123456789")
	hash1 := []byte("abcdefghijklmnopqrstuvwxyz")
	encoded1, err := encodeSaltAndHash(salt1, hash1)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if encoded1 == "" {
		t.Errorf("Expected encoded password, but got empty")
	}
	salt2, hash2, err := decodeSaltAndHash(encoded1)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if !bytes.Equal(salt1, salt2) {
		t.Errorf("Expected salt %v, got %v", salt1, salt2)
	}
	if !bytes.Equal(hash1, hash2) {
		t.Errorf("Expected hash %v, got %v", hash1, hash2)
	}
}

func TestPasswordComparisonSimple(t *testing.T) {
	repo := newMockRepository(t)
	authSvc := authService{
		repo:    repo,
		time:    2,
		memory:  65536,
		threads: 4,
		keyLen:  128,
		saltLen: 128,
	}

	salt1 := []byte("0123456789")
	pass1 := []byte("abcdefghijklmnopqrstuvwxyz")
	var err error
	var encodedHash1 string
	var hash1 []byte
	hash1, err = authSvc.generateHash(pass1, salt1)
	encodedHash1, err = encodeSaltAndHash(salt1, hash1)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if encodedHash1 == "" {
		t.Errorf("Expected encoded password, but got empty")
	}

	ok, err := authSvc.compare(encodedHash1, string(pass1))
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if !ok {
		t.Errorf("Expected password match, but got mismatch")
	}

	ok, err = authSvc.compare(encodedHash1, "wrong")
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if ok {
		t.Errorf("Expected password mismatch, but got match")
	}
}

func TestPasswordComparisonComplex(t *testing.T) {
	repo := newMockRepository(t)
	authSvc := authService{
		repo:    repo,
		time:    2,
		memory:  65536,
		threads: 4,
		keyLen:  128,
		saltLen: 128,
	}

	salt1, err := randomSecret(authSvc.saltLen)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	pass1 := generateRandomString(32)
	var encodedHash1 string
	var hash1 []byte
	hash1, err = authSvc.generateHash([]byte(pass1), salt1)
	encodedHash1, err = encodeSaltAndHash(salt1, hash1)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if encodedHash1 == "" {
		t.Errorf("Expected encoded password, but got empty")
	}

	ok, err := authSvc.compare(encodedHash1, pass1)
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if !ok {
		t.Errorf("Expected password match, but got mismatch")
	}

	ok, err = authSvc.compare(encodedHash1, "wrong")
	if err != nil {
		t.Errorf("Expected no error, but got: %s", err)
	}
	if ok {
		t.Errorf("Expected password mismatch, but got match")
	}
}
