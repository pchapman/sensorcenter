package services

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

type UserDashService interface {
	DeleteUserDash(id int64) error
	RetrieveUserDashes(userId string) ([]scom.UserDash, error)
	RetrieveUserDashById(id int64) (*scom.UserDash, error)
	RetrieveUserDashesByUserAndLocation(userId string, locationId int64) ([]scom.UserDash, error)
}

type userDashService struct {
	repo repository.Repository
}

func newUserDashService(repo repository.Repository) (UserDashService, error) {
	return &userDashService{
		repo: repo,
	}, nil
}

func (uds *userDashService) DeleteUserDash(id int64) error {
	return uds.repo.DeleteUserDashboardById(id)
}

func (uds *userDashService) RetrieveUserDashes(userId string) ([]scom.UserDash, error) {
	return uds.repo.RetrieveUserDashboardsByUser(userId)
}

func (uds *userDashService) RetrieveUserDashById(id int64) (userDash *scom.UserDash, err error) {
	userDash, err = uds.repo.RetrieveUserDashById(id)
	if err != nil {
		return nil, err
	}
	if userDash == nil {
		err = errNotFound
	}
	return
}

func (uds *userDashService) RetrieveUserDashesByUserAndLocation(userId string, locationId int64) ([]scom.UserDash, error) {
	return uds.repo.RetrieveUserDashboardsByUserAndLocation(userId, locationId)
}
