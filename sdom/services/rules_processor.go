package services

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/robertkrimen/otto"
	"gitlab.com/pchapman/go-utils/misc"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"log"
	"os"
)

type RulesProcessor interface {
	EnqueueReading(reading *scom.SensorReading)
}

type rulesProcessor struct {
	readingChannel chan scom.SensorReading
}

func (proc rulesProcessor) EnqueueReading(reading *scom.SensorReading) {
	proc.readingChannel <- *reading
}

type processor struct {
	repo           repository.Repository
	readingChannel <-chan scom.SensorReading
	sns            *sns.SNS
}

func newRulesProcessor(repo repository.Repository) (RulesProcessor, error) {
	cnt, ok := misc.Int8Env(config.EnvRulesProcessorCount)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvRulesProcessorCount)
	}

	topic := os.Getenv(config.EnvSnsTopicArn)
	if topic == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvSnsTopicArn)
	}

	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	sns := sns.New(sess)

	c := make(chan scom.SensorReading)
	for i := int8(0); i < cnt; i++ {
		go processor{repo: repo, readingChannel: c, sns: sns}.run()
	}

	return rulesProcessor{readingChannel: c}, nil
}

const varLocationId = "locationId"
const varNotify = "notify"
const varNotifyMsg = "notifyMsg"
const varReadingId = "readingId"
const varReadingTimestamp = "readingTimestamp"
const varReadingValue = "readingValue"
const varSensorId = "sensorId"
const varSensorName = "sensorName"
const varStateJSON = "stateJSON"

func (proc processor) run() {
	vm := otto.New()
	var reading scom.SensorReading
	for {
		reading = <-proc.readingChannel

		// Look for rules and their states
		rules, err := proc.repo.RetrieveRulesForSensor(reading.SensorId)
		if err != nil {
			log.Printf("Unable to locate rules for sensor %d: %s", reading.SensorId, err)
			continue
		}

		if rules != nil && len(rules) > 0 {
			sensor, err := proc.repo.RetrieveSensorById(reading.SensorId)
			if err != nil {
				log.Printf("Unable to retrieve sensor %d: %s", reading.SensorId, err)
				continue
			}
			// Set common values into VM
			if err = vm.Set(varLocationId, reading.LocationId); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varLocationId, err)
				continue
			}
			if err = vm.Set(varReadingId, reading.Id); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varReadingId, err)
				continue
			}
			if err = vm.Set(varReadingTimestamp, reading.Timestamp); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varReadingTimestamp, err)
				continue
			}
			if err = vm.Set(varReadingValue, reading.Value); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varReadingValue, err)
				continue
			}
			if err = vm.Set(varSensorId, reading.SensorId); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varSensorId, err)
				continue
			}
			if err = vm.Set(varSensorName, sensor.Name); err != nil {
				log.Printf("Unable to set rule %s variable: %s", varSensorName, err)
				continue
			}

			for _, rule := range rules {
				// Set state in VM
				if err = vm.Set(varNotify, false); err != nil {
					log.Printf("Unable to set rule %s variable: %s", varNotify, err)
					continue
				}
				if err = vm.Set(varNotifyMsg, ""); err != nil {
					log.Printf("Unable to set rule %s variable: %s", varNotifyMsg, err)
					continue
				}
				if err = vm.Set(varStateJSON, rule.State); err != nil {
					log.Printf("Unable to set rule %s variable: %s", varStateJSON, err)
					continue
				}

				// Execute rule
				if _, err = vm.Run(rule.Script); err != nil {
					log.Printf("Error executing rule %d: %s", rule.Id, err)
					continue
				}

				// Get data from VM
				msgV, err := vm.Get(varNotifyMsg)
				if err != nil {
					log.Printf("Unable to retrieve %s after executing rule %d: %s", varNotifyMsg, rule.Id, err)
					continue
				}
				msg, err := msgV.ToString()
				if err != nil {
					log.Printf("Unable to retrieve %s after executing rule %d: %s", varNotifyMsg, rule.Id, err)
					continue
				}
				notifyV, err := vm.Get(varNotify)
				if err != nil {
					log.Printf("Unable to determine if we should notify the user after executing rule %d: %s", rule.Id, err)
					continue
				}
				notify, err := notifyV.ToBoolean()
				if err != nil {
					log.Printf("Unable to determine if we should notify the user after executing rule %d: %s", rule.Id, err)
					continue
				}
				stateV, err := vm.Get(varStateJSON)
				if err != nil {
					log.Printf("Unable retrieve %s after executing rule %d: %s", varStateJSON, rule.Id, err)
					continue
				}
				rule.State, err = stateV.ToString()
				if err != nil {
					log.Printf("Unable retrieve %s after executing rule %d: %s", varStateJSON, rule.Id, err)
					continue
				}

				// Save rules state
				err = proc.repo.UpdateRule(&rule)
				if err != nil {
					log.Printf("Error saving state after executing rule %d: %s", rule.Id, err)
					continue
				}

				// Notify Users
				if notify {
					err = proc.notifyUsers(sensor, rule, &reading, msg)
					log.Printf("Unable to notify users of state change for rule %d: %s", rule.Id, err)
				}
			}
		}
	}
}

func (proc processor) notifyUsers(sensor *scom.Sensor, rule scom.Rule, _ *scom.SensorReading, msg string) error {
	_, err := proc.sns.Publish(&sns.PublishInput{
		Message:  &msg,
		Subject:  aws.String(fmt.Sprintf("sensor %s", sensor.Name)),
		TopicArn: &rule.NotificationTopic,
	})
	return err
}
