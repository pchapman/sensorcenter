package services

import (
	"errors"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

type SdomServices struct {
	AuthService     AuthService
	LocationService LocationService
	ReporterService ReporterService
	SensorService   SensorService
	UserDashService UserDashService
}

func NewSdomServices(repo repository.Repository) (*SdomServices, error) {
	var err error
	svc := &SdomServices{}
	svc.AuthService, err = NewAuthService(repo)
	if err != nil {
		return nil, err
	}
	svc.LocationService, err = newLocationService(repo)
	if err != nil {
		return nil, err
	}
	svc.ReporterService, err = newReporterService(repo)
	if err != nil {
		return nil, err
	}
	rulesProc, err := newRulesProcessor(repo)
	if err != nil {
		return nil, err
	}
	svc.SensorService, err = newSensorService(repo, rulesProc)
	if err != nil {
		return nil, err
	}
	svc.UserDashService, err = newUserDashService(repo)
	if err != nil {
		return nil, err
	}
	return svc, nil
}

var errNotFound = errors.New("not found")

func IsNotFound(err error) bool {
	return errors.Is(err, errNotFound)
}
