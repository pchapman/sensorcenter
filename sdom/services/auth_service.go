package services

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/pchapman/go-utils/misc"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"golang.org/x/crypto/argon2"
	"io/ioutil"
	"log"
	mrand "math/rand"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type AccessTokenErrorResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	ErrorCodes       []int  `json:"error_codes"`
	Timestamp        string `json:"timestamp"`
	TraceId          string `json:"trace_id"`
	CorrelationId    string `json:"correlation_id"`
}

type AccessTokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int64  `json:"expires_in"`
	ExpiresOn    int64  `json:"expires_on"`
	NotBefore    int64  `json:"not_before"`
	Resource     string `json:"resource:"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
}

type User struct {
	Id        string `json:"id"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	NickName  string `json:"nickname"`
}

func (u *User) Name() string {
	return fmt.Sprintf("%s %s", u.FirstName, u.LastName)
}

type AuthService interface {
	AuthenticateReporter(keyId string, password string) (reporter *scom.Reporter, err error)
	AuthenticateUserDash(keyId string, password string) (userDash *scom.UserDash, err error)
	AuthenticateUserToken(token string) (*User, error)
	AuthorizeReporter(reporterId int64) (sensorIDs []int64, err error)
	AuthorizeUser(userId string) (locationIDs []int64, sensorIDs []int64, err error)
	AuthorizeUserDash(dashId int64) (sensorIDs []int64, err error)
	CreateReporter(locationId int64, name string) (reporter *scom.Reporter, password string, err error)
	CreateUserDash(userId string, locationId int64) (userDash *scom.UserDash, password string, err error)
	ObtainAccessToken(code, callbackUrl string) (accessToken string, expiresIn int64, err error)
	OAuthProviderLoginURL(state, callbackUrl string) string
	RotateUserDashPassword(userDashId int64) (password string, err error)
	RotateReporterPassword(reporterId int64) (password string, err error)
}

type authService struct {
	repo              repository.Repository
	time              uint32 // time represents the number of passes over the specified memory during generating hash keys
	memory            uint32 // memory is the memory to be used for generating hash keys.
	threads           uint8  // threads is the number of threads to be used for generating hash keys.
	keyLen            uint32 // keyLen is the length of generated hash keys.
	saltLen           uint32 // saltLen is the length of a generated salt used for generating hash keys.
	oAuthClientId     string
	oAuthClientSecret string
	oAuthProviderUrl  string
	passwordLen       uint8 // passwordLen is the length for randomly generated passwords
}

func NewAuthService(repo repository.Repository) (AuthService, error) {
	svc := &authService{
		passwordLen: 32,
		repo:        repo,
	}

	svc.oAuthClientId = os.Getenv(config.EnvAuthClientId)
	if svc.oAuthClientId == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvAuthClientId)
	}
	svc.oAuthClientSecret = os.Getenv(config.EnvAuthClientSecret)
	if svc.oAuthClientSecret == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvAuthClientSecret)
	}
	svc.oAuthProviderUrl = os.Getenv(config.EnvAuthProviderUrl)
	if svc.oAuthProviderUrl == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvAuthProviderUrl)
	}

	var ok bool
	svc.time, ok = misc.UInt32Env(config.EnvPWHashTime)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvPWHashTime)
	}
	svc.memory, ok = misc.UInt32Env(config.EnvPWHashMemory)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvPWHashMemory)
	}
	svc.threads, ok = misc.UInt8Env(config.EnvPWHashThreads)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvPWHashThreads)
	}
	svc.keyLen, ok = misc.UInt32Env(config.EnvPWHashKeyLen)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvPWHashKeyLen)
	}
	svc.saltLen, ok = misc.UInt32Env(config.EnvPWHashSaltLen)
	if !ok {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvPWHashSaltLen)
	}

	return svc, nil
}

var notFoundError error = errors.New("invalid user")

func IsNotFoundError(err error) bool {
	return err == notFoundError
}

func (a *authService) AuthenticateReporter(keyId string, password string) (reporter *scom.Reporter, err error) {
	reporter, err = a.repo.RetrieveReporterByKeyId(keyId)
	if err != nil {
		return nil, err
	}
	if reporter == nil {
		return nil, notFoundError
	}
	// Compare stored hash with password
	ok, err := a.compare(reporter.AuthHash, password)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, fmt.Errorf("invalid password")
	}
	return reporter, nil
}

func (a *authService) AuthenticateUserDash(keyId string, password string) (userDash *scom.UserDash, err error) {
	userDash, err = a.repo.RetrieveUserDashByKeyId(keyId)
	if err != nil {
		return nil, err
	}
	if userDash == nil {
		return nil, notFoundError
	}
	// Compare stored hash with password
	ok, err := a.compare(userDash.AuthHash, password)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, fmt.Errorf("invalid password")
	}
	return userDash, nil
}

func (a *authService) AuthenticateUserToken(token string) (*User, error) {
	// Get user info using the bearer token for authentication
	var user User
	u := fmt.Sprintf("%s/api/v4/users/me", a.oAuthProviderUrl)
	req, _ := http.NewRequest(http.MethodGet, u, nil)
	req.Header.Add("accept", "application/json")
	req.Header.Add("authorization", fmt.Sprintf("Bearer %s", token))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	} else {
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		if resp.StatusCode == http.StatusOK {
			err = json.Unmarshal(data, &user)
			if err != nil {
				return nil, err
			}
		} else if resp.StatusCode == http.StatusUnauthorized {
			return nil, nil
		} else {
			return nil, errors.New("invalid response")
		}
	}
	return &user, nil
}

func (a *authService) AuthorizeReporter(reporterId int64) (sensorIDs []int64, err error) {
	return a.repo.RetrieveSensorIDsForReporter(reporterId)
}

func (a *authService) AuthorizeUser(userId string) (locationIDs []int64, sensorIDs []int64, err error) {
	locationIDs, err = a.repo.RetrieveLocationIDsForUser(userId)
	if err != nil {
		return nil, nil, err
	}
	sensorIDs, err = a.repo.RetrieveSensorIDsForUser(userId)
	return locationIDs, sensorIDs, err
}

func (a *authService) AuthorizeUserDash(dashId int64) (sensorIDs []int64, err error) {
	return a.repo.RetrieveSensorIDsForUserDash(dashId)
}

func (a *authService) CreateReporter(locationId int64, name string) (reporter *scom.Reporter, password string, err error) {
	if name == "" {
		return nil, "", fmt.Errorf("name cannot be empty")
	}
	l, err := a.repo.RetrieveLocationById(locationId)
	if err != nil {
		return nil, "", err
	}
	if l == nil {
		return nil, "", fmt.Errorf("invalid location ID")
	}

	var exists bool
	externalId := ""
	for externalId == "" {
		externalId = generateRandomString(24)
		exists, err = a.repo.ReporterKeyIdExists(externalId)
		if err != nil {
			log.Printf("Error checking if external ID exists: %s", err)
			// Default to using a UUID
			externalId = uuid.New().String()
		}
		if exists {
			externalId = ""
		}
	}

	reporter = &scom.Reporter{
		KeyId:      externalId,
		LocationId: locationId,
		Name:       name,
	}

	password, reporter.AuthHash, err = a.createNewPassword()
	if err != nil {
		return nil, "", err
	}

	reporter.Id, err = a.repo.InsertReporter(reporter)
	if err != nil {
		return nil, "", err
	}
	return
}

func (a *authService) CreateUserDash(userId string, locationId int64) (userDash *scom.UserDash, password string, err error) {
	if userId == "" {
		return nil, "", fmt.Errorf("user ID cannot be empty")
	}
	l, err := a.repo.RetrieveLocationById(locationId)
	if err != nil {
		return nil, "", err
	}
	if l == nil {
		return nil, "", fmt.Errorf("invalid location ID")
	}

	var exists bool
	keyId := ""
	for keyId == "" {
		keyId = generateRandomString(24)
		exists, err = a.repo.ReporterKeyIdExists(keyId)
		if err != nil {
			log.Printf("Error checking if key ID exists: %s", err)
			// Default to using a UUID
			keyId = uuid.New().String()
		}
		if exists {
			keyId = ""
		}
	}

	userDash = &scom.UserDash{
		KeyId:      keyId,
		LocationId: locationId,
		UserId:     userId,
	}

	password, userDash.AuthHash, err = a.createNewPassword()
	if err != nil {
		return nil, "", err
	}

	userDash.Id, err = a.repo.InsertUserDash(userDash)
	if err != nil {
		return nil, "", err
	}
	return
}

func (a *authService) OAuthProviderLoginURL(state, callbackUrl string) string {
	u := url.QueryEscape(callbackUrl)
	u = fmt.Sprintf("%s/oauth/authorize?response_type=code&client_id=%s&redirect_uri=%s&state=%s", a.oAuthProviderUrl, a.oAuthClientId, u, state)
	return u
}

func (a *authService) ObtainAccessToken(code, callbackUrl string) (accessToken string, expiresIn int64, err error) {
	// Call token URI at /oauth/access_token to verify the token and obtain an access token
	var atresp AccessTokenResponse
	u := url.QueryEscape(callbackUrl)
	u = fmt.Sprintf("%s/oauth/access_token?grant_type=authorization_code&client_id=%s&client_secret=%s&code=%s&redirect_uri=%s", a.oAuthProviderUrl, a.oAuthClientId, a.oAuthClientSecret, code, u)
	req, _ := http.NewRequest(http.MethodPost, u, nil)
	req.Header.Add("accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", 0, err
	} else {
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		if resp.StatusCode != 200 {
			eresp := new(AccessTokenErrorResponse)
			err = json.Unmarshal(data, &eresp)
			if err == nil {
				log.Printf("Error from token request:\t%s\t%s", eresp.Error, eresp.ErrorDescription)
				return "", 0, errors.New(eresp.Error)
			} else {
				return "", 0, err
			}
		} else {
			err = json.Unmarshal(data, &atresp)
			if err != nil {
				return "", 0, err
			}
		}
	}
	return atresp.AccessToken, atresp.ExpiresIn, nil
}

const validRandomChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func generateRandomString(strlen uint8) string {
	var j int
	id := ""
	l := len(validRandomChars)
	for i := 0; i < int(strlen); i++ {
		j = mrand.Intn(l)
		id += string(rune(validRandomChars[j]))
	}
	return id
}

func (a *authService) RotateReporterPassword(reporterId int64) (password string, err error) {
	reporter, err := a.repo.RetrieveReporterById(reporterId)
	if err != nil {
		return "", err
	}
	if reporter == nil {
		return "", fmt.Errorf("invalid reporter ID")
	}

	password, authHash, err := a.createNewPassword()
	if err != nil {
		return "", err
	}

	err = a.repo.UpdateReporterAuthHash(reporterId, authHash)
	if err != nil {
		return "", err
	}
	return password, nil
}

func (a *authService) RotateUserDashPassword(userDashId int64) (password string, err error) {
	userDash, err := a.repo.RetrieveUserDashById(userDashId)
	if err != nil {
		return "", err
	}
	if userDash == nil {
		return "", fmt.Errorf("invalid user dash ID")
	}

	password, authHash, err := a.createNewPassword()
	if err != nil {
		return "", err
	}

	err = a.repo.UpdateUserDashAuthHash(userDashId, authHash)
	if err != nil {
		return "", err
	}
	return password, nil
}

func (a *authService) generateHash(password, salt []byte) (hash []byte, err error) {
	if password == nil || len(password) == 0 {
		return nil, fmt.Errorf("no password provided")
	}
	if salt == nil || len(salt) == 0 {
		return nil, fmt.Errorf("no salt provided")
	}
	// Generate hash
	hash = argon2.IDKey(password, salt, a.time, a.memory, a.threads, a.keyLen)
	// Return the generated hash and salt used for storage.
	return hash, nil
}

func (a *authService) compare(storedHash, password string) (bool, error) {
	salt, hash, err := decodeSaltAndHash(storedHash)
	if err != nil {
		return false, err
	}
	hash2, err := a.generateHash([]byte(password), salt)
	if err != nil {
		return false, err
	}
	return bytes.Equal(hash, hash2), nil
}

func decodeSaltAndHash(s string) (salt []byte, hash []byte, err error) {
	parts := strings.Split(s, "$$")
	if len(parts) != 2 {
		return nil, nil, fmt.Errorf("invalid hash format")
	}
	salt, err = base64.StdEncoding.DecodeString(parts[0])
	if err != nil {
		return
	}
	hash, err = base64.StdEncoding.DecodeString(parts[1])
	if err != nil {
		return
	}
	return
}

func encodeSaltAndHash(salt, hash []byte) (string, error) {
	result := base64.StdEncoding.EncodeToString(salt)
	result += "$$"
	result += base64.StdEncoding.EncodeToString(hash)
	return result, nil
}

func randomSecret(length uint32) ([]byte, error) {
	secret := make([]byte, length)

	_, err := rand.Read(secret)
	if err != nil {
		return nil, err
	}

	return secret, nil
}

func (a *authService) createNewPassword() (password, saltAndHash string, err error) {
	salt, err := randomSecret(a.saltLen)
	if err != nil {
		return "", "", err
	}
	password = generateRandomString(32)

	hash, err := a.generateHash([]byte(password), salt)
	if err != nil {
		return "", "", err
	}

	saltAndHash, err = encodeSaltAndHash(salt, hash)
	if err != nil {
		return "", "", err
	}

	return password, saltAndHash, nil
}
