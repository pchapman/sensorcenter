package services

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

type ReporterService interface {
	DeleteReporter(id int64) error
	RetrieveReporterById(id int64) (*scom.Reporter, error)
	RetrieveReportersByLocation(locationId int64) ([]scom.Reporter, error)
}

type reporterService struct {
	repo repository.Repository
}

func newReporterService(repo repository.Repository) (ReporterService, error) {
	return &reporterService{
		repo: repo,
	}, nil
}

func (s *reporterService) DeleteReporter(id int64) error {
	return s.repo.DeleteReporterById(id)
}

func (s *reporterService) RetrieveReporterById(reporterId int64) (r *scom.Reporter, err error) {
	r, err = s.repo.RetrieveReporterById(reporterId)
	if err != nil {
		return
	}
	if r == nil {
		err = errNotFound
	}
	return
}

func (s *reporterService) RetrieveReportersByLocation(locationId int64) ([]scom.Reporter, error) {
	return s.repo.RetrieveReportersForLocation(locationId)
}
