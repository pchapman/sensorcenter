package services

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

type LocationService interface {
	CreateLocation(loc *scom.Location, userId string) (int64, error)
	DeleteLocation(locationId int64) error
	RetrieveAllLocations() ([]scom.Location, error)
	RetrieveLocationById(locationId int64) (*scom.Location, error)
	RetrieveLocationByName(name string) (*scom.Location, error)
	UpdateLocation(loc *scom.Location) error
	ValidateLocation(loc *scom.Location) ([]string, error)
}

type locationService struct {
	repo repository.Repository
}

func newLocationService(repo repository.Repository) (LocationService, error) {
	return &locationService{
		repo: repo,
	}, nil
}

func (ls *locationService) CreateLocation(loc *scom.Location, userId string) (id int64, err error) {
	id, err = ls.repo.InsertLocation(loc)
	if err != nil {
		return
	}
	err = ls.repo.InsertUserLocation(userId, id)
	return
}

func (ls *locationService) DeleteLocation(locationId int64) error {
	var err error
	err = ls.repo.DeleteRulesForLocation(locationId)
	if err != nil {
		return err
	}
	err = ls.repo.DeleteSensorReadingsForLocation(locationId)
	if err != nil {
		return err
	}
	err = ls.repo.DeleteSensorsForLocation(locationId)
	if err != nil {
		return err
	}
	err = ls.repo.DeleteReportersForLocation(locationId)
	if err != nil {
		return err
	}
	err = ls.repo.DeleteUserDashboardsForLocation(locationId)
	if err != nil {
		return err
	}
	err = ls.repo.DeleteUserLocationsForLocation(locationId)
	if err != nil {
		return err
	}
	return ls.repo.DeleteLocationById(locationId)
}

func (ls *locationService) RetrieveAllLocations() (locations []scom.Location, err error) {
	return ls.repo.RetrieveAllLocations()
}

func (ls *locationService) RetrieveLocationById(locationId int64) (location *scom.Location, err error) {
	location, err = ls.repo.RetrieveLocationById(locationId)
	if err != nil {
		return
	}
	if location == nil {
		err = errNotFound
	}
	return
}

func (ls *locationService) RetrieveLocationByName(name string) (location *scom.Location, err error) {
	return ls.repo.RetrieveLocationByName(name)
}

func (ls *locationService) UpdateLocation(loc *scom.Location) error {
	return ls.repo.UpdateLocation(loc)
}

func (ls *locationService) ValidateLocation(loc *scom.Location) (validationErrors []string, err error) {
	validationErrors = make([]string, 0)

	if loc.Name == "" {
		validationErrors = append(validationErrors, "Name cannot be empty")
	} else {
		var l2 *scom.Location
		l2, err = ls.repo.RetrieveLocationByName(loc.Name)
		if err != nil {
			return
		}
		if l2 != nil && l2.Id != loc.Id {
			validationErrors = append(validationErrors, "Name must be unique")
		}
	}
	if loc.Latitude != nil && (*loc.Latitude < -90 || *loc.Latitude > 90) {
		validationErrors = append(validationErrors, "Latitude must be between -90 and 90")
	}
	if loc.Longitude != nil && (*loc.Longitude < -180 || *loc.Longitude > 180) {
		validationErrors = append(validationErrors, "Longitude must be between -180 and 180")
	}
	if loc.Latitude != nil || loc.Longitude != nil {
		var l2 *scom.Location
		l2, err = ls.repo.RetrieveLocationByCoordinates(loc.Latitude, loc.Longitude)
		if err != nil {
			return
		}
		if l2 != nil && l2.Id != loc.Id {
			validationErrors = append(validationErrors, "Coordinates must be unique")
		}
	}

	return
}
