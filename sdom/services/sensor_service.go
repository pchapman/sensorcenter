package services

import (
	"fmt"
	gocache "github.com/patrickmn/go-cache"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/ssub/ssub_config"
	"sort"
	"time"
)

type SensorService interface {
	CreateSensor(sensor *scom.Sensor) (id int64, err error)
	DeleteSensor(id int64) error
	ReportReading(sensor *scom.Sensor, unixTime int64, value float64) (*scom.SensorReading, error)
	RetrieveAllSensors() ([]scom.Sensor, error)
	RetrieveSensorById(sensorId int64) (*scom.Sensor, error)
	RetrieveLatestSensorReadingsForLocation(locationId int64) (readings *scom.LocationSensorReadings, err error)
	UpdateSensor(sensor *scom.Sensor) error
	RetrieveSensorsByLocation(locationId int64) (sensors []scom.Sensor, err error)
	ValidateSensor(sensor *scom.Sensor) ([]string, error)
}

type sensorService struct {
	cache     *gocache.Cache
	repo      repository.Repository
	rulesPorc RulesProcessor
}

func newSensorService(repo repository.Repository, rulesProc RulesProcessor) (SensorService, error) {
	return &sensorService{
		cache:     gocache.New(5*time.Minute, 10*time.Minute),
		repo:      repo,
		rulesPorc: rulesProc,
	}, nil
}

func (ss *sensorService) getCachedSensorById(sensorId int64) (*scom.Sensor, bool) {
	v, ok := ss.cache.Get(fmt.Sprintf("SensorById:%d", sensorId))
	if !ok {
		return nil, ok
	}
	s, ok := v.(scom.Sensor)
	return &s, ok
}

func (ss *sensorService) getLatestCachedSensorReadingBySensorId(sensorId int64) (*scom.SensorReading, bool) {
	v, ok := ss.cache.Get(fmt.Sprintf("SensorReadingBySensorId:%d", sensorId))
	if !ok {
		return nil, ok
	}
	sr, ok := v.(scom.SensorReading)
	return &sr, ok
}

func (ss *sensorService) getCachedSensorsByLocation(locationId int64) ([]scom.Sensor, bool) {
	v, ok := ss.cache.Get(fmt.Sprintf("SensorsByLocationId:%d", locationId))
	if !ok {
		return nil, ok
	}
	return v.([]scom.Sensor), ok
}

type sensorsByName []scom.Sensor

func (s sensorsByName) Len() int {
	return len(s)
}

func (s sensorsByName) Less(i, j int) bool {
	return s[i].Name < s[j].Name
}

func (s sensorsByName) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (ss *sensorService) cacheSensor(sensor *scom.Sensor) {
	ss.cache.Set(fmt.Sprintf("SensorById:%d", sensor.Id), *sensor, gocache.DefaultExpiration)
	sensors, ok := ss.getCachedSensorsByLocation(sensor.LocationId)
	if !ok {
		return
	}
	found := false
	var newArray sensorsByName
	for _, s := range sensors {
		if s.Id == sensor.Id {
			newArray = append(newArray, *sensor)
			found = true
		} else {
			newArray = append(newArray, s)
		}
	}
	if !found {
		newArray = append(newArray, *sensor)
	}
	// Sort newArray by sensor name
	sort.Sort(newArray)
	ss.cache.Set(fmt.Sprintf("SensorsByLocationId:%d", sensor.LocationId), newArray, gocache.DefaultExpiration)
}

func (ss *sensorService) cacheSensorReading(reading *scom.SensorReading) {
	if reading == nil {
		return
	}
	ss.cache.Set(fmt.Sprintf("SensorReadingBySensorId:%d", reading.SensorId), *reading, gocache.DefaultExpiration)
}

func (ss *sensorService) cacheSensorsForLocation(locationId int64, sensors []scom.Sensor) {
	for _, sensor := range sensors {
		if sensor.LocationId != locationId {
			return // Bad data
		}
		ss.cache.Set(fmt.Sprintf("SensorById:%d", sensor.Id), sensor, gocache.DefaultExpiration)
	}
	ss.cache.Set(fmt.Sprintf("SensorsByLocationId:%d", locationId), sensors, gocache.DefaultExpiration)
}

func (ss *sensorService) CreateSensor(sensor *scom.Sensor) (id int64, err error) {
	sensor.Id, err = ss.repo.InsertSensor(sensor)
	if err != nil {
		return 0, err
	}
	ss.cacheSensor(sensor)
	return sensor.Id, nil
}

func (ss *sensorService) DeleteSensor(sensorId int64) error {
	var err error
	err = ss.repo.DeleteRulesForSensor(sensorId)
	if err != nil {
		return err
	}
	err = ss.repo.DeleteSensorReadingsForSensor(sensorId)
	if err != nil {
		return err
	}
	return ss.repo.DeleteSensorById(sensorId)
}

func (ss *sensorService) ReportReading(sensor *scom.Sensor, unixTime int64, value float64) (*scom.SensorReading, error) {
	reading, err := scom.NewSensorReading(sensor, unixTime, value)
	if err != nil {
		return nil, err
	}
	reading.Id, err = ss.repo.InsertSensorReading(reading)
	if err != nil {
		return nil, err
	}

	ss.cacheSensorReading(reading)
	ss.rulesPorc.EnqueueReading(reading)
	return reading, nil
}

func (ss *sensorService) RetrieveAllSensors() (sensors []scom.Sensor, err error) {
	return ss.repo.RetrieveAllSensors()
}

func (ss *sensorService) RetrieveLatestSensorReadingsForLocation(locationId int64) (*scom.LocationSensorReadings, error) {
	location, err := ss.repo.RetrieveLocationById(locationId)
	if err != nil {
		return nil, err
	}
	if location == nil {
		return nil, errNotFound
	}

	readings := &scom.LocationSensorReadings{
		LocationId:   locationId,
		LocationName: location.Name,
		Readings:     []scom.SensorReadingElem{},
	}

	sensors, ok := ss.getCachedSensorsByLocation(locationId)
	if !ok {
		sensors, err = ss.repo.RetrieveSensorsForLocation(locationId)
		if err != nil {
			return nil, err
		}
		ss.cacheSensorsForLocation(locationId, sensors)
	}

	var elem *scom.SensorReadingElem
	for _, sensor := range sensors {
		elem = new(scom.SensorReadingElem)
		elem.Sensor = sensor.Copy()
		elem.Reading, ok = ss.getLatestCachedSensorReadingBySensorId(sensor.Id)
		if !ok {
			elem.Reading, err = ss.repo.RetrieveLatestSensorReading(sensor.Id)
			if err != nil {
				return nil, err
			}
			ss.cacheSensorReading(elem.Reading)
		}
		if elem.Reading != nil {
			readings.Readings = append(readings.Readings, *elem)
		}
	}
	readings.Reported = time.Now()
	return readings, nil
}

func (ss *sensorService) RetrieveSensorById(sensorId int64) (sensor *scom.Sensor, err error) {
	sensor, ok := ss.getCachedSensorById(sensorId)
	if ok {
		return sensor, nil
	}

	sensor, err = ss.repo.RetrieveSensorById(sensorId)
	if err != nil {
		return nil, err
	}
	if sensor == nil {
		err = errNotFound
	} else {
		ss.cacheSensor(sensor)
	}
	return
}

func (ss *sensorService) UpdateSensor(sensor *scom.Sensor) error {
	err := ss.repo.UpdateSensor(sensor)
	if err != nil {
		return err
	}
	ss.cacheSensor(sensor)
	return nil
}

func (ss *sensorService) RetrieveSensorsByLocation(locationId int64) (sensors []scom.Sensor, err error) {
	return ss.repo.RetrieveSensorsForLocation(locationId)
}

func (ss *sensorService) ValidateSensor(sensor *scom.Sensor) ([]string, error) {
	problems := []string{}

	if sensor.Name == "" {
		problems = append(problems, "Sensor name cannot be empty.")
	}
	s, err := ss.repo.RetrieveSensorByName(sensor.Name)
	if err != nil {
		return nil, err
	}
	if s != nil && s.Id != sensor.Id {
		problems = append(problems, fmt.Sprintf("Sensor name \"%s\" already exists.", sensor.Name))
	}

	if !scom.ValidateSensorType(string(sensor.Type)) {
		problems = append(problems, fmt.Sprintf("Invalid sensor type %s.", sensor.Type))
	}

	if !scom.ValidateSchedule(sensor.Schedule) {
		problems = append(problems, fmt.Sprintf("Invalid schedule \"%s\".", sensor.Schedule))
	}

	if sensor.StoreUnit == "" {
		problems = append(problems, "Sensor stored unit cannot be empty.")
	}

	if sensor.DisplayUnit == "" {
		problems = append(problems, "Sensor display unit cannot be empty.")
	}

	if sensor.DisplayConversion == "" {
		if sensor.StoreUnit != sensor.DisplayUnit {
			problems = append(problems, "Sensor store and display units differ. A conversion formula must be provided.")
		}
	} else {
		v, r := scom.ValidateDisplayConversion(sensor.DisplayConversion)
		if !v {
			problems = append(problems, fmt.Sprintf("Invalid display unit conversion: \"%s\".", r))
		}
	}

	err = ssub_config.ValidateConfiguration(sensor)
	if err != nil {
		problems = append(problems, fmt.Sprintf("In sensor configuration: %s", err.Error()))
	}

	return problems, nil
}
