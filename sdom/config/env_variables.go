package config

const EnvAuthCallbackUrl = "SC_USER_AUTH_CALLBACK_URL"
const EnvAuthClientId = "SC_USER_AUTH_CLIENT_ID"
const EnvAuthClientSecret = "SC_USER_AUTH_CLIENT_SECRET"
const EnvAuthProviderUrl = "SC_USER_AUTH_PROVIDER_URL"

// EnvDbUrl is the name of the environment variable containing the database connection URL
const EnvDbUrl = "SC_DB_URL"

const EnvPWHashTime = "SC_PW_HASH_TIME"
const EnvPWHashSaltLen = "SC_PW_HASH_SALT_LEN"
const EnvPWHashMemory = "SC_PW_HASH_MEMORY"
const EnvPWHashThreads = "SC_PW_HASH_THREADS"
const EnvPWHashKeyLen = "SC_PW_HASH_KEY_LEN"

const EnvListenAddress = "SC_LISTEN_ADDRESS"
const EnvListenPort = "SC_LISTEN_PORT"
const EnvSessionsSecret = "SC_SESSIONS_SECRET"

const EnvRulesProcessorCount = "SC_RULES_PROCESSOR_COUNT"
const EnvSnsTopicArn = "SC_SNS_TOPIC_ARN"
