package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/dashcmd"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/locationcmd"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/sensorcmd"
	"os"
)

var rootCmd = &cobra.Command{}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(serveCmd, initializeCmd, locationcmd.LocationCmd, dashcmd.DashCmd, sensorcmd.SensorCmd)
}
