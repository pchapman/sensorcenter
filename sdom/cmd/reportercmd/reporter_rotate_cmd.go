package reportercmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
)

var reporterRotateCmd = &cobra.Command{
	Use:   "rotate",
	Short: "Rotate API Key",
	Long:  `Rotates the password for a reporter's API Key`,
	Run:   runDashRotateCmd,
}

var (
	reporterKeyId string
)

func initReporterRotateCmd() {
	reporterRotateCmd.Flags().StringVarP(&reporterKeyId, "reporter_key_id", "d", "", "Reporter API Key ID")
}

func runDashRotateCmd(cmd *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	if reporterKeyId == "" {
		reporterKeyId = cmdutils.QueryStringField("reporter api key id")
		if reporterKeyId == "" {
			return
		}
	}
	reporter, err := repo.RetrieveReporterByKeyId(reporterKeyId)
	if err != nil {
		panic(err)
	}
	if reporter != nil {
		fmt.Printf("The reporter API key %s does not exist\n", reporterKeyId)
		return
	}
	passwd, err := svcs.AuthService.RotateUserDashPassword(reporter.Id)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Make note of the new password: %s\n", passwd)
}
