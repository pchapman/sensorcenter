package reportercmd

import "github.com/spf13/cobra"

var ReporterCmd = &cobra.Command{
	Use:   "reporter",
	Short: "Manages reporters",
	Long:  `Manages sensor reading reporters`,
}

func init() {
	initReporterAddCmd()
	initReporterRotateCmd()
	ReporterCmd.AddCommand(reporterAddCmd, reporterRotateCmd)
}
