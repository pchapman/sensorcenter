package reportercmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
)

var reporterAddCmd = &cobra.Command{
	Use:   "add [-l location_id] [-n name]",
	Short: "Adds sensor reporter",
	Long:  `Adds a reporter for sensor readings`,
	Run:   runReporterAddCmd,
}

var (
	locationId int64
	name       string
)

func initReporterAddCmd() {
	reporterAddCmd.Flags().Int64VarP(&locationId, "location", "l", 0, "Location ID")
	reporterAddCmd.Flags().StringVarP(&name, "name", "n", "", "Reporter name")
}

func runReporterAddCmd(cmd *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	if locationId == 0 {
		locationId = cmdutils.QueryForLocationId(repo, nil)
		if locationId == 0 {
			return
		}
	}

	if name == "" {
		name = cmdutils.QueryStringField("Name")
		if name == "" {
			return
		}
	}

	CreateReporter(repo, svcs.AuthService, locationId, &name)
}

func CreateReporter(repo repository.Repository, service services.AuthService, locid int64, name *string) int64 {
	if name == nil || *name == "" {
		n := cmdutils.QueryStringField("Reporter Name")
		if n == "" {
			fmt.Printf("Reporter name not provided.")
			return 0
		}
		name = &n
	}
	for {
		exists, err := repo.ReporterNameExists(*name)
		if err != nil {
			panic(err)
		}
		if !exists {
			break
		}
		fmt.Printf("Reporter %s already exists.", *name)
		n := cmdutils.QueryStringField("Name")
		if n == "" {
			fmt.Printf("Reporter name not provided.")
			return 0
		}
		name = &n
	}
	rptr, pwd, err := service.CreateReporter(locid, *name)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Reporter %s created. Make note of the following:\n", *name)
	fmt.Printf("\tAPI Key ID: %s\n", rptr.KeyId)
	fmt.Printf("\tAPI Password: %s\n", pwd)
	return rptr.Id
}
