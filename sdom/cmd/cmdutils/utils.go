package cmdutils

import (
	"bufio"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"os"
	"strconv"
	"strings"
)

func QueryFloat32PField(fieldName string) *float32 {
	f := float32(*queryFloatPField(fieldName, 32))
	return &f
}

func QueryFloat64PField(fieldName string) *float64 {
	return queryFloatPField(fieldName, 64)
}

func queryFloatPField(fieldName string, bitSize int) *float64 {
	input := QueryStringField(fieldName)
	if input == "" {
		return nil
	}
	f, err := strconv.ParseFloat(input, bitSize)
	if err != nil {
		fmt.Printf("%s should be a numeric value\n", fieldName)
		return queryFloatPField(fieldName, bitSize)
	}
	return &f
}

func QueryStringField(fieldName string) string {
	//fmt.Printf("%s: ", fieldName)
	//var input string
	//_, err := fmt.Scanln(&input)
	//if err != nil {
	//	panic(err)
	//}
	//return input

	reader := bufio.NewReader(os.Stdin)

	fmt.Printf("%s: ", fieldName)
	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		return ""
	}
	return strings.TrimSuffix(input, "\n")
}

func queryIntPField(fieldName string) *int64 {
	input := QueryStringField(fieldName)
	if input == "" {
		return nil
	}
	i, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		fmt.Printf("%s should be a numeric value\n", fieldName)
		return queryIntPField(fieldName)
	}
	return &i
}

func QueryInt64PField(fieldName string) *int64 {
	return queryIntPField(fieldName)
}

func QueryStringPField(fieldName string) *string {
	input := QueryStringField(fieldName)
	if input == "" {
		return nil
	}
	return &input
}

func QueryForLocationId(repo repository.Repository, userId *string) int64 {
	var err error
	var locations []scom.Location
	if userId == nil || *userId == "" {
		locations, err = repo.RetrieveAllLocations()
	} else {
		locations, err = repo.RetrieveLocationsForUser(*userId)
	}
	if err != nil {
		panic(err)
	}
	if locations != nil && len(locations) != 0 {
		for idx, loc := range locations {
			fmt.Printf("%d: %s\n", idx+1, loc.Name)
		}
		fmt.Printf("Select a location by number or (q)uit: ")
		input := ""
		_, err = fmt.Scanln(&input)
		if err != nil {
			panic(err)
		}
		if strings.ToLower(input) == "q" {
			return 0
		}
		i, err := strconv.Atoi(input)
		if err != nil {
			panic(err)
		}
		if i < 0 || i > len(locations) {
			fmt.Println("Invalid input")
			return QueryForLocationId(repo, userId)
		}

		return locations[i-1].Id
	}
	fmt.Println("No locations found")

	return 0
}

func QueryForYesNo(question string) (bool, error) {
	fmt.Printf("%s (y/n): ", question)
	input := ""
	_, err := fmt.Scanln(&input)
	if err != nil {
		return false, err
	}
	return strings.ToLower(input) == "y", nil
}
