package dashcmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
)

var dashAddCmd = &cobra.Command{
	Use:   "add [-l location_id] [-u user_id]",
	Short: "Adds a dashboard for a user",
	Long:  `Adds a dashboard for a user`,
	Run:   runDashAddCmd,
}

var (
	locationId int64
	userId     string
)

func initDashAddCmd() {
	dashAddCmd.Flags().Int64VarP(&locationId, "location_id", "l", 0, "location id")
	dashAddCmd.Flags().StringVarP(&userId, "user_id", "u", "", "user id")
}

func runDashAddCmd(cmd *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	if userId == "" {
		userId = cmdutils.QueryStringField("User Id")
	}
	if locationId == 0 {
		locationId = cmdutils.QueryForLocationId(repo, &userId)
	}
	dashes, err := repo.RetrieveUserDashboardsByUserAndLocation(userId, locationId)
	if err != nil {
		panic(err)
	}
	if len(dashes) > 0 {
		msg := fmt.Sprintf("The user %s already has %d dashboard(s) for location %d. Continue?", userId, len(dashes), locationId)
		cont, err := cmdutils.QueryForYesNo(msg)
		if err != nil {
			panic(err)
		}
		if !cont {
			return
		}
	}
	dash, pass, err := svcs.AuthService.CreateUserDash(userId, locationId)
	if err != nil {
		panic(err)
	}
	fmt.Printf("The User Dashboard was created.  Make note of the following:\n")
	fmt.Printf("\tAPI Key ID: %s\n", dash.KeyId)
	fmt.Printf("\tAPI Password: %s\n", pass)
}
