package dashcmd

import "github.com/spf13/cobra"

var DashCmd = &cobra.Command{
	Use:   "dash",
	Short: "Manages user dashboards",
	Long:  `Manages user dashboards`,
}

func init() {
	initDashAddCmd()
	initDashRotateCmd()
	DashCmd.AddCommand(dashAddCmd, dashRotateCmd)
}
