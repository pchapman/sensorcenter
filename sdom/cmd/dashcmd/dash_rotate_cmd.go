package dashcmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
)

var dashRotateCmd = &cobra.Command{
	Use:   "rotate [-d dash_key_id]",
	Short: "Rotate API Key",
	Long:  `Rotates the password for a dashboard's API Key`,
	Run:   runDashRotateCmd,
}

var (
	dashKeyId string
)

func initDashRotateCmd() {
	dashRotateCmd.Flags().StringVarP(&dashKeyId, "dash_key_id", "d", "", "Dashboard API Key ID")
}

func runDashRotateCmd(cmd *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	if dashKeyId == "" {
		dashKeyId = cmdutils.QueryStringField("dashboard api key id")
		if dashKeyId == "" {
			return
		}
	}
	dash, err := repo.RetrieveUserDashByKeyId(dashKeyId)
	if err != nil {
		panic(err)
	}
	if dash != nil {
		fmt.Printf("The dashboard API key %s does not exist\n", dashKeyId)
		return
	}
	passwd, err := svcs.AuthService.RotateUserDashPassword(dash.Id)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Make note of the new password: %s\n", passwd)
}
