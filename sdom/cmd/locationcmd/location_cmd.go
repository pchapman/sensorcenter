package locationcmd

import "github.com/spf13/cobra"

var LocationCmd = &cobra.Command{
	Use:   "location",
	Short: "Manages locations",
	Long:  `Manages locations`,
}

func init() {
	LocationCmd.AddCommand(locationAddCmd)
}
