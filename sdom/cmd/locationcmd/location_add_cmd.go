package locationcmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

var locationAddCmd = &cobra.Command{
	Use:   "add [user_id]",
	Short: "Adds locations for a user",
	Long:  `Adds locations for a user`,
	Run:   runLocationAddCmd,
}

func runLocationAddCmd(_ *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}

	userId := ""
	if len(args) > 0 {
		userId = args[0]
	}

	if userId == "" {
		userId = cmdutils.QueryStringField("User Id")
		if userId == "" {
			return
		}
	}

	for true {
		locationId, ok := createNewLocation(repo)
		if ok {
			err = repo.InsertUserLocation(userId, locationId)
			if err != nil {
				fmt.Println(err)
			}
			fmt.Printf("Inserted location %d for user %s\n", locationId, userId)
		}

		cont, err := cmdutils.QueryForYesNo("Continue?")
		if err != nil {
			panic(err)
		}
		if !cont {
			return
		}
	}
}

func createNewLocation(repo repository.Repository) (locationId int64, ok bool) {
	loc := &scom.Location{}

	for loc.Name == "" {
		loc.Name = cmdutils.QueryStringField("Name")
		exists, err := repo.LocationNameExists(loc.Name)
		if err != nil {
			panic(err)
		}
		if exists {
			cont, err := cmdutils.QueryForYesNo("Location already exists.  Continue?")
			if err != nil {
				panic(err)
			}
			if !cont {
				return 0, false
			}
			loc.Name = ""
		}
	}

	loc.Description = cmdutils.QueryStringPField("Description")
	loc.AddressLine1 = cmdutils.QueryStringPField("AddressLine1")
	loc.AddressLine2 = cmdutils.QueryStringPField("AddressLine2")
	loc.AddressCity = cmdutils.QueryStringPField("AddressCity")
	loc.AddressRegion = cmdutils.QueryStringPField("AddressRegion")
	loc.AddressPostalCode = cmdutils.QueryStringPField("AddressPostalCode")
	loc.AddressCountry = cmdutils.QueryStringPField("AddressCountry")
	loc.Longitude = cmdutils.QueryFloat32PField("Longitude")
	loc.Latitude = cmdutils.QueryFloat32PField("Latitude")

	var err error
	loc.Id, err = repo.InsertLocation(loc)
	if err != nil {
		fmt.Printf("Unable to save the new location: %s\n", err)
		return 0, false
	}
	fmt.Printf("Inserted location %d\n", loc.Id)

	return loc.Id, true
}
