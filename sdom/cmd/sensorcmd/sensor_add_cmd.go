package sensorcmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/go-utils/misc"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/cmdutils"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd/reportercmd"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_switch"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_thermal"
	"log"
	"os"
	"strconv"
	"strings"
)

var sensorAddCmd = &cobra.Command{
	Use:   "add [-l location_id] [-r reporter_id] [-n name] [-t type] [-s schedule] [-d configData]",
	Short: "Adds sensors",
	Long:  `Adds sensors for a user`,
	Run:   runSensorAddCmd,
}

var (
	configData string
	locationId int64
	reporterId int64
	name       string
	sensorType string
	schedule   string
)

func initSensorAddCmd() {
	sensorAddCmd.Flags().StringVarP(&configData, "config_data", "d", "", "config data file")
	sensorAddCmd.Flags().Int64VarP(&locationId, "location_id", "l", 0, "location id")
	sensorAddCmd.Flags().Int64VarP(&reporterId, "reporter_id", "r", 0, "reporter id")
	sensorAddCmd.Flags().StringVarP(&name, "name", "n", "", "name")
	sensorAddCmd.Flags().StringVarP(&sensorType, "type", "t", "", "type")
	sensorAddCmd.Flags().StringVarP(&schedule, "schedule", "s", "", "schedule")
}

func runSensorAddCmd(_ *cobra.Command, _ []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	if locationId == 0 {
		locationId = cmdutils.QueryForLocationId(repo, nil)
		if locationId == 0 {
			fmt.Printf("No location selected\n")
			return
		}
	}

	if reporterId == 0 {
		reporterId = queryForReporterId(repo, svcs.AuthService, locationId)
		if reporterId == 0 {
			return
		}
	}

	sensor := scom.Sensor{
		LocationId: locationId,
		ReporterId: reporterId,
	}

	if name != "" {
		sensor.Name = name
	} else {
		sensor.Name = cmdutils.QueryStringField("Name")
	}
	if sensor.Name == "" {
		fmt.Printf("No sensor name provided\n")
		return
	}

	for {
		exists, err := repo.SensorNameExists(sensor.Name)
		if err != nil {
			panic(err)
		}
		if !exists {
			break
		}
		cont, err := cmdutils.QueryForYesNo(fmt.Sprintf("Sensor %s already exists.  Continue with another name?", sensor.Name))
		if err != nil {
			panic(err)
		}
		if !cont {
			return
		}
		sensor.Name = cmdutils.QueryStringField("Sensor Name")
	}

	if sensorType != "" {
		if scom.ValidateSensorType(sensorType) {
			sensor.Type = scom.SensorType(sensorType)
		} else {
			fmt.Printf("sensor type %s is not valid.", sensorType)
		}
	}

	if sensor.Type == "" {
		sensor.Type = querySensorType()
		if sensor.Type == "" {
			fmt.Printf("No sensor type provided\n")
			return
		}
	}

	if schedule == "" {
		schedule = cmdutils.QueryStringField("Schedule")
	}
	for {
		valid := scom.ValidateSchedule(schedule)
		if valid {
			break
		}
		fmt.Printf("Schedule %s is not valid.", schedule)
		schedule = cmdutils.QueryStringField("Schedule")
	}
	sensor.Schedule = schedule

	sensor.StoreUnit = cmdutils.QueryStringField("Store Unit")
	if sensor.StoreUnit == "" {
		fmt.Printf("No store unit provided\n")
		return
	}

	sensor.DisplayConversion = queryDisplayConversion()
	if sensor.DisplayConversion == "" {
		sensor.DisplayUnit = sensor.StoreUnit
	} else {
		sensor.DisplayUnit = cmdutils.QueryStringField("Display Unit")
	}

	for true {
		dsd := cmdutils.QueryStringField("Display Sig Digits (empty string to apply no rounding)")
		if dsd == "" {
			break
		} else {
			i, err := strconv.ParseInt(dsd, 10, 64)
			if err != nil {
				log.Fatalf("invalid display sig digits %s: %s", dsd, err)
				continue
			}
			sensor.DisplaySigDigits = misc.PInt32(int32(i))
			break
		}
	}

	var data []byte
	switch sensor.Type {
	case scom.ST_Switch:
		if len(configData) > 0 {
			data, err = os.ReadFile(configData)
			if err != nil {
				panic(fmt.Errorf("unable to read config data file: %s", err))
			}
			_, err = sc_switch.NewSensorConfig(string(data))
			if err != nil {
				panic(fmt.Errorf("unable to parse config data: %s", err))
			}
			sensor.ConfigData = string(data)
		} else {
			panic(fmt.Errorf("no config data provided, but required for switch sensors"))
		}
	case scom.ST_Temperature:
		if len(configData) > 0 {
			data, err = os.ReadFile(configData)
			if err != nil {
				panic(fmt.Errorf("unable to read config data file: %s", err))
			}
			_, err = sc_thermal.NewSensorConfig(string(data))
			if err != nil {
				panic(fmt.Errorf("unable to parse config data: %s", err))
			}
			sensor.ConfigData = string(data)
		} else {
			panic(fmt.Errorf("no config data provided, but required for thermal sensors"))
		}
	}

	sensor.Id, err = repo.InsertSensor(&sensor)
	if err != nil {
		panic(err)
	}

	fmt.Printf("sensor %s (%d) created\n", sensor.Name, sensor.Id)
}

func queryDisplayConversion() string {
	expr := cmdutils.QueryStringField("Display Conversion Expression, The variable name \"value\" should be used to represent the reported sensor value.\n")
	if expr == "" {
		return ""
	}
	ok, reason := scom.ValidateDisplayConversion(expr)
	if ok {
		return expr
	}
	fmt.Printf("Invalid display conversion expression: %s\tReason: %s\n", expr, reason)
	return queryDisplayConversion()
}

func querySensorType() scom.SensorType {
	stypes := scom.SensorTypes()
	for i, st := range stypes {
		fmt.Printf("%d: %s\n", i+1, st)
	}
	fmt.Printf("Select a sensor type by number or (q)uit: ")
	input := ""
	_, err := fmt.Scanln(&input)
	if err != nil {
		panic(err)
	}
	if strings.ToLower(input) == "q" {
		return ""
	}
	i, err := strconv.Atoi(input)
	if err != nil {
		panic(err)
	}
	if i < 0 || i > len(stypes) {
		fmt.Println("Invalid input")
		return querySensorType()
	}
	return stypes[i-1]
}

func queryForReporterId(repo repository.Repository, authService services.AuthService, locationId int64) int64 {
	reporters, err := repo.RetrieveReportersForLocation(locationId)
	if err != nil {
		log.Fatalf("unable to retrieve reporters: %s", err)
	}
	if len(reporters) != 0 {
		for idx, rep := range reporters {
			fmt.Printf("%d: %s\n", idx+1, rep.Name)
		}
		fmt.Printf("Select a reporter by number, (c)reate or (q)uit: ")
		input := ""
		_, err = fmt.Scanln(&input)
		if err != nil {
			panic(err)
		}
		if strings.ToLower(input) == "q" {
			return 0
		}
		if strings.ToLower(input) == "c" {
			return reportercmd.CreateReporter(repo, authService, locationId, nil)
		}
		i, err := strconv.Atoi(input)
		if err != nil {
			panic(err)
		}
		if i < 0 || i > len(reporters) {
			fmt.Println("Invalid input")
			return queryForReporterId(repo, authService, locationId)
		}
		return reporters[i-1].Id
	}

	fmt.Println("No reporters found")
	return reportercmd.CreateReporter(repo, authService, locationId, nil)
}
