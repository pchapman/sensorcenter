package sensorcmd

import "github.com/spf13/cobra"

var SensorCmd = &cobra.Command{
	Use:   "sensor",
	Short: "Manages sensors",
	Long:  `Manages sensors`,
}

func init() {
	initSensorAddCmd()
	SensorCmd.AddCommand(sensorAddCmd)
}
