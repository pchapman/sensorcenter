package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/httpsvc"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"os"
	"os/signal"
	"syscall"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Runs the sdom webapi server",
	Long:  `Runs the sdom webapi server.`,
	Run:   runServeCmd,
}

func runServeCmd(cmd *cobra.Command, args []string) {
	// Obtain and verify configuration
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}

	// Initialize the database connection
	err = repo.InitializeDb()
	if err != nil {
		panic(err)
	}

	// Create the services
	svcs, err := services.NewSdomServices(repo)
	if err != nil {
		panic(err)
	}

	// Create and start the app
	app, err := httpsvc.NewApp(svcs)
	if err != nil {
		panic(err)
	}
	app.Start()
	if err != nil {
		panic(err)
	}

	// Create a channel to receive OS signals
	signalChan := make(chan os.Signal, 1)

	// Signal to wait for `Ctrl+C`
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	// Wait for the signal
	fmt.Printf("Press Ctrl+C to stop\n")
	sig := <-signalChan
	fmt.Println("Received signal:", sig)
	_ = app.Stop()
	_ = repo.Close()

	// Exit the program
	os.Exit(0)
}
