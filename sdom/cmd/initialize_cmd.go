package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/pchapman/sensorcenter/sdom/repository"
)

var initializeCmd = &cobra.Command{
	Use:   "initialize",
	Short: "Initializes the database",
	Long:  `Initializes an empty database and applies all updates to an existing one.`,
	Run:   runInitializeCmd,
}

func runInitializeCmd(cmd *cobra.Command, args []string) {
	repo, err := repository.NewRepository()
	if err != nil {
		panic(err)
	}

	err = repo.InitializeDb()
	if err != nil {
		panic(err)
	}
}
