package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"gitlab.com/pchapman/sensorcenter/sdom/view"
	"net/http"
)

type SensorType struct {
	ConfigurationHelp    string
	DefaultDisplayedUnit string
	DefaultStoredUnit    string
	Name                 string
	Value                string
}

type SensorPageData struct {
	Create    bool
	Errors    []string
	Location  *scom.Location
	Reporters []scom.Reporter
	Sensor    *scom.Sensor
	User      *services.User
	Types     []SensorType
}

const (
	sensorURI = "/location/:" + scom.ParamLocationId + "/sensor/:" + scom.ParamSensorId
	//validateConfigDataURI     = "/sensor/validate/configData"
	//validateScheduleURI       = "/sensor/validate/schedule"
	//validateUnitConversionURI = "/sensor/validate/unitConversion"
)

type sensorPagesHandler struct {
	defaultSensorType SensorType
	sensorTypes       []SensorType
	svcs              *services.SdomServices
	templates         *view.Templates
}

func newSensorPagesHandler(svcs *services.SdomServices, templates *view.Templates) *sensorPagesHandler {
	sph := &sensorPagesHandler{
		svcs:      svcs,
		templates: templates,
		sensorTypes: []SensorType{
			{
				Value:                scom.ST_Angle.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "°",
				DefaultStoredUnit:    "°",
				Name:                 scom.ST_Angle.ProperCase(),
			},
			{
				Value:                scom.ST_Current.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "A", // amperes
				DefaultStoredUnit:    "A",
				Name:                 scom.ST_Current.ProperCase(),
			},
			{
				Value:                scom.ST_Humidity.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "g/kg",
				DefaultStoredUnit:    "g/kg",
				Name:                 scom.ST_Humidity.ProperCase(),
			},
			{
				Value:                scom.ST_Position.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "mm",
				DefaultStoredUnit:    "mm",
				Name:                 scom.ST_Position.ProperCase(),
			},
			{
				Value:                scom.ST_Pressure.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "Pa",
				DefaultStoredUnit:    "Pa",
				Name:                 scom.ST_Pressure.ProperCase(),
			},
			{
				Value:                scom.ST_Resistance.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "Ω",
				DefaultStoredUnit:    "Ω",
				Name:                 scom.ST_Resistance.ProperCase(),
			},
			{
				Value:                scom.ST_RPM.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "rpm",
				DefaultStoredUnit:    "rpm",
				Name:                 scom.ST_RPM.ProperCase(),
			},
			{
				Value:                scom.ST_Speed.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "m/s",
				DefaultStoredUnit:    "m/s",
				Name:                 scom.ST_Speed.ProperCase(),
			},
			{
				Value:                scom.ST_Switch.String(),
				ConfigurationHelp:    "https://gitlab.com/pchapman/sensorcenter/-/blob/master/ssub/sc_switch/README.md?ref_type=heads",
				DefaultDisplayedUnit: "on/off",
				DefaultStoredUnit:    "on/off",
				Name:                 scom.ST_Switch.ProperCase(),
			},
			{
				Value:                scom.ST_Temperature.String(),
				ConfigurationHelp:    "https://gitlab.com/pchapman/sensorcenter/-/blob/master/ssub/sc_thermal/docs/Raspberry-Pi-DS18B20.md?ref_type=heads",
				DefaultDisplayedUnit: "C",
				DefaultStoredUnit:    "C",
				Name:                 scom.ST_Temperature.ProperCase(),
			},
			{
				Value:                scom.ST_Voltage.String(),
				ConfigurationHelp:    "",
				DefaultDisplayedUnit: "V",
				DefaultStoredUnit:    "V",
				Name:                 scom.ST_Voltage.ProperCase(),
			},
		},
	}
	sph.defaultSensorType = sph.sensorTypes[9]
	return sph
}

func (sph *sensorPagesHandler) registerRoutes(e *echo.Echo) {
	e.DELETE(sensorURI, sph.deleteSensorHandler)
	e.GET(sensorURI, sph.getSensorHandler)
	e.POST(sensorURI, sph.postSensorHandler)
}

func (sph *sensorPagesHandler) deleteSensorHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		sensorId, ok, err := pathParamInt(c, scom.ParamSensorId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		if !cc.canWrite(&locId, nil) {
			return echo.ErrForbidden
		}
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		sensor, err := sph.svcs.SensorService.RetrieveSensorById(sensorId)
		if err != nil {
			return err
		}
		if sensor != nil {
			if sensor.LocationId != locId {
				return echo.ErrBadRequest
			}

			err = sph.svcs.SensorService.DeleteSensor(sensorId)
			if err != nil {
				return err
			}
		}
		if cc.Request().Header.Get("HX-Request") == "true" {
			return c.NoContent(http.StatusOK)
		} else {
			return c.Redirect(http.StatusSeeOther, locationsURI)
		}
	} else {
		return c.Redirect(http.StatusSeeOther, homeURI)
	}
}

func (sph *sensorPagesHandler) getSensorHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		var id *int64 = nil
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		sensorId, ok, err := pathParamInt(c, scom.ParamSensorId)
		if err != nil {
			return err
		}
		if ok {
			id = &sensorId
		}

		data, err := sph.sensorData(cc, locId, id, nil, nil)
		if err != nil {
			return err
		}
		if locId != data.Sensor.LocationId {
			return echo.ErrBadRequest
		}

		return sph.templates.Render(c, "sensor.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
}

func (sph *sensorPagesHandler) postSensorHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		sensor := &scom.Sensor{}
		err := c.Bind(sensor)
		if err != nil {
			return err
		}
		if sensor.ConfigData == "" {
			sensor.ConfigData = "{}"
		}

		validationErrors, err := sph.svcs.SensorService.ValidateSensor(sensor)
		if err != nil {
			return err
		}
		if len(validationErrors) > 0 {
			data, err := sph.sensorData(cc, sensor.LocationId, &sensor.Id, sensor, validationErrors)
			if err != nil {
				return err
			}
			return sph.templates.Render(c, "sensor.html", data)
		}
		if sensor.Id == 0 {
			if !cc.canWrite(&sensor.LocationId, nil) {
				return echo.ErrForbidden
			}
			var id int64
			id, err = sph.svcs.SensorService.CreateSensor(sensor)
			if err != nil {
				return err
			}
			sensor.Id = id
		} else {
			if !cc.canWrite(&sensor.LocationId, &sensor.Id) {
				return echo.ErrForbidden
			}
			err = sph.svcs.SensorService.UpdateSensor(sensor)
		}
		if err != nil {
			return err
		}
		data, err := sph.sensorData(cc, sensor.LocationId, &sensor.Id, sensor, nil)
		if err != nil {
			return err
		}
		return sph.templates.Render(c, "sensor.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
}

func (sph *sensorPagesHandler) sensorData(cc *SessionContext, locationId int64, sensorId *int64, sensor *scom.Sensor, validationErrors []string) (*SensorPageData, error) {
	var err error
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if validationErrors == nil {
		validationErrors = make([]string, 0)
	}

	if locationId == 0 && sensor != nil {
		locationId = sensor.LocationId
	}
	if locationId == 0 {
		return nil, echo.ErrBadRequest
	}

	// general data
	data := &SensorPageData{
		Create: sensorId == nil || *sensorId == 0,
		Errors: validationErrors,
		Types:  sph.sensorTypes,
		User:   cc.user,
	}

	// location
	data.Location, err = sph.svcs.LocationService.RetrieveLocationById(locationId)
	if err != nil {
		return data, err
	}

	// reporters
	data.Reporters, err = sph.svcs.ReporterService.RetrieveReportersByLocation(locationId)
	if err != nil {
		return data, err
	}

	if data.Create {
		if sensor == nil {
			// Build a new sensor with sensible default values
			sensor = &scom.Sensor{
				ConfigData:  "{}",
				DisplayUnit: sph.defaultSensorType.DefaultDisplayedUnit,
				LocationId:  locationId,
				Type:        scom.SensorType(sph.defaultSensorType.Value),
				StoreUnit:   sph.defaultSensorType.DefaultStoredUnit,
			}
			if len(data.Reporters) == 1 {
				sensor.ReporterId = data.Reporters[0].Id
			}
		}
	} else if sensor == nil {
		if !cc.canWrite(nil, sensorId) {
			return data, echo.ErrForbidden
		}
		sensor, err = sph.svcs.SensorService.RetrieveSensorById(*sensorId)
		if err != nil {
			if services.IsNotFoundError(err) {
				return data, echo.ErrNotFound
			}
			return data, err
		}
	}
	data.Sensor = sensor

	return data, nil
}
