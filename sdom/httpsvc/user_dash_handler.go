package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"net/http"
	"strconv"
)

type userDashApiHandler struct {
	userDashSvc services.UserDashService
}

func newUserDashApiHandler(svcs *services.SdomServices) *userDashApiHandler {
	return &userDashApiHandler{userDashSvc: svcs.UserDashService}
}

func (h *userDashApiHandler) registerRoutes(e *echo.Echo) {
	e.GET(scom.PathGetUserDash, h.retrieveUserDash)
}

func (h *userDashApiHandler) retrieveUserDash(c echo.Context) (err error) {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}

	dashIdParam := c.Param(scom.ParamUserDashId)
	if dashIdParam == "" {
		return echo.ErrBadRequest
	}
	var dashId int64
	if dashIdParam == "me" {
		if cc.userDashId == 0 {
			return echo.ErrBadRequest
		}
		dashId = cc.userDashId
	} else {
		dashId, err = strconv.ParseInt(dashIdParam, 10, 64)
		if err != nil {
			return echo.ErrBadRequest
		}
	}

	dash, err := h.userDashSvc.RetrieveUserDashById(dashId)
	if err != nil {
		if services.IsNotFound(err) {
			return echo.ErrNotFound
		}
		return err
	}
	if (cc.userDashId > 0 && cc.userDashId == dash.Id) || (cc.user.Id != "" && cc.user.Id == dash.UserId) {
		return c.JSON(http.StatusOK, dash)
	}

	return echo.ErrForbidden
}
