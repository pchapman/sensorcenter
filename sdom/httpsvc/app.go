package httpsvc

import (
	"context"
	"fmt"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/pchapman/go-utils/misc"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"gitlab.com/pchapman/sensorcenter/sdom/view"
	"log"
	"net/http"
	"os"
)

type App struct {
	listenAddr string
	port       int32
	echo       *echo.Echo
}

func NewApp(svcs *services.SdomServices) (*App, error) {
	sessionsSecret := os.Getenv(config.EnvSessionsSecret)
	if sessionsSecret == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvSessionsSecret)
	}

	listenAddr := os.Getenv(config.EnvListenAddress)
	if listenAddr == "" {
		listenAddr = "0.0.0.0"
	}
	port, ok := misc.Int32Env(config.EnvListenPort)
	if !ok {
		port = 8080
	}

	e := echo.New()
	tmpls, err := view.NewTemplates()
	if err != nil {
		e.Logger.Fatalf("failed to create templates: %s", err)
	}

	// Set up Middlewares
	eh := newErrorHandler(tmpls)
	e.HTTPErrorHandler = eh.ErrorHandler
	e.Use(middleware.Logger())
	e.Use(session.Middleware(sessions.NewCookieStore([]byte(sessionsSecret))))

	// Static assets
	assetHandler := http.FileServerFS(view.Static())
	e.GET("/static/*", echo.WrapHandler(assetHandler))

	// API Routes
	ah, err := newAuthHandler(svcs)
	if err != nil {
		return nil, err
	}
	e.Use(ah.authMiddleware)
	dh := newUserDashApiHandler(svcs)
	dh.registerRoutes(e)
	lh := newLocationApiHandler(svcs)
	lh.registerRoutes(e)
	rh := newReporterApiHandler(svcs)
	rh.registerRoutes(e)
	sh := newSensorApiHandler(svcs)
	sh.registerRoutes(e)

	// Page Routes
	ah.registerRoutes(e)
	ph := newHomePageHandler(svcs, tmpls)
	ph.registerRoutes(e)
	lph := newLocationPagesHandler(svcs, tmpls)
	lph.registerRoutes(e)
	sph := newSensorPagesHandler(svcs, tmpls)
	sph.registerRoutes(e)

	return &App{
		listenAddr: listenAddr,
		port:       port,
		echo:       e,
	}, nil
}

func (app *App) Start() {
	go func() {
		err := app.echo.Start(fmt.Sprintf("%s:%d", app.listenAddr, app.port))
		if err != nil {
			log.Printf("Unable to start HTTP server: %s", err)
		}
	}()
}

func (app *App) Stop() error {
	return app.echo.Shutdown(context.Background())
}
