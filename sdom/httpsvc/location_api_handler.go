package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"net/http"
)

type locationApiHandler struct {
	locationSvc services.LocationService
	sensorSvc   services.SensorService
}

func newLocationApiHandler(svcs *services.SdomServices) *locationApiHandler {
	return &locationApiHandler{locationSvc: svcs.LocationService, sensorSvc: svcs.SensorService}
}

func (h *locationApiHandler) registerRoutes(e *echo.Echo) {
	e.GET(scom.PathGetLocationReadings, h.retrieveLocationReadings)
	e.GET(scom.PathGetAllLocations, h.retrieveAllLocations)
	e.GET(scom.PathGetLocation, h.retrieveLocation)
}

func (h *locationApiHandler) retrieveAllLocations(c echo.Context) error {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}
	locations, err := h.locationSvc.RetrieveAllLocations()
	if err != nil {
		return err
	}
	locations = cc.limitLocations(locations)
	return c.JSON(http.StatusOK, locations)
}

func (h *locationApiHandler) retrieveLocationReadings(c echo.Context) error {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}

	locationId, ok, err := pathParamInt(c, scom.ParamLocationId)
	if !ok || err != nil {
		return echo.ErrBadRequest
	}
	if !cc.canRead(&locationId, nil) {
		return echo.ErrForbidden
	}

	readings, err := h.sensorSvc.RetrieveLatestSensorReadingsForLocation(locationId)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, readings)
}

func (h *locationApiHandler) retrieveLocation(c echo.Context) error {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}

	locationId, ok, err := pathParamInt(c, scom.ParamLocationId)
	if !ok || err != nil {
		return echo.ErrBadRequest
	}
	if !cc.canRead(&locationId, nil) {
		return echo.ErrForbidden
	}

	location, err := h.locationSvc.RetrieveLocationById(locationId)
	if err != nil {
		if services.IsNotFound(err) {
			return echo.ErrNotFound
		}
		return err
	}
	return c.JSON(http.StatusOK, location)
}
