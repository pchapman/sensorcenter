package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"gitlab.com/pchapman/sensorcenter/sdom/view"
	"net/http"
)

const (
	locationDashCreateURI     = "/location/:" + scom.ParamLocationId + "/dash"
	locationDashDeleteURI     = "/location/:" + scom.ParamLocationId + "/dash/:" + scom.ParamUserDashId
	locationDashListURI       = "/location/:" + scom.ParamLocationId + "/dash"
	locationDashResetURI      = "/location/:" + scom.ParamLocationId + "/dash/:" + scom.ParamUserDashId + "/reset"
	locationReporterCreateURI = "/location/:" + scom.ParamLocationId + "/reporter"
	locationReporterDeleteURI = "/location/:" + scom.ParamLocationId + "/reporter/:" + scom.ParamReporterId
	locationReporterListURI   = "/location/:" + scom.ParamLocationId + "/reporter"
	locationReporterResetURI  = "/location/:" + scom.ParamLocationId + "/reporter/:" + scom.ParamReporterId + "/reset"
	locationURI               = "/location/:" + scom.ParamLocationId
	locationsURI              = "/locations"
)

type LocationPageData struct {
	Create         bool
	Errors         []string
	Location       *scom.Location
	Reporters      []scom.Reporter
	Sensors        []scom.Sensor
	User           *services.User
	UserDashboards []scom.UserDash
}

type locationPagesHandler struct {
	svcs      *services.SdomServices
	templates *view.Templates
}

func newLocationPagesHandler(svcs *services.SdomServices, templates *view.Templates) *locationPagesHandler {
	return &locationPagesHandler{
		svcs:      svcs,
		templates: templates,
	}
}

func (lph *locationPagesHandler) registerRoutes(e *echo.Echo) {
	e.DELETE(locationURI, lph.deleteLocationHandler)
	e.DELETE(locationDashDeleteURI, lph.deleteLocationDashHandler)
	e.DELETE(locationReporterDeleteURI, lph.deleteReporterHandler)
	e.GET(locationDashListURI, lph.locationDashListHandler)
	e.GET(locationReporterListURI, lph.locationReporterListHandler)
	e.GET(locationURI, lph.getLocationHandler)
	e.GET(locationsURI, lph.locationsHandler)
	e.POST(locationURI, lph.postLocationHandler)
	e.POST(locationDashResetURI, lph.resetLocationDashHandler)
	e.PUT(locationDashCreateURI, lph.putLocationDashHandler)
	e.POST(locationReporterResetURI, lph.resetReporterHandler)
	e.PUT(locationReporterCreateURI, lph.putReporterHandler)
}

func (lph *locationPagesHandler) locationsHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		data := map[string]interface{}{
			"user": cc.user,
		}
		locations, err := lph.svcs.LocationService.RetrieveAllLocations()
		if err != nil {
			return err
		}
		locations = cc.limitLocations(locations)
		data["locations"] = locations
		return lph.templates.Render(c, "locations.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
}

func (lph *locationPagesHandler) locationData(cc *SessionContext, locId *int64, loc *scom.Location, validationErrors []string) (*LocationPageData, error) {
	var err error
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if validationErrors == nil {
		validationErrors = make([]string, 0)
	}

	// actions
	data := &LocationPageData{
		Create: locId == nil || *locId == 0,
		Errors: validationErrors,
		User:   cc.user,
	}

	// location data
	if locId != nil && *locId > 0 && loc == nil {
		if !cc.canWrite(locId, nil) {
			return data, echo.ErrForbidden
		}
		loc, err = lph.svcs.LocationService.RetrieveLocationById(*locId)
		if err != nil {
			if services.IsNotFoundError(err) {
				return data, echo.ErrNotFound
			}
			return data, err
		}
	}
	if loc != nil {
		data.Location = loc
	}

	// user dashboards
	data.UserDashboards, err = lph.svcs.UserDashService.RetrieveUserDashesByUserAndLocation(cc.user.Id, *locId)
	if err != nil {
		return data, err
	}

	// reporters
	data.Reporters, err = lph.svcs.ReporterService.RetrieveReportersByLocation(*locId)
	if err != nil {
		return data, err
	}

	// sensors
	data.Sensors, err = lph.svcs.SensorService.RetrieveSensorsByLocation(*locId)
	if err != nil {
		return data, err
	}

	return data, nil
}

func (lph *locationPagesHandler) loadReporterForRead(cc *SessionContext, rId *int64) (*scom.Reporter, error) {
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if rId == nil || *rId == 0 {
		return &scom.Reporter{}, nil
	}
	var err error
	r, err := lph.svcs.ReporterService.RetrieveReporterById(*rId)
	if err != nil {
		return nil, err
	}
	if r == nil {
		return nil, echo.ErrNotFound
	}
	if cc.canRead(&r.LocationId, nil) {
		return r, nil
	}
	return r, echo.ErrForbidden
}

func (lph *locationPagesHandler) loadReporterForWrite(cc *SessionContext, rId *int64, r *scom.Reporter) (*scom.Reporter, error) {
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if r == nil {
		if rId == nil || *rId == 0 {
			return &scom.Reporter{}, nil
		}
		var err error
		r, err = lph.svcs.ReporterService.RetrieveReporterById(*rId)
		if err != nil {
			return nil, err
		}
		if r == nil {
			return nil, echo.ErrNotFound
		}
	} else if r.Id == 0 {
		return r, nil
	}
	if cc.canWrite(&r.LocationId, nil) {
		return r, nil
	}
	return r, echo.ErrForbidden
}

func (lph *locationPagesHandler) loadUserDashForRead(cc *SessionContext, dashId *int64) (*scom.UserDash, error) {
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if dashId == nil || *dashId == 0 {
		return &scom.UserDash{}, nil
	}
	var err error
	dash, err := lph.svcs.UserDashService.RetrieveUserDashById(*dashId)
	if err != nil {
		return nil, err
	}
	if dash == nil {
		return nil, echo.ErrNotFound
	}
	if cc.canRead(&dash.LocationId, nil) {
		return dash, nil
	}
	return dash, echo.ErrForbidden
}

func (lph *locationPagesHandler) loadUserDashForWrite(cc *SessionContext, dashId *int64, dash *scom.UserDash) (*scom.UserDash, error) {
	if cc == nil || !cc.authenticated() {
		return nil, echo.ErrUnauthorized
	}
	if dash == nil {
		if dashId == nil || *dashId == 0 {
			return &scom.UserDash{}, nil
		}
		var err error
		dash, err = lph.svcs.UserDashService.RetrieveUserDashById(*dashId)
		if err != nil {
			return nil, err
		}
		if dash == nil {
			return nil, echo.ErrNotFound
		}
	} else if dash.Id == 0 {
		return dash, nil
	}
	if cc.canWrite(&dash.LocationId, nil) {
		return dash, nil
	}
	return dash, echo.ErrForbidden
}

func (lph *locationPagesHandler) locationDashListHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}
		dashboards, err := lph.svcs.UserDashService.RetrieveUserDashesByUserAndLocation(cc.user.Id, locId)
		if err != nil {
			return err
		}
		// The full page has a reference to the related location, but the fragment only needs the ID.
		// Rather than load the location, we simply use a map with the Id key referencing location ID.
		data := map[string]interface{}{
			"Location": map[string]interface{}{
				"Id": locId,
			},
			"UserDashboards": dashboards,
		}
		return lph.templates.Render(c, "user_dashboards_table_frag.html", data)
	} else {
		return echo.ErrUnauthorized
	}
}

func (lph *locationPagesHandler) locationReporterListHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}
		reporters, err := lph.svcs.ReporterService.RetrieveReportersByLocation(locId)
		if err != nil {
			return err
		}
		// The full page has a reference to the related location, but the fragment only needs the ID.
		// Rather than load the location, we simply use a map with the Id key referencing location ID.
		data := map[string]interface{}{
			"Location": map[string]interface{}{
				"Id": locId,
			},
			"Reporters": reporters,
		}
		return lph.templates.Render(c, "reporters_table_frag.html", data)
	} else {
		return echo.ErrUnauthorized
	}
}

func (lph *locationPagesHandler) deleteLocationDashHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		dashId, ok, err := pathParamInt(c, scom.ParamUserDashId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		dash, err := lph.loadUserDashForWrite(cc, &dashId, nil)
		if err != nil {
			return err
		}
		if dash.LocationId != locId {
			return echo.ErrBadRequest
		}

		err = lph.svcs.UserDashService.DeleteUserDash(dashId)
		if err != nil {
			return err
		}
		if cc.Request().Header.Get("HX-Request") == "true" {
			return c.NoContent(http.StatusOK)
		} else {
			return c.Redirect(http.StatusSeeOther, locationsURI)
		}
	} else {
		return c.Redirect(http.StatusSeeOther, homeURI)
	}
}

func (lph *locationPagesHandler) deleteLocationHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		if !cc.canWrite(&locId, nil) {
			return echo.ErrForbidden
		}

		err = lph.svcs.LocationService.DeleteLocation(locId)
		if err != nil {
			return err
		}
		if cc.Request().Header.Get("HX-Request") == "true" {
			return c.NoContent(http.StatusOK)
		} else {
			return c.Redirect(http.StatusSeeOther, locationsURI)
		}
	} else {
		return c.Redirect(http.StatusSeeOther, homeURI)
	}
}

func (lph *locationPagesHandler) deleteReporterHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		rId, ok, err := pathParamInt(c, scom.ParamReporterId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		r, err := lph.loadReporterForWrite(cc, &rId, nil)
		if err != nil {
			return err
		}
		if r.LocationId != locId {
			return echo.ErrBadRequest
		}

		err = lph.svcs.ReporterService.DeleteReporter(rId)
		if err != nil {
			return err
		}
		if cc.Request().Header.Get("HX-Request") == "true" {
			return c.NoContent(http.StatusOK)
		} else {
			return c.Redirect(http.StatusSeeOther, locationsURI)
		}
	} else {
		return c.Redirect(http.StatusSeeOther, homeURI)
	}
}

func (lph *locationPagesHandler) getLocationHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		var id *int64 = nil
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if ok {
			id = &locId
		}
		data, err := lph.locationData(cc, id, nil, nil)
		if err != nil {
			return err
		}
		return lph.templates.Render(c, "location.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
}

func (lph *locationPagesHandler) postLocationHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		loc := &scom.Location{}
		err := c.Bind(loc)
		if err != nil {
			return err
		}
		validationErrors, err := lph.svcs.LocationService.ValidateLocation(loc)
		if err != nil {
			return err
		}
		if len(validationErrors) > 0 {
			data, err := lph.locationData(cc, &loc.Id, loc, validationErrors)
			if err != nil {
				return err
			}
			return lph.templates.Render(c, "location.html", data)
		}
		if loc.Id == 0 {
			var id int64
			id, err = lph.svcs.LocationService.CreateLocation(loc, cc.user.Id)
			if err != nil {
				return err
			}
			loc.Id = id
		} else {
			if !cc.canWrite(&loc.Id, nil) {
				return echo.ErrForbidden
			}
			err = lph.svcs.LocationService.UpdateLocation(loc)
		}
		if err != nil {
			return err
		}
		data, err := lph.locationData(cc, &loc.Id, loc, nil)
		if err != nil {
			return err
		}
		return lph.templates.Render(c, "location.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
}

func (lph *locationPagesHandler) putLocationDashHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		dash, secret, err := lph.svcs.AuthService.CreateUserDash(cc.user.Id, locId)
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"id":     dash.Id,
			"key":    dash.KeyId,
			"secret": secret,
		}

		return c.JSON(http.StatusOK, data)
	} else {
		return echo.ErrUnauthorized
	}
}

func (lph *locationPagesHandler) putReporterHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}
		name, ok, err := queryParamStr(c, scom.ParamName)
		if err != nil {
			return err
		}
		if !ok {
			name, ok, err = pathParamStr(c, scom.ParamName)
			if err != nil {
				return err
			}
		}
		if !ok {
			return echo.ErrBadRequest
		}

		r, secret, err := lph.svcs.AuthService.CreateReporter(locId, name)
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"id":     r.Id,
			"key":    r.KeyId,
			"secret": secret,
		}

		return c.JSON(http.StatusOK, data)
	} else {
		return echo.ErrUnauthorized
	}
}

func (lph *locationPagesHandler) resetLocationDashHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		dashId, ok, err := pathParamInt(c, scom.ParamUserDashId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		dash, err := lph.loadUserDashForWrite(cc, &dashId, nil)
		if err != nil {
			return err
		}
		if dash.LocationId != locId {
			return echo.ErrBadRequest
		}

		secret, err := lph.svcs.AuthService.RotateUserDashPassword(dashId)
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"id":     dash.Id,
			"key":    dash.KeyId,
			"secret": secret,
		}

		return c.JSON(http.StatusOK, data)
	} else {
		return echo.ErrUnauthorized
	}
}

func (lph *locationPagesHandler) resetReporterHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		locId, ok, err := pathParamInt(c, scom.ParamLocationId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		rId, ok, err := pathParamInt(c, scom.ParamReporterId)
		if err != nil {
			return err
		}
		if !ok {
			return echo.ErrBadRequest
		}

		r, err := lph.loadReporterForWrite(cc, &rId, nil)
		if err != nil {
			return err
		}
		if r.LocationId != locId {
			return echo.ErrBadRequest
		}

		secret, err := lph.svcs.AuthService.RotateReporterPassword(rId)
		if err != nil {
			return err
		}

		data := map[string]interface{}{
			"dashId": r.Id,
			"key":    r.KeyId,
			"secret": secret,
		}

		return c.JSON(http.StatusOK, data)
	} else {
		return echo.ErrUnauthorized
	}
}
