package httpsvc

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/nu7hatch/gouuid"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type ErrorResponse struct {
	ErrorId     string    `json:"errorId" yaml:"errorId"`
	Error       string    `json:"error" yaml:"error"`
	Timestamp   time.Time `json:"timestamp" yaml:"timestamp"`
	Environment string    `json:"environment" yaml:"environment"`
	Version     string    `json:"apiVersion" yaml:"apiVersion`
}

var appVersion string
var environment string

func SetAppVersion(version string) {
	appVersion = version
}

func reportError(w http.ResponseWriter, statusCode int, error string) {
	errResp := ErrorResponse{
		Environment: environment,
		Timestamp:   time.Now(),
		Version:     appVersion,
	}
	uuid, err := uuid.NewV4()
	if err != nil {
		log.Printf("Error getting UUID for error", err)
		errResp.ErrorId = fmt.Sprintf("%d", rand.Int31())
	} else {
		errResp.ErrorId = uuid.String()
	}
	errResp.Error = fmt.Sprintf("Error %s: %s", errResp.ErrorId, error)
	log.Print(errResp.Error)
	body, err := json.Marshal(errResp)
	w.WriteHeader(statusCode)
	_, err = w.Write(body)
	if err != nil {
		log.Printf("Unable to write error to response: %s", err)
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
	}
}

func pathParamInt(c echo.Context, paramName string) (value int64, exists bool, err error) {
	param := c.Param(paramName)
	if param == "" {
		return 0, false, nil
	}
	exists = true
	value, err = strconv.ParseInt(param, 10, 64)
	return
}

func pathParamStr(c echo.Context, paramName string) (value string, exists bool, err error) {
	value = c.Param(paramName)
	if value == "" {
		exists = false
	} else {
		exists = true
	}
	return
}

func queryParamFloat(c echo.Context, paramName string) (value float64, exists bool, err error) {
	param := c.QueryParam(paramName)
	if param == "" {
		return 0, false, nil
	}
	exists = true
	value, err = strconv.ParseFloat(param, 64)
	return
}

func queryParamInt(c echo.Context, paramName string) (value int64, exists bool, err error) {
	param := c.QueryParam(paramName)
	if param == "" {
		return 0, false, nil
	}
	exists = true
	value, err = strconv.ParseInt(param, 10, 64)
	return
}

func queryParamStr(c echo.Context, paramName string) (value string, exists bool, err error) {
	value = c.QueryParam(paramName)
	if value == "" {
		exists = false
	} else {
		exists = true
	}
	return
}
