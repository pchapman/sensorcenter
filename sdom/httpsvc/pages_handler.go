package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"gitlab.com/pchapman/sensorcenter/sdom/view"
	"net/http"
	"time"
)

const (
	homeURI   = "/"
	statusURI = "/status/:" + scom.ParamUserDashId
)

type homePageHandler struct {
	svcs      *services.SdomServices
	templates *view.Templates
}

func newHomePageHandler(svcs *services.SdomServices, templates *view.Templates) *homePageHandler {
	return &homePageHandler{
		svcs:      svcs,
		templates: templates,
	}
}

func (ph *homePageHandler) registerRoutes(e *echo.Echo) {
	e.GET(homeURI, ph.homeHandler)
	e.GET(statusURI, ph.statusHandler)
	e.POST(statusURI, ph.statusHandler)
}

func (ph *homePageHandler) homeHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		data := map[string]interface{}{
			"user": cc.user,
		}
		dashBoards, err := ph.svcs.UserDashService.RetrieveUserDashes(cc.user.Id)
		if err != nil {
			return err
		}

		dashData := []map[string]interface{}{}
		for _, dash := range dashBoards {
			sd, err := ph.statusFragData(&dash, true)
			if err != nil {
				return err
			}
			dashData = append(dashData, sd)
		}
		data["dashData"] = dashData
		return ph.templates.Render(c, "home.html", data)
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, loginURI)
	}
}

func (ph *homePageHandler) statusHandler(c echo.Context) error {
	if c.Request().Header.Get("HX-Request") == "" {
		// This is not an HTMX AJAX request, redirect to the home page
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	}
	cc, _ := c.(*SessionContext)
	if cc.authenticated() && cc.user != nil {
		dashId, ok, err := pathParamInt(c, scom.ParamUserDashId)
		if !ok || err != nil {
			return echo.ErrBadRequest
		}
		dash, err := ph.svcs.UserDashService.RetrieveUserDashById(dashId)
		if err != nil {
			return err
		}
		if dash == nil {
			return echo.ErrNotFound
		}
		if cc.canRead(&dash.LocationId, nil) {
			refresh := true
			if c.Request().Method == http.MethodPost {
				val := c.FormValue("autoRefresh")
				if val != "on" {
					refresh = false
				}
			}
			data, err := ph.statusFragData(dash, refresh)
			if err != nil {
				return err
			}
			return ph.templates.Render(c, "status_frag.html", data)
		} else {
			return echo.ErrForbidden
		}
	} else {
		return c.Redirect(http.StatusTemporaryRedirect, loginURI)
	}
}

func (ph *homePageHandler) statusFragData(dash *scom.UserDash, refresh bool) (map[string]interface{}, error) {
	sensorReadings, err := ph.svcs.SensorService.RetrieveLatestSensorReadingsForLocation(dash.LocationId)
	if err != nil {
		return nil, err
	}
	sensorReadings.Reported = time.Now()
	data := map[string]interface{}{
		"autoRefresh":    true,
		"dashId":         dash.Id,
		"refreshTrigger": "every 60s",
		"sensorReadings": sensorReadings,
	}
	if !refresh {
		data["autoRefresh"] = false
		data["refreshTrigger"] = "none"
	}
	return data, nil
}
