package httpsvc

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/config"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"log"
	"net/http"
	"os"
	"slices"
	"strings"
	"time"
)

// Session Data Keys
const (
	authSessionsKey string = "auth_session"
	authStateKey    string = "auth_state"
	firstNameKey    string = "first_name"
	lastNameKey     string = "last_name"
	sessionTimeout  string = "session_timeout"
	userIdKey       string = "user_id"
)

// URIs
const (
	loginURI        = "/login"
	logoutURI       = "/logout"
	authCallbackURI = "/auth/callback"
)

type AccessTokenErrorResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	ErrorCodes       []int  `json:"error_codes"`
	Timestamp        string `json:"timestamp"`
	TraceId          string `json:"trace_id"`
	CorrelationId    string `json:"correlation_id"`
}

type AccessTokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int64  `json:"expires_in"`
	ExpiresOn    int64  `json:"expires_on"`
	NotBefore    int64  `json:"not_before"`
	Resource     string `json:"resource:"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
}

type SessionContext struct {
	allowedLocations []int64
	allowedSensors   []int64
	reporterId       int64
	session          *sessions.Session
	user             *services.User
	userDashId       int64
	echo.Context
}

type UserTimeZone struct {
	UseAutomatic      string `json:"useAutomaticTimezone"`
	AutomaticTimezone string `json:"automaticTimezone"`
	ManualTimezone    string `json:"manualTimezone"`
}

func (cc *SessionContext) authenticated() bool {
	return cc.user != nil || cc.reporterId > 0 || cc.userDashId > 0
}

func (cc *SessionContext) canRead(locationId *int64, sensorId *int64) bool {
	if locationId != nil && !slices.Contains(cc.allowedLocations, *locationId) {
		return false
	}
	if sensorId != nil && !slices.Contains(cc.allowedSensors, *sensorId) {
		return false
	}
	return true
}

func (cc *SessionContext) canWrite(locationId *int64, sensorId *int64) bool {
	if cc.userDashId > 0 {
		return false
	}
	if locationId != nil && !slices.Contains(cc.allowedLocations, *locationId) {
		return false
	}
	if sensorId != nil && !slices.Contains(cc.allowedSensors, *sensorId) {
		return false
	}
	return true
}

func (cc *SessionContext) limitLocations(locations []scom.Location) (allowedLocations []scom.Location) {
	for _, loc := range locations {
		if slices.Contains(cc.allowedLocations, loc.Id) {
			allowedLocations = append(allowedLocations, loc)
		}
	}
	return
}

func (cc *SessionContext) limitSensorReadings(readings []scom.SensorReading) (allowedReadings []scom.SensorReading) {
	for _, reading := range readings {
		if slices.Contains(cc.allowedSensors, reading.SensorId) {
			allowedReadings = append(allowedReadings, reading)
		}
	}
	return
}

func (cc *SessionContext) limitSensors(sensors []scom.Sensor) (allowedSensors []scom.Sensor) {
	for _, sensor := range sensors {
		if slices.Contains(cc.allowedSensors, sensor.Id) {
			allowedSensors = append(allowedSensors, sensor)
		}
	}
	return
}

type authHandler struct {
	authSvc             services.AuthService
	reporterSvc         services.ReporterService
	userAuthCallbackUrl string
	userDashSvc         services.UserDashService
}

func newAuthHandler(svcs *services.SdomServices) (*authHandler, error) {
	authCallbackURl := os.Getenv(config.EnvAuthCallbackUrl)
	if authCallbackURl == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvAuthCallbackUrl)
	}

	authProviderUrl := os.Getenv(config.EnvAuthProviderUrl)
	if authProviderUrl == "" {
		return nil, fmt.Errorf("environment variable %s must be set", config.EnvAuthProviderUrl)
	}
	return &authHandler{
		userAuthCallbackUrl: authCallbackURl,
		authSvc:             svcs.AuthService,
		reporterSvc:         svcs.ReporterService,
		userDashSvc:         svcs.UserDashService,
	}, nil
}

const authSessionTimeout = 3600

func (ah *authHandler) authenticateUser(cc *SessionContext, bearerToken string) error {
	user, err := ah.authSvc.AuthenticateUserToken(bearerToken)
	if err != nil {
		return err
	}
	if user == nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Invalid token")
	}
	cc.user = user
	cc.allowedLocations, cc.allowedSensors, err = ah.authSvc.AuthorizeUser(user.Id)
	if err != nil {
		return echo.ErrInternalServerError
	}
	err = ah.storeSession(cc, authSessionTimeout)
	if err != nil {
		return err
	}
	log.Printf("User %s authenticated", user.Id)
	return nil
}

func (ah *authHandler) authenticateReporter(cc *SessionContext, apiID, apiSecret string) (ok bool, err error) {
	reporter, err := ah.authSvc.AuthenticateReporter(apiID, apiSecret)
	if err != nil {
		if !services.IsNotFoundError(err) {
			log.Printf("Error authenticating reporter: %s", err)
			return false, err
		}
		log.Printf(" Reporter not found")
	}
	if reporter != nil {
		cc.reporterId = reporter.Id
		cc.allowedLocations = []int64{reporter.LocationId}
		cc.allowedSensors, err = ah.authSvc.AuthorizeReporter(reporter.Id)
		if err != nil {
			log.Printf("Error getting list of allowed sensors for reporter: %s", err)
			return false, err
		}
		log.Printf("Reporter %d authenticated", reporter.Id)
		return true, nil
	}
	return false, nil
}

func (ah *authHandler) authorizeUserDash(cc *SessionContext, apiID, apiSecret string) (ok bool, err error) {
	userDash, err := ah.authSvc.AuthenticateUserDash(apiID, apiSecret)
	if err != nil {
		// Else, dashboard not found or some other error.  Return error.
		log.Printf("Error authenticating user dashboard: %s", err)
		return false, err
	}
	if userDash != nil {
		cc.userDashId = userDash.Id
		cc.allowedLocations = []int64{userDash.LocationId}
		cc.allowedSensors, err = ah.authSvc.AuthorizeUserDash(userDash.Id)
		if err != nil {
			log.Printf("Error getting list of allowed sensors for dashboard: %s", err)
			return false, err
		}
		log.Printf("User dashboard %d authenticated", userDash.Id)
		return true, nil
	}
	return false, nil
}

func (ah *authHandler) authMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var err error
		var ok bool

		// Only user info is stored in the session, as it is encrypted and stored in cookies
		// API keys are re-validated every time.
		cc := &SessionContext{Context: c}
		cc.session, _ = session.Get(authSessionsKey, c)

		timeout, ok := cc.session.Values[sessionTimeout].(int64)
		rn := time.Now().Unix()
		if ok && timeout > rn {
			err := ah.invalidateSession(cc)
			if err != nil {
				return err
			}
		} else {
			err = ah.unpackSession(cc)
			if err != nil {
				return err
			}
		}

		if !cc.authenticated() {
			auth := c.Request().Header.Get("Authorization")
			if auth != "" {
				if strings.HasPrefix(auth, "Bearer ") {
					auth = strings.Replace(auth, "Bearer ", "", -1)
					err = ah.authenticateUser(cc, auth)
					if err != nil {
						return err
					}
				} else if strings.HasPrefix(auth, "API ") {
					log.Print("API token found")
					auth = strings.Replace(auth, "API ", "", -1)
					parts := strings.Split(auth, ":")
					if len(parts) != 2 {
						log.Printf("Invalid API token: %s", auth)
						return echo.NewHTTPError(http.StatusBadRequest, "Invalid API key")
					}

					// Is it a reporter?
					ok, err = ah.authenticateReporter(cc, parts[0], parts[1])
					if err != nil {
						return err
					}
					if ok {
						return next(cc)
					}

					// Is it a user dashboard?
					ok, err = ah.authorizeUserDash(cc, parts[0], parts[1])
					if err != nil {
						return err
					}
					if ok {
						return next(cc)
					}

					log.Printf("unable to authenticate caller")
					return echo.NewHTTPError(http.StatusUnauthorized, "Invalid API key")
				}
			}
		}

		return next(cc)
	}
}

func (ah *authHandler) callbackHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	err := ah.unpackSession(cc)

	// Get Session and setting Cookies
	state, ok := cc.session.Values[authStateKey].(string)
	if !ok {
		return fmt.Errorf("invalid type for key '%s'", authStateKey)
	}

	// I should get back a URL with two parameters: code and state
	code := cc.QueryParams().Get("code")
	if code == "" {
		return errors.New("invalid code")
	}
	givenState := cc.QueryParam("state")
	if givenState == "" {
		return errors.New("invalid state")
	}
	if givenState != state {
		return fmt.Errorf("state missmatch: %s != %s", state, givenState)
	}

	token, expiresIn, err := ah.authSvc.ObtainAccessToken(code, ah.userAuthCallbackUrl)
	if err != nil {
		return err
	}

	user, err := ah.authSvc.AuthenticateUserToken(token)
	if err != nil {
		return err
	}
	cc.user = user
	cc.allowedLocations, cc.allowedSensors, err = ah.authSvc.AuthorizeUser(user.Id)
	if err != nil {
		return err
	}

	// Store session
	err = ah.storeSession(cc, int(expiresIn))
	if err != nil {
		cc.Logger().Errorf("unable to save session: %s", err)
		return errors.New("unable to save session")
	}

	return cc.Redirect(http.StatusTemporaryRedirect, homeURI)
}

func (ah *authHandler) invalidateSession(cc *SessionContext) error {
	cc.user = nil
	return ah.storeSession(cc, authSessionTimeout)
}

func (ah *authHandler) loginHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)
	if cc.authenticated() {
		return c.Redirect(http.StatusTemporaryRedirect, homeURI)
	} else {
		state := uuid.New().String()
		cc.session.Values[authStateKey] = state
		e := cc.session.Save(c.Request(), c.Response())
		if e != nil {
			cc.Logger().Errorf("unable to save session: %s", e)
			return errors.New("unable to save session")
		}

		// Begin the OAuth2 authorization flow
		return cc.Redirect(http.StatusTemporaryRedirect, ah.authSvc.OAuthProviderLoginURL(state, ah.userAuthCallbackUrl))
	}
}

func (ah *authHandler) logoutHandler(c echo.Context) error {
	cc, _ := c.(*SessionContext)

	err := ah.invalidateSession(cc)
	if err != nil {
		return err
	}

	return cc.Redirect(http.StatusTemporaryRedirect, loginURI)
}

func (ah *authHandler) registerRoutes(e *echo.Echo) {
	e.GET(loginURI, ah.loginHandler)
	e.GET(logoutURI, ah.logoutHandler)
	e.GET(authCallbackURI, ah.callbackHandler)
}

func (ah *authHandler) storeSession(cc *SessionContext, expiration int) error {
	// Only user info is stored in the session, as it is encrypted and stored in cookies.
	// API keys are re-validated every time.
	if cc.user != nil {
		cc.session.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   0,
			HttpOnly: true,
		}
		cc.session.Values[firstNameKey] = cc.user.FirstName
		cc.session.Values[lastNameKey] = cc.user.LastName
		cc.session.Values[userIdKey] = cc.user.Id
	} else {
		cc.session.Options = &sessions.Options{
			Path:     "/",
			MaxAge:   expiration, // in seconds
			HttpOnly: true,
		}
		cc.session.Values = map[interface{}]interface{}{}
		cc.session.Values[firstNameKey] = ""
		cc.session.Values[lastNameKey] = ""
		cc.session.Values[userIdKey] = 0
	}
	return cc.session.Save(cc.Request(), cc.Response())
}

func (ah *authHandler) unpackSession(cc *SessionContext) error {
	// Only user info is stored in the session, as it is encrypted and stored in cookies
	// API keys are re-validated every time.
	var err error
	var ok bool
	var s string
	if s, ok = cc.session.Values[userIdKey].(string); ok {
		cc.user = &services.User{}
		// user was authenticated, load user data
		cc.user.Id = s
		if s, ok = cc.session.Values[firstNameKey].(string); ok {
			cc.user.FirstName = s
		}
		if s, ok = cc.session.Values[lastNameKey].(string); ok {
			cc.user.LastName = s
		}
		cc.allowedLocations, cc.allowedSensors, err = ah.authSvc.AuthorizeUser(cc.user.Id)
		if err != nil {
			return err
		}
		log.Printf("User %s session unpacked", cc.user.Id)
	}
	return nil
}
