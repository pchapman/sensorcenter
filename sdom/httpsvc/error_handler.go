package httpsvc

import (
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/sdom/view"
	"net/http"
)

type ErrorHandler struct {
	templates *view.Templates
}

func newErrorHandler(templates *view.Templates) *ErrorHandler {
	return &ErrorHandler{
		templates: templates,
	}
}

func (eh *ErrorHandler) ErrorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	errorId := uuid.New().String()
	c.Logger().Error("Caught Error: %s", err, "errorId", errorId)

	data := make(map[string]interface{})
	data["code"] = code
	data["errorId"] = errorId
	data["message"] = err.Error()

	var e2 error
	if c.Request().Header.Get("Accept") == "application/json" {
		e2 = c.JSON(code, data)
	} else {
		e2 = eh.templates.Render(c, "error.html", data)
	}

	if e2 != nil {
		c.Logger().Error(e2)
	}

	// Fallback
	_ = c.String(code, err.Error())
}
