package httpsvc

/* Handlers for reporting and querying sensors and sensor readings.
 */

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"net/http"
)

type sensorApiHandler struct {
	sensorSvc services.SensorService
}

func newSensorApiHandler(services *services.SdomServices) *sensorApiHandler {
	return &sensorApiHandler{
		sensorSvc: services.SensorService,
	}
}

func (h *sensorApiHandler) registerRoutes(e *echo.Echo) {
	e.POST(scom.PathPostNewReading, h.reportSensorReading)
	e.GET(scom.PathGetAllSensors, h.retrieveAllSensors)
}

func (h *sensorApiHandler) reportSensorReading(c echo.Context) error {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}
	sensorId, ok, err := pathParamInt(c, scom.ParamSensorId)
	if !ok || err != nil {
		return echo.ErrBadRequest
	}
	if !cc.canWrite(nil, &sensorId) {
		return echo.ErrForbidden
	}

	unixTime, ok, err := queryParamInt(c, scom.ParamTimestamp)
	if err != nil {
		return echo.ErrBadRequest
	}
	if !ok {
		unixTime = 0 // It will default to current time in the service
	}
	value, ok, err := queryParamFloat(c, scom.ParamValue)
	if !ok || err != nil {
		return echo.ErrBadRequest
	}

	sensor, err := h.sensorSvc.RetrieveSensorById(sensorId)
	if err != nil {
		if services.IsNotFound(err) {
			return echo.ErrNotFound
		}
		return err
	}

	_, err = h.sensorSvc.ReportReading(sensor, unixTime, value)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (h *sensorApiHandler) retrieveAllSensors(c echo.Context) error {
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}
	sensors, err := h.sensorSvc.RetrieveAllSensors()
	if err != nil {
		return err
	}
	sensors = cc.limitSensors(sensors)
	return c.JSON(http.StatusOK, sensors)
}
