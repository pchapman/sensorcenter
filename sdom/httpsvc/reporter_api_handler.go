package httpsvc

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/sdom/services"
	"log"
	"net/http"
	"strconv"
)

type reporterApiHandler struct {
	reporterSvc services.ReporterService
}

func newReporterApiHandler(svcs *services.SdomServices) *reporterApiHandler {
	return &reporterApiHandler{
		reporterSvc: svcs.ReporterService,
	}
}

func (h *reporterApiHandler) registerRoutes(e *echo.Echo) {
	e.GET(scom.PathGetReporter, h.retrieveReporter)
}

func (h *reporterApiHandler) retrieveReporter(c echo.Context) error {
	var err error
	cc, ok := c.(*SessionContext)
	if !ok {
		return echo.ErrUnauthorized
	}

	reporterIdStr := c.Param(scom.ParamReporterId)
	if reporterIdStr == "" {
		return echo.ErrBadRequest
	}
	var reporterId int64
	if reporterIdStr == "me" {
		if cc.reporterId == 0 {
			log.Printf("Reporter id \"me\" requested, but no reporter authenticated")
			return echo.ErrBadRequest
		}
		reporterId = cc.reporterId
	} else {
		reporterId, err = strconv.ParseInt(reporterIdStr, 10, 64)
		if err != nil {
			log.Printf("Invalid reporter id parameter: \"%s\"", reporterIdStr)
			return echo.ErrBadRequest
		}
	}
	reporter, err := h.reporterSvc.RetrieveReporterById(reporterId)
	if err != nil {
		if services.IsNotFound(err) {
			return echo.ErrNotFound
		}
		log.Printf("Failed to retrieve reporter %d: %s", reporterId, err)
		return err
	}
	return c.JSON(http.StatusOK, reporter)
}
