package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/pchapman/sensorcenter/sdom/cmd"
)

// This value is set in build:
// go build -ldflags "-X main.appVersion=1.0.0"
var appVersion string

func main() {
	cmd.Execute()
}
