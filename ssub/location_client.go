package ssub

import (
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"strings"
)

type LocationClient interface {
	RetrieveLocation(id int64) (location *scom.Location, err error)
	RetrieveLocationReadings(int int64) (readings *scom.LocationSensorReadings, err error)
}

type locationClient struct {
	apiBase      string
	authProvider AuthProvider
}

func NewLocationClient(apiBase string, authProvider AuthProvider) LocationClient {
	return &locationClient{
		apiBase:      apiBase,
		authProvider: authProvider,
	}
}

func (lc *locationClient) RetrieveLocation(id int64) (location *scom.Location, err error) {
	locationId := fmt.Sprintf("%d", id)
	u := lc.apiBase + strings.ReplaceAll(scom.PathGetLocation, ":"+scom.ParamLocationId, locationId)
	loc := &scom.Location{}
	err = executeRestCall("GET", u, lc.authProvider, nil, loc)
	return
}

func (lc *locationClient) RetrieveLocationReadings(int int64) (readings *scom.LocationSensorReadings, err error) {
	locationId := fmt.Sprintf("%d", int)
	u := lc.apiBase + strings.ReplaceAll(scom.PathGetLocationReadings, ":"+scom.ParamLocationId, locationId)
	readings = &scom.LocationSensorReadings{}
	err = executeRestCall("GET", u, lc.authProvider, nil, readings)
	return
}
