package ssub

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"strings"
)

type UserDashClient interface {
	RetrieveThisDash() (result *scom.UserDash, err error)
}

type userDashClient struct {
	apiBase      string
	authProvider AuthProvider
}

func NewUserDashClient(apiBase string, authProvider AuthProvider) UserDashClient {
	return &userDashClient{
		apiBase:      apiBase,
		authProvider: authProvider,
	}
}

func (uc *userDashClient) RetrieveThisDash() (result *scom.UserDash, err error) {
	u := uc.apiBase + strings.ReplaceAll(scom.PathGetUserDash, ":"+scom.ParamUserDashId, "me")
	result = new(scom.UserDash)
	err = executeRestCall("GET", u, uc.authProvider, nil, result)
	return
}
