package sc_thermal

import (
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"gitlab.com/pchapman/weatherflow_go"
	"time"
)

type wfThermalSensor struct {
	client      *ssub.Ssub
	sensor      *scom.Sensor
	wfApiKey    string
	wfStationId int
}

func newWeatherFlowSensor(client *ssub.Ssub, sensor *scom.Sensor, config *Config, scheduler scom.Scheduler) error {

	ts := &wfThermalSensor{
		client:      client,
		sensor:      sensor,
		wfApiKey:    config.WFApiKey,
		wfStationId: config.WFStationId,
	}

	return scheduler.ScheduleSensorChecks(sensor, ts.checkSensor)
}

func (ts wfThermalSensor) checkSensor() error {
	obs, err := weatherflow_go.QueryLastestStationObservation(ts.wfApiKey, ts.wfStationId)
	if err != nil {
		return err
	}
	return ts.client.Sensors.ReportSensorReading(ts.sensor.Id, float64(obs.List[0].AirTemperature), time.Now().UTC().Unix())
}

func validateWFSensorConfig(config *Config) error {
	if config.WFApiKey == "" {
		return fmt.Errorf("WFApiKey cannot be empty")
	}
	if config.WFStationId == 0 {
		return fmt.Errorf("WFStationId cannot be 0")
	}
	return nil
}
