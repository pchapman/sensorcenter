package sc_thermal

import (
	"context"
	"fmt"
	"github.com/hectormalot/omgo"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"time"
)

// omThermalSensor is a sensor that reads temperatures from the OpenMeteo API. https://open-meteo.com/
type omThermalSensor struct {
	client    *ssub.Ssub
	latitude  float64
	longitude float64
	omgo      *omgo.Client
	sensor    *scom.Sensor
}

func newOpenMeteoSensor(client *ssub.Ssub, sensor *scom.Sensor, config *Config, scheduler scom.Scheduler) error {

	location, err := client.Locations.RetrieveLocation(sensor.LocationId)
	if err != nil {
		return err
	}
	if location.Latitude == nil || location.Longitude == nil {
		return fmt.Errorf("location %d is missing latitude or longitude", sensor.LocationId)
	}
	ts := &omThermalSensor{
		client:    client,
		latitude:  float64(*location.Latitude),
		longitude: float64(*location.Longitude),
		sensor:    sensor,
	}

	c, err := omgo.NewClient()
	if err != nil {
		return err
	}
	ts.omgo = &c

	return scheduler.ScheduleSensorChecks(sensor, ts.checkSensor)
}

func (ts omThermalSensor) checkSensor() error {
	loc, err := omgo.NewLocation(ts.latitude, ts.longitude)
	if err != nil {
		return err
	}
	cur, err := ts.omgo.CurrentWeather(context.TODO(), loc, nil)
	if err != nil {
		return err
	}
	return ts.client.Sensors.ReportSensorReading(ts.sensor.Id, cur.Temperature, time.Now().UTC().Unix())
}

func validateOMSensorConfig(config *Config) error {
	return nil
}
