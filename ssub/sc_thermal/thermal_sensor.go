package sc_thermal

import (
	"encoding/json"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
)

const (
	SubTypeOneWire     SubType = "OneWire"
	SubTypeOpenMeteo   SubType = "OpenMeteo"
	SubTypeWeatherFlow SubType = "WeatherFlow"
)

func InitializeThermalSensor(client *ssub.Ssub, sensor *scom.Sensor, scheduler scom.Scheduler) error {
	if !scom.ValidateSchedule(sensor.Schedule) {
		return fmt.Errorf("sensor schedule is invalid")
	}
	config, err := NewSensorConfig(sensor.ConfigData)
	if err != nil {
		return err
	}
	st := SubType(config.SubType)
	switch st {
	case SubTypeOneWire:
		return newOneWireSensor(client, sensor, config, scheduler)
	case SubTypeOpenMeteo:
		return newOpenMeteoSensor(client, sensor, config, scheduler)
	case SubTypeWeatherFlow:
		return newWeatherFlowSensor(client, sensor, config, scheduler)
	default:
		return fmt.Errorf("unknown subType %s", st)
	}
}

type SubType string

type Config struct {
	ReadFilePath string  `json:"ReadFilePath" yaml:"ReadFilePath"`
	SubType      SubType `json:"SubType" yaml:"SubType"`
	WFApiKey     string  `json:"WFApiKey" yaml:"WFApiKey"`
	WFStationId  int     `json:"WFStationId" yaml:"WFStationId"`
}

func NewSensorConfig(configData string) (*Config, error) {
	config := &Config{}
	err := json.Unmarshal([]byte(configData), config)
	if err != nil {
		return nil, err
	}
	return config, ValidateSensorConfig(config)
}

func GenerateSensorConfigData(config *Config) (string, error) {
	err := ValidateSensorConfig(config)
	if err != nil {
		return "", err
	}
	data, err := json.Marshal(config)
	return string(data), err
}

func ValidateSensorConfig(config *Config) error {
	if config == nil {
		return fmt.Errorf("config cannot be nil")
	}
	switch config.SubType {
	case SubTypeOneWire:
		return validate1WSensorConfig(config)
	case SubTypeOpenMeteo:
		return validateOMSensorConfig(config)
	case SubTypeWeatherFlow:
		return validateWFSensorConfig(config)
	default:
		return fmt.Errorf("unknown subType \"%s\"", config.SubType)
	}
}
