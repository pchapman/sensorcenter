package sc_thermal

import (
	"bufio"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"os"
	"strconv"
	"strings"
	"time"
)

// oneWireSensor handles obtaining thermal sensor data from the One-Wire linux interface's one-wire interface exposed
// through the w1-gpio and w1-therm kernel modules and publishing it to the sdom server.
// See https://www.kernel.org/doc/html/latest/w1/slaves/w1_therm.html
// See https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
// See https://www.kernel.org/doc/Documentation/thermal/sysfs-api.txt
// See https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

type oneWireSensor struct {
	client       *ssub.Ssub
	sensor       *scom.Sensor
	readFilePath string
}

func newOneWireSensor(client *ssub.Ssub, sensor *scom.Sensor, config *Config, scheduler scom.Scheduler) error {
	ts := &oneWireSensor{
		client:       client,
		sensor:       sensor,
		readFilePath: config.ReadFilePath,
	}

	return scheduler.ScheduleSensorChecks(sensor, ts.checkSensor)
}

func (ts oneWireSensor) checkSensor() error {
	v, e := parse1WThermometer(ts.readFilePath)
	if e != nil {
		return e
	}
	return ts.client.Sensors.ReportSensorReading(ts.sensor.Id, float64(v), time.Now().UTC().Unix())
}

func parse1WThermometer(filePath string) (float32, error) {
	// Read data line by line to look for a string that contains t= followed by an integer value
	// If found, parse the string and return the temperature, which is the integer value divided by 1000
	// If not found, return an error

	file, err := os.Open(filePath)
	if err != nil {
		return 0.0, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		idx := strings.Index(line, "t=")
		if idx > -1 {
			temperature, err := strconv.ParseFloat(line[idx+2:], 32)
			if err != nil {
				return 0.0, err
			}

			return float32(temperature) / 1000, nil
		}
	}

	return 0.0, fmt.Errorf("no temperature value found in %s", filePath)
}

func validate1WSensorConfig(config *Config) error {
	if config.ReadFilePath == "" {
		return fmt.Errorf("ReadFilePath cannot be empty")
	}
	return nil
}
