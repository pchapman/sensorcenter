# Installing YoStats

## Hardware and System Setup

The below are taken from the tutorial at https://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

### Connect the DS18B20 to the Raspberry PI

The DS18B20 has three separate pins for ground, data, and Vcc:

![Pinout Diagram](Raspberry-Pi-DS18B20-Tutorial-DS18B20-Pinout-Diagram.png)

### Wiring for SSH Terminal Output

Follow this wiring diagram to output the temperature to an SSH terminal:

![Wiring Diagram](Raspberry-Pi-DS18B20.png)

R1: 4.7K Ohm or 10K Ohm resistor

### Enable the One-Wire Interface

We’ll need to enable the One-Wire interface before the Pi can receive data from the sensor. Once you’ve connected the DS18B20, power up your Pi and log in, then follow these steps to enable the One-Wire interface:

1. At the command prompt, enter `sudo nano /boot/firmware/config.txt`, then add this to the bottom of the file:

`dtoverlay=w1-gpio`

2. Exit Nano, and reboot the Pi with sudo reboot

3. Log in to the Pi again, and at the command prompt enter `sudo modprobe w1-gpio`

4. Then enter `sudo modprobe w1-therm`

5. Change directories to the `/sys/bus/w1/devices` directory by entering `cd /sys/bus/w1/devices`

6. Now enter ls to list the devices:

![Terminal Device Address](Raspberry-Pi-DS18B20-Temperature-Sensor-Tutorial-One-Wire-Device-Address.png)

In my case, `28-000006637696 w1_bus_master1` is displayed.

7. Now enter `cd 28-XXXXXXXXXXXX` (change the X’s to your own address)

For example, in my case I would enter `cd 28-000006637696`

8. Enter `cat w1_slave` which will show the raw temperature reading output by the sensor:

![Terminal Device Output](Raspberry-Pi-DS18B20-Temperature-Sensor-Tutorial-DS18B20-Raw-Output.png)

Here the temperature reading is `t=28625`, which when divided by 1000 indicates a temperature of 28.625 degrees Celsius.

9. To make the changes permanent, edit `/etc/modules` using `sudo nano /etc/modules`  Ensure the following lines are in the file:

```
w1_gpio
w1_therm
```
