package sc_thermal

import "testing"

func TestParseW1Thermometer(t *testing.T) {
	temp, err := parse1WThermometer("testdata/one_wire_therm_test_data.txt")
	if err != nil {
		t.Error(err)
	}
	if temp != 27.75 {
		t.Errorf("expected 27.75, got %f", temp)
	}
}
