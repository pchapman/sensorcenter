package ssub

import (
	"encoding/json"
	"testing"
)

const sensorArrayJson = `
[
  {
    "id": 2,
    "locationId": 1,
    "reporterId": 1,
    "name": "Tempest Weather Station Temp",
    "type": "temperature",
    "schedule": "@every 1 minutes",
    "configData": "{\"SubType\": \"WeatherFlow\", \"WFApiKey\": \"ac06f422-77e4-46e9-ada3-5c2a9ccbc3ba\", \"WFStationId\": 61817}",
    "storedUnit": "C",
    "displayUnit": "F",
    "displayConversion": "value * (9/5) + 32"
  }
]
`

func TestSensorSliceUnmarshal(t *testing.T) {
	ss := new(SensorsSlice)
	err := testUnmarshal(sensorArrayJson, ss)
	if err != nil {
		t.Error(err)
	}
	if len(*ss) != 1 {
		t.Errorf("expected 1, got %d", len(*ss))
	}
}

func testUnmarshal(testJson string, result interface{}) (err error) {
	err = json.Unmarshal([]byte(testJson), result)
	return
}
