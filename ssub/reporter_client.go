package ssub

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"strings"
)

type ReporterClient interface {
	RetrieveThisReporter() (result *scom.Reporter, err error)
}

type reporterClient struct {
	apiBase      string
	authProvider AuthProvider
}

func NewReporterClient(apiBase string, authProvider AuthProvider) ReporterClient {
	return &reporterClient{
		apiBase:      apiBase,
		authProvider: authProvider,
	}
}

func (r reporterClient) RetrieveThisReporter() (result *scom.Reporter, err error) {
	u := r.apiBase + strings.ReplaceAll(scom.PathGetReporter, ":"+scom.ParamReporterId, "me")
	result = new(scom.Reporter)
	err = executeRestCall("GET", u, r.authProvider, nil, result)
	return
}
