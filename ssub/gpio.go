package ssub

import (
	"github.com/stianeikeland/go-rpio/v4"
	"sync"
)

var Gpio = struct {
	uses int
	sync.RWMutex
}{
	uses: 0,
}

func GpioInUse() bool {
	Gpio.RLock()
	defer Gpio.RUnlock()
	return Gpio.uses > 0
}

func ReportGpioInUse() (err error) {
	Gpio.Lock()
	defer Gpio.Unlock()
	if Gpio.uses == 0 {
		err = rpio.Open()
		if err != nil {
			return
		}
	}
	Gpio.uses++
	return
}

func ReportGpioNotInUse() (err error) {
	Gpio.Lock()
	defer Gpio.Unlock()
	if Gpio.uses == 0 {
		return
	}
	Gpio.uses--
	if Gpio.uses == 0 {
		return rpio.Close()
	}
	return nil
}

func closeGpio() error {
	Gpio.Lock()
	defer Gpio.Unlock()
	if Gpio.uses == 0 {
		return nil
	}
	Gpio.uses = 0
	return rpio.Close()
}
