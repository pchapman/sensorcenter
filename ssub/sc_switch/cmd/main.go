package main

import (
	"flag"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_switch"
	"os"
	"os/signal"
	"syscall"
)

type cliSensorClient struct {
	sensor *scom.Sensor
	ssub.SensorClient
}

func (c *cliSensorClient) ReportSensorReading(reading *scom.SensorReading) (err error) {
	// Call NewSensorReading to ensure display value and other values are set properly
	reading, err = scom.NewSensorReading(c.sensor, reading.Timestamp, reading.Value)
	if err != nil {
	   panic(err)
	}
	sre := scom.SensorReadingElem{
		Sensor:  c.sensor,
		Reading: reading,
	}
	fmt.Printf("Sensor %d\tValue: %f\tDisp Value: %f\t: %s\n", reading.SensorId, reading.Value, reading.DisplayValue, sre.DisplayString())
	return nil
}

func main() {
	chipName := flag.String("chip", "", "Controller chip name")
	pinNum := flag.Int("pin", 0, "Pin number")
	edgeStr := flag.String("edge", "", "Edge name")
	pullStr := flag.String("pull", "", "Internal pull up/down resistor")

	flag.Parse()
	config := sc_switch.Config{
		ChipName:      *chipName,
		EdgeDetection: sc_switch.EdgeDetection(*edgeStr),
		GpioPin:       int32(*pinNum),
		Disposition:   sc_switch.Disposition(*pullStr),
	}
	config.SubType = sc_switch.SubTypeRPIGPIO
	err := sc_switch.ValidateSensorConfig(&config)
	if err != nil {
		fmt.Printf("Configuration error for sensor subtype %s: %s\n", config.SubType, err)
	}

	config.SubType = sc_switch.SubTypeGPIO
	err = sc_switch.ValidateSensorConfig(&config)
	if err != nil {
		fmt.Printf("Configuration error for sensor subtype %s: %s\n", config.SubType, err)
		return
	}

	cfgJson, err := sc_switch.GenerateSensorConfigData(&config)
	if err != nil {
		fmt.Println(err)
		return
	}

	sensor := &scom.Sensor{
		Id:                0,
		LocationId:        0,
		ReporterId:        0,
		Name:              "Dummy Sensor",
		Type:              scom.ST_Switch,
		Schedule:          "@every 1 seconds",
		ConfigData:        cfgJson,
		StoreUnit:         "State",
		DisplayUnit:       "Closed",
		DisplayConversion: "value==0?1:0", // For a normally open switch, swap values
		DisplaySigDigits:  nil,
	}
	client := ssub.Ssub{
		AuthProvider: ssub.NewReporterAuthProvider("reporterId", "reporterPassword"),
		Sensors: &cliSensorClient{
			sensor: sensor,
		},
	}

	scheduler := scom.NewScheduler()

	err = sc_switch.InitializeSwitchSensor(&client, sensor, scheduler)
	if err != nil {
		fmt.Println(err)
		return
	}
	scheduler.Start()

	// Create a channel to receive OS signals
	signalChan := make(chan os.Signal, 1)

	// Signal to wait for `Ctrl+C`
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	// Wait for the signal
	fmt.Printf("Press Ctrl+C to stop\n")
	sig := <-signalChan
	fmt.Println("Received signal:", sig)

	scheduler.Stop()
	ssub.Cleanup()

	// Exit the program
	os.Exit(0)
}
