package sc_switch

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
)

var invalidValue = errors.New("invalid value")

type EdgeDetection string

const (
	EdgeNone    EdgeDetection = "None"
	EdgeRising  EdgeDetection = "Rising"
	EdgeFalling EdgeDetection = "Falling"
	EdgeBoth    EdgeDetection = "Both"
)

func (e EdgeDetection) validate() error {
	switch e {
	case EdgeNone, EdgeRising, EdgeFalling, EdgeBoth:
		return nil
	default:
		return invalidValue
	}
}

func (e EdgeDetection) String() string {
	switch e {
	case EdgeNone:
		return "None"
	case EdgeRising:
		return "RisingEdge"
	case EdgeFalling:
		return "FallingEdge"
	case EdgeBoth:
		return "BothEdges"
	default:
		return ""
	}
}

type Disposition string

const (
	DispositionNoChange Disposition = "NoChange" // Do not change the previous pull resistor setting or an unknown value
	DispositionFloat    Disposition = "Float"    // Let the input float
	PullDown            Disposition = "PullDown" // Apply pull-down
	DispositionPullUp   Disposition = "PullUp"   // Apply pull-up
)

func (i Disposition) validate() error {
	switch i {
	case DispositionNoChange, DispositionFloat, PullDown, DispositionPullUp:
		return nil
	default:
		return invalidValue
	}
}

func (i Disposition) String() string {
	switch i {
	case DispositionNoChange:
		return "NoChange"
	case DispositionFloat:
		return "Float"
	case PullDown:
		return "PullDown"
	case DispositionPullUp:
		return "PullUp"
	default:
		return ""
	}
}

func DispositionValues() []Disposition {
	return []Disposition{DispositionNoChange, DispositionFloat, PullDown, DispositionPullUp}
}

type SubType string

const (
	// SubTypeAPI, value API is a switch sensor subtype that is the placeholder for a sensor value that is reported in
	// by some app via restful calls and not using the ssub library.
	SubTypeAPI SubType = "API"
	// SubTypeGPIO, value GPIO is a switch sensor subtype that reads the state of a GPIO pin on a Linux system that
	// exports GPIO through /dev/gpiochipX. It does so through the go-gpiocdev library which is based on the libgpiod
	// library.  See https://github.com/warthog618/go-gpiocdev
	SubTypeGPIO SubType = "GPIO"
	// SubTypeRPIGPIO, value RPI_GPIO is a switch sensor subtype that reads the state of a GPIO pin on a Raspberry PI.
	// It does so through the rpio library.  See https://github.com/stianeikeland/go-rpio.  This library is not likely
	// to work for other platforms.  SubTypeGPIO is recommended, even on Raspberry PI, as it is more cross-platform
	// compatible.
	SubTypeRPIGPIO SubType = "RPI_GPIO"
)

func ofType(s string) (SubType, error) {
	switch SubType(s) {
	case SubTypeAPI:
		return SubTypeAPI, nil
	case SubTypeGPIO:
		return SubTypeGPIO, nil
	case SubTypeRPIGPIO:
		return SubTypeRPIGPIO, nil
	default:
		return "", invalidValue
	}
}

func (s SubType) String() string {
	switch s {
	case SubTypeAPI:
		return "API"
	case SubTypeGPIO:
		return "GPIO"
	case SubTypeRPIGPIO:
		return "RPI_GPIO"
	default:
		return ""
	}
}

type Config struct {
	ChipName      string        `json:"ControllerChipName" yaml:"ControllerChipName"`
	EdgeDetection EdgeDetection `json:"EdgeDetection" yaml:"EdgeDetection"`
	GpioPin       int32         `json:"GpioPin" yaml:"GpioPin"`
	Disposition   Disposition   `json:"Disposition" yaml:"Disposition"`
	SubType       SubType       `json:"SubType" yaml:"SubType"`
}

func NewSensorConfig(configData string) (*Config, error) {
	config := &Config{}
	err := json.Unmarshal([]byte(configData), config)
	if err != nil {
		return nil, err
	}
	return config, ValidateSensorConfig(config)
}

func GenerateSensorConfigData(config *Config) (string, error) {
	err := ValidateSensorConfig(config)
	if err != nil {
		return "", err
	}
	data, err := json.Marshal(config)
	return string(data), err
}

func ValidateSensorConfig(config *Config) error {
	if config == nil {
		return fmt.Errorf("config cannot be nil")
	}
	switch config.SubType {
	case SubTypeAPI:
		return validateApiSensorConfig(config)
	case SubTypeGPIO:
		return validateGpioSensorConfig(config)
	case SubTypeRPIGPIO:
		return validateRpiGpioSensorConfig(config)
	default:
		return fmt.Errorf("unknown subType \"%s\"", config.SubType)
	}
}

func InitializeSwitchSensor(client *ssub.Ssub, sensor *scom.Sensor, scheduler scom.Scheduler) error {
	if !scom.ValidateSchedule(sensor.Schedule) {
		return fmt.Errorf("sensor schedule is invalid")
	}
	config, err := NewSensorConfig(sensor.ConfigData)
	if err != nil {
		return err
	}
	st := SubType(config.SubType)
	switch st {
	case SubTypeGPIO:
		return newGpioSensor(client, sensor, config, scheduler)
	case SubTypeRPIGPIO:
		return newRpiGpioSensor(client, sensor, config, scheduler)
	default:
		return fmt.Errorf("unknown or unsupported subType %s", st)
	}
}

func validateApiSensorConfig(config *Config) error {
	// This is a placeholder subtype for some application which will call in sensor settings.
	// There is no config required for this subtype.
	return nil
}
