package sc_switch

import (
	"fmt"
	"github.com/warthog618/go-gpiocdev"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"time"
)

// gpioSensor is a sensor that reads the state of a GPIO pin on a Linux system that exports GPIO through /dev/gpiochipX.
// It does so through the go-gpiocdev library which is based on the libgpiod library.
// See https://github.com/warthog618/go-gpiocdev
type gpioSensor struct {
	client *ssub.Ssub
	config *Config
	sensor *scom.Sensor
}

func newGpioSensor(client *ssub.Ssub, sensor *scom.Sensor, config *Config, scheduler scom.Scheduler) error {
	err := gpiocdev.IsChip(config.ChipName)
	if err != nil {
		return fmt.Errorf("invalid controller chip name %s: %s", config.ChipName, err.Error())
	}

	options := []gpiocdev.LineConfigOption{
		gpiocdev.AsInput,
	}
	switch config.Disposition {
	case PullDown:
		options = append(options, gpiocdev.LineBiasPullDown)
	case DispositionFloat:
		options = append(options, gpiocdev.LineBiasDisabled)
	case DispositionPullUp:
		options = append(options, gpiocdev.LineBiasPullDown)
	case DispositionNoChange:
		options = append(options, gpiocdev.LineBiasUnknown)
	default:
		return fmt.Errorf("invalid pin disposition %s", config.Disposition)
	}
	l, err := gpiocdev.RequestLine(config.ChipName, int(config.GpioPin), gpiocdev.AsInput)
	if err != nil {
		return err
	}
	defer l.Close()
	err = l.Reconfigure(options...)
	if err != nil {
		return err
	}

	sw := &gpioSensor{
		client: client,
		config: config,
		sensor: sensor,
	}
	return scheduler.ScheduleSensorChecks(sensor, sw.checkSensor)
}

func (sw gpioSensor) checkSensor() error {
	l, err := gpiocdev.RequestLine(sw.config.ChipName, int(sw.config.GpioPin), gpiocdev.AsInput)
	if err != nil {
		return err
	}
	defer l.Close()

	v, err := l.Value()
	if err != nil {
		return err
	}

	return sw.client.Sensors.ReportSensorReading(sw.sensor.Id, float64(v), time.Now().UTC().Unix())
}

func validateGpioSensorConfig(config *Config) (err error) {
	if config.ChipName == "" {
		return fmt.Errorf("controller chip name required")
	}
	if config.GpioPin < 0 {
		return fmt.Errorf("invalid gpio pin %d", config.GpioPin)
	}
	err = config.EdgeDetection.validate()
	if err != nil {
		return fmt.Errorf("invalid edge detection %s", config.EdgeDetection)
	}
	if config.EdgeDetection != EdgeNone {
		//TODO: Edge detection?
		return fmt.Errorf("the gpio sensor does not yet support edge detection")
	}
	err = config.Disposition.validate()
	if err != nil {
		return fmt.Errorf("invalid disposition %s", config.Disposition)
	}
	return
}
