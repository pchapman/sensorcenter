## What Chip and Pin Is it?
`gpiodetect` can be kinda useful for listing gpio control chips.
`gpioinfo chipname` for example `gpioinfo 300b000.pinctrl` can be helpful for showing info about GPIO lines, but you
really have to know the gpio pin vs physical pin which is only available from the manufacturer.

For Orange Pi devices, the `WiringOP` library can be really helpful for identifying GPIO lines for physical pins. For
the command `gpio readall`, the `Physical` columns indicate physical pin and the `GPIO` columns indicate GPIO lines.  It
comes built-in on Orange Pi images, but will work if compiled and run on armbian.

## To make GPIO (/dev/gpiochipX) readable by non-root:

https://ryan.lovelett.me/posts/write-udev-rule-for-gpiochip/

### TLDR;

1. Create a group named gpio and assign at least 1 user to it.
2. Create a file called `/etc/udev/rules.d/99-gpio.rules` and add the line `SUBSYSTEM=="gpio",NAME="gpiochip%n",OWNER="root",GROUP="gpio",MODE="0660"`
3. You should be able to reload rules via `sudo udevadm control --reload` but I find a reboot works the best
4. Verify permissions `ls -alh /dev/gpiochip*`

## Reading Values

Note that for a normally open contact using a pullup resistor, the value for closed is 0 and the value for open is 1,
which is a little counter-intuitive.  It is recommended to use the following configuration:

```json
{
  "StoreUnit": "State",
  "DisplayUnit": "Closed",
  "DisplayConversion": "value==0?1:0",
  "DisplaySigDigits": null
}
```

With the above configuration, the sensor value will display as "Closed" or "Not Closed".
