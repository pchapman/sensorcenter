package sc_switch

import (
	"fmt"
	"github.com/stianeikeland/go-rpio/v4"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"time"
)

// rpiGpioSensor is a sensor that reads the state of a GPIO pin on a Raspberry PI.  This library is likely to not work for
// other platforms.
type rpiGpioSensor struct {
	client *ssub.Ssub
	config *Config
	pin    *rpio.Pin
	sensor *scom.Sensor
}

func newRpiGpioSensor(client *ssub.Ssub, sensor *scom.Sensor, config *Config, scheduler scom.Scheduler) error {
	sw := &rpiGpioSensor{
		client: client,
		config: config,
		sensor: sensor,
	}
	// Set up GPIO
	err := ssub.ReportGpioInUse()
	if err != nil {
		return err
	}

	p := rpio.Pin(config.GpioPin)
	sw.pin = &p
	sw.pin.Input()
	switch config.Disposition {
	case PullDown:
		sw.pin.PullDown()
	case DispositionFloat:
		sw.pin.PullOff()
	case DispositionPullUp:
		sw.pin.PullUp()
	case DispositionNoChange:
		// Do nothing
	default:
		return fmt.Errorf("invalid pin disposition %s", config.Disposition)
	}
	switch config.EdgeDetection {
	case EdgeNone:
		sw.pin.Detect(rpio.NoEdge)
	case EdgeRising:
		sw.pin.Detect(rpio.RiseEdge)
	case EdgeFalling:
		sw.pin.Detect(rpio.FallEdge)
	case EdgeBoth:
		sw.pin.Detect(rpio.AnyEdge)
	default:
		return fmt.Errorf("invalid edge detection %s", config.EdgeDetection)
	}

	return scheduler.ScheduleSensorChecks(sensor, sw.checkSensor)
}

func (sw *rpiGpioSensor) checkSensor() error {
	var value float64
	if sw.config.EdgeDetection == EdgeNone {
		value = float64(sw.pin.Read())
	} else if sw.pin.EdgeDetected() {
		value = 1
	} else {
		value = 0
	}
	return sw.client.Sensors.ReportSensorReading(sw.sensor.Id, value, time.Now().UTC().Unix())
}

func validateRpiGpioSensorConfig(config *Config) (err error) {
	if config.GpioPin < 0 {
		return fmt.Errorf("invalid gpio pin %d", config.GpioPin)
	}
	err = config.EdgeDetection.validate()
	if err != nil {
		return fmt.Errorf("invalid edge detection %s", config.EdgeDetection)
	}
	err = config.Disposition.validate()
	if err != nil {
		return fmt.Errorf("invalid disposition %s", config.Disposition)
	}
	return
}
