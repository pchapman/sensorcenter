package ssub

import (
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"net/url"
	"strings"
)

type SensorsSlice []scom.Sensor

type SensorClient interface {
	ReportSensorReading(sensorId int64, value float64, unixTime int64) error
	RetrieveSensors() (SensorsSlice, error)
}

type sensorClient struct {
	apiBase      string
	authProvider AuthProvider
}

func NewSensorClient(apiBase string, authProvider AuthProvider) SensorClient {
	return &sensorClient{
		apiBase:      apiBase,
		authProvider: authProvider,
	}
}

func (sc *sensorClient) ReportSensorReading(sensorId int64, value float64, unixTime int64) error {
	path := sc.apiBase + strings.ReplaceAll(scom.PathPostNewReading, ":"+scom.ParamSensorId, fmt.Sprintf("%d", sensorId))
	u, err := url.Parse(path)
	if err != nil {
		return err
	}
	q := u.Query()
	q.Set(scom.ParamTimestamp, fmt.Sprintf("%d", unixTime))
	q.Set(scom.ParamValue, fmt.Sprintf("%f", value))
	u.RawQuery = q.Encode()
	return executeRestCall("POST", u.String(), sc.authProvider, nil, nil)
}

func (sc *sensorClient) RetrieveSensors() (result SensorsSlice, err error) {
	u := sc.apiBase + scom.PathGetAllSensors
	ss := new(SensorsSlice)
	err = executeRestCall("GET", u, sc.authProvider, nil, ss)
	return *ss, err
}
