package ssub

import "fmt"

type AuthProvider interface {
	AccessToken() string
}

type apiAuthProvider struct {
	apiKey    string
	apiSecret string
}

func (a *apiAuthProvider) AccessToken() string {
	return fmt.Sprintf("API %s:%s", a.apiKey, a.apiSecret)
}

func NewReporterAuthProvider(apiKey string, apiSecret string) AuthProvider {
	return &apiAuthProvider{
		apiKey:    apiKey,
		apiSecret: apiSecret,
	}
}

func NewUserDashAuthProvider(apiKey string, apiSecret string) AuthProvider {
	return &apiAuthProvider{
		apiKey:    apiKey,
		apiSecret: apiSecret,
	}
}

type UserAuthProvider struct {
	userToken string
}

func NewUserAuthProvider(userToken string) AuthProvider {
	return &UserAuthProvider{
		userToken: userToken,
	}
}

func (a *UserAuthProvider) AccessToken() string {
	return fmt.Sprintf("Bearer %s", a.userToken)
}
