package ssub

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

func executeRestCall(action string, url string, authProvider AuthProvider, data []byte, results interface{}) error {
	client := &http.Client{}

	req, err := http.NewRequest(action, url, bytes.NewBuffer(data))
	if err != nil {
		log.Printf("SSUB: Error building http request for %s against %s: %s\n", action, url, err)
		return err
	}

	log.Printf("SSUB: Executing %s against endpoint %s", action, url)

	if authProvider != nil {
		req.Header.Add("Authorization", authProvider.AccessToken())
	}
	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")
	resp, err := client.Do(req)

	if err != nil {
		log.Printf("SSUB: Got error for %s against %s: %s\n", action, url, err)
		return err
	}

	defer resp.Body.Close()
	log.Printf("SSUB: Response with status code %d for %s against endpoint %s", resp.StatusCode, action, url)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("SSUB: Got error getting response body for %s against %s: %s\n", action, url, err)
		return err
	}
	if resp.StatusCode != 200 {
		if resp.StatusCode == 204 && results == nil {
			// This is fine.  Processed ok, no content, but we don't expect any.
		} else {
			s := string(body)
			log.Printf("SSUB: Got status code %d for %s against %s: %s\n", resp.StatusCode, action, url, s)
			return errors.New(s)
		}
	}
	if results != nil {
		err = json.Unmarshal(body, results)
	}
	return err
}
