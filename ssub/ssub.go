package ssub

import (
	"log"
	"strings"
)

type Ssub struct {
	AuthProvider   AuthProvider
	Locations      LocationClient
	Reporters      ReporterClient
	Sensors        SensorClient
	UserDashboards UserDashClient
}

func NewSsub(apiURI string, provider AuthProvider) *Ssub {
	if strings.HasSuffix(apiURI, "/") {
		apiURI = apiURI[:len(apiURI)-1]
	}
	return &Ssub{
		AuthProvider:   provider,
		Locations:      NewLocationClient(apiURI, provider),
		Reporters:      NewReporterClient(apiURI, provider),
		Sensors:        NewSensorClient(apiURI, provider),
		UserDashboards: NewUserDashClient(apiURI, provider),
	}
}

// Cleanup should be called before the program exits. It releases any resources used by the library.
func Cleanup() {
	err := closeGpio()
	if err != nil {
		log.Printf("Error closing GPIO usage: %s", err.Error())
	}
	return
}
