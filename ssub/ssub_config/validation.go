package ssub_config

import (
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_switch"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_thermal"
)

func ValidateConfiguration(sensor *scom.Sensor) error {
	switch sensor.Type {
	case scom.ST_Switch:
		config, err := sc_switch.NewSensorConfig(sensor.ConfigData)
		if err != nil {
			return err
		}
		err = sc_switch.ValidateSensorConfig(config)
		return err
	case scom.ST_Temperature:
		config, err := sc_thermal.NewSensorConfig(sensor.ConfigData)
		if err != nil {
			return err
		}
		err = sc_thermal.ValidateSensorConfig(config)
		return err
	default:
		return nil
	}
}
