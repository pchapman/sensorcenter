# SensorCenter Reporter

SensorCenter Reporter is the edge device software responsible for checking 1 or more sensors on a regular schedule and
reporting them to a central sdom server.

## Installing SensorCenter Reporter with SystemD

1. Create system user and group `screporter`:  `sudo useradd screporter -s /sbin/nologin -M`
2. Copy sc_reporter executable into `/usr/local/bin`
3. Copy screporter.service into `/etc/systemd/system`
4. Ensure permissions for systemd file: `sudo chmod 750 /etc/systemd/system/screporter.service`
5. Refresh systemd: `systemctl daemon-reload`
6. Edit the configuration to modify environment variables: `sudo systemctl edit screporter.service`
7. Enable the service: `sudo systemctl enable screporter.service`
8. Start the service: `sudo systemctl start screporter.service`
9. Verify the service is running by checking logs: `sudo journalctl -f -u screporter.service`
