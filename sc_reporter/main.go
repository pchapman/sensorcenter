package main

import (
	"fmt"
	"gitlab.com/pchapman/sensorcenter/scom"
	"gitlab.com/pchapman/sensorcenter/ssub"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_switch"
	"gitlab.com/pchapman/sensorcenter/ssub/sc_thermal"
	"os"
	"os/signal"
	"syscall"
)

const (
	envSSApiUrl         = "SS_API_URL"
	envSSReporterKeyId  = "SS_REPORTER_KEY_ID"
	envSSReporterSecret = "SS_REPORTER_SECRET"
)

func main() {
	apiUrl := os.Getenv(envSSApiUrl)
	if apiUrl == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSApiUrl))
	}
	reporterId := os.Getenv(envSSReporterKeyId)
	if reporterId == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSReporterKeyId))
	}
	reporterKey := os.Getenv(envSSReporterSecret)
	if reporterKey == "" {
		panic(fmt.Sprintf("environment variable %s must be set", envSSReporterSecret))
	}
	authProvider := ssub.NewReporterAuthProvider(reporterId, reporterKey)
	ssClient := ssub.NewSsub(apiUrl, authProvider)

	me, err := ssClient.Reporters.RetrieveThisReporter()
	if err != nil {
		panic(fmt.Sprintf("error retrieving reporter: %s", err))
	}
	if me == nil {
		panic("no reporter found")
	}

	sensors, err := ssClient.Sensors.RetrieveSensors()
	if err != nil {
		panic(fmt.Sprintf("error retrieving sensors: %s", err))
	}
	if sensors == nil || len(sensors) < 1 {
		panic("no sensors found")
	}
	scheduler := scom.NewScheduler()
	for _, sensor := range sensors {
		switch sensor.Type {
		case scom.ST_Switch:
			err = sc_switch.InitializeSwitchSensor(ssClient, &sensor, scheduler)
		case scom.ST_Temperature:
			err = sc_thermal.InitializeThermalSensor(ssClient, &sensor, scheduler)
		default:
			panic(fmt.Sprintf("unsupported sensor type: %s", sensor.Type))
		}
		if err != nil {
			panic(fmt.Sprintf("error initializing sensor: %s", err))
		}
	}
	scheduler.Start()
	fmt.Printf("Reporter %s monitoring of %d sensors started\n", me.Name, len(sensors))

	// Create a channel to receive OS signals
	signalChan := make(chan os.Signal, 1)

	// Signal to wait for `Ctrl+C`
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	// Wait for the signal
	fmt.Printf("Press Ctrl+C to stop\n")
	sig := <-signalChan
	fmt.Println("Received signal:", sig)
	scheduler.Stop()
	ssub.Cleanup()

	// Exit the program
	os.Exit(0)
}
